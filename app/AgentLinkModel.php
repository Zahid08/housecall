<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class AgentLinkModel extends Model
{
    use Notifiable;

    protected $table = "agents_link";
    protected $primaryKey = 'link_id';
    protected $fillable = ['link_trackid','link_name', 'status'];
    protected $guarded = ['link_id'];

}
