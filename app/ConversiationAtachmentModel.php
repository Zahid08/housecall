<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ConversiationAtachmentModel extends Model
{
    use Notifiable;

    protected $table = "conversation_attachment";
    protected $primaryKey = 'ca_id';
    protected $fillable = [
        'ca_name',
        'ca_conversation_id',
        'ca_type'
    ];
    protected $guarded = ['ca_id'];
}
