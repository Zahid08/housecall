<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ConversiationModel extends Model
{
    use Notifiable;
    protected $table = "conversation";
    protected $primaryKey = 'conversation_id';
    protected $fillable = [
        '	conversation_sent_from',
        '	conversation_sent_to',
        'conversation_subject',
        'conversation_body',
        'conversation_to_deleted',
        'conversation_from_deleted',
        'conversation_status',
        'conversation_trash_from_deleted',
        'conversation_trash_to_deleted'
    ];
    protected $guarded = ['conversation_id','conversation_sent_to'];

}
