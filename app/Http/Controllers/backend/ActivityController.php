<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;

use App\UserActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ActivityController extends Controller
{
    public  function ActivityList(){
        $dataList= UserActivity::orderBy('users_activity_id', 'asc')->paginate(15);
        return view('backend.activity.activity-contetn',['dataList'=>$dataList]);
    }

    public  function removeActivity(Request $request){
        $users_activity_track_id= $request->users_activity_id;
       $dataList=UserActivity::where('users_activity_id',$users_activity_track_id)->first();
          $dataList->delete();
         return redirect()->back()->with('success','Successfully remove this activity');
    }

}
