<?php

namespace App\Http\Controllers\backend;
use App\AgentLinkModel;
use App\Http\Controllers\Controller;

use App\LeadInformationModel;
use App\TrackidModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AgentController extends Controller
{
     public  function  agentLink(){
         $dataList= AgentLinkModel::orderBy('link_id', 'desc')->get();
         $centername=User::where('users_type','Center')->get();

         return view('backend.agentLink.agentContent',compact('dataList','centername'));
     }

     public  function  agentLinkGenerate(Request $request){
        $link_name=Input::get('link_name');
         $errors = array();
         if (empty($link_name) || $link_name == '') {
             $errors['link_name'] = "Link name required";
         }
         if (count($errors) > 0) {
             return redirect()->back()->withInput()->withErrors($errors)->with('errorArray', 'Array Error Occured ');
         }
         else{
             $randomNumber = new TrackidModel();
             $trackId = $randomNumber->trackId(5, 10) . "BI" . date("YmdHis");

             $obj=new AgentLinkModel();
             $obj->link_trackid=$trackId;
             $obj->link_name=$link_name;
             $obj->status="Active";
             $obj->save();
             return redirect()->back()->with('success','Successfully generate your link');
         }
     }


     public  function activeAgentLink(){
         $link_trackid=Input::get('link_trackid');
         $datalist=AgentLinkModel::where('link_trackid',$link_trackid)->first();
         $datalist->status="Active";
         $datalist->save();
         return redirect()->back();
     }

    public  function inActiveAgentLink(){
        $link_trackid=Input::get('link_trackid');
        $datalist=AgentLinkModel::where('link_trackid',$link_trackid)->first();
        $datalist->status="Inactive";
        $datalist->save();
        return redirect()->back();
    }


    public  function removeAgentLink(){
        $link_trackid=Input::get('link_trackid');
        $datalist=AgentLinkModel::where('link_trackid',$link_trackid)->first();
        $datalist->delete();
        return redirect()->back();
    }

     public  function linkstate(){
        $data=AgentLinkModel::where('status','Active')->exists();
       if($data) {
           return redirect('/linktrack/' . $data->link_name . '/' . $data->link_trackid);
       }
       else{
           return redirect('pagenotfound');
       }
     }

     public  function Page($linkname,$id){
         $track_id=$linkname;
         $data=AgentLinkModel::where('link_trackid',$id)
             ->where('status','Active')
             ->exists();
         if ($data){
             return view('frontend.leadForm',compact('track_id'));
         }
         else{
             return redirect('pagenotfound');
         }
     }


     public  function postData(Request $request){
         $centername=Input::get('center_name');
         $agentname=Input::get('agent_name');
         $owners_name=Input::get('owners_name');
         $phone_number=Input::get('phone_number');
         $emergency_number=Input::get('emergency_number');
         $street_address=Input::get('street_address');
         $city=Input::get('city');
         $state=Input::get('state');
         $postal_code=Input::get('postal_code');
         $appointment_date=Input::get('appointment_date');
         $appointment_time=Input::get('appointment_time');
         $appointment_time1=Input::get('appointment_time1');
         $Colsers_name=Input::get('Colsers_name');
         $comments=Input::get('comments');

         $errors = array();

         if (empty($centername) || $centername == '') {
             $errors['centername'] = "center name required";
         }
         if (empty($agentname) || $agentname == '') {
             $errors['agentname'] = "agent name required";
         }

         if (empty($owners_name) || $owners_name == '') {
             $errors['owners_name'] = "owners  name required";
         }

         if (empty($phone_number) || $phone_number == '') {
             $errors['phone_number'] = "Phone number required";
         }
         if (empty($emergency_number) || $emergency_number == '') {
             $errors['emergency_number'] = "emergency number required";
         }
         if (empty($street_address) || $street_address == '') {
             $errors['street_address'] = "street address required";
         }
         if (empty($city) || $city == '') {
             $errors['city'] = "City required";
         }
         if (empty($state) || $state == '') {
             $errors['state'] = "state required";
         }
         if (empty($postal_code) || $postal_code == '') {
             $errors['postal_code'] = "postal_code required";
         }
         if (empty($appointment_date) || $appointment_date == '') {
             $errors['appointment_date'] = "Appointment date required";
         }
         if (empty($appointment_time) || $appointment_time == '') {
             $errors['appointment_time'] = "Appointment time required";
         }

         if (empty($Colsers_name) || $Colsers_name == '') {
             $errors['Colsers_name'] = "Colsers name required";
         }
         if (empty($comments) || $comments == '') {
             $errors['comments'] = "Comments required";
         }

         if (count($errors) > 0) {
             return redirect()->back()->withInput()->withErrors($errors)->with('errorArray', 'Array Error Occured ');
         }
         else{
             $randomNumber = new TrackidModel();
             $trackId = $randomNumber->trackId(5, 10) . "BI" . date("YmdHis");

             $obj=new LeadInformationModel();
             $obj->center_name=$centername;
             $obj->lead_trackid=$trackId;
             $obj->agent_name=$agentname;
             $obj->owners_name=$owners_name;
             $obj->phone_number=$phone_number;
             $obj->emergency_number=$emergency_number;
             $obj->street_address=$street_address;
             $obj->city=$city;
             $obj->state=$state;
             $obj->postal_code=$postal_code;
             $obj->appointment_date=$appointment_date;
             $obj->appointment_time=$appointment_time." - ".$appointment_time1;
             $obj->Colsers_name=$Colsers_name;
             $obj->comments=$comments;
             $obj->save();
             return redirect()-> back()->with('success','Successfully save information');
         }

     }

     public function Link(){
       return redirect()->back();

     }
}
