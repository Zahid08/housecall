<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;

use App\LeadInformationModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CenterController extends Controller
{
    public  function leaddetails(){
        $dataList= LeadInformationModel::where('center_name', Auth::user()->users_track_id)->get();
       return view('backend.center.leadlist',compact('dataList'));
    }

}
