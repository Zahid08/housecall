<?php

namespace App\Http\Controllers\backend;
use App\BrowserModelBI;
use App\Http\Controllers\Controller;

use App\TrackidModel;
use App\User;
use App\UserActivity;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CenterRegistration extends Controller
{
    public  function  centerregistration(){
$dataList=User::where('users_type','Center')->get();
        return view('backend.center.registerCenter',compact('dataList'));
    }
    public  function centerregistrationnew(Request $request){

        $users_name = Input::get('users_name');
        $users_email = Input::get('users_email');
        $password = Input::get('users_password');
        $re_password = Input::get('re_password');

        $errors = array();
        /*
         * Checking user name is empty or not
         */
        if (empty($users_name) || $users_name == '') {
            $errors['users_name'] = "Center name required";
        }
        /*
         * Checking user phone is empty or not
         */
        if (empty($users_email) || $users_email == '') {
            $errors['users_email'] = "Center e-mail required";
        }
        /*
         * Checking email empty or not
         */
        if (!empty(Input::get('users_email'))) {
            if (!filter_var(Input::get('users_email'), FILTER_VALIDATE_EMAIL) === true) {
                $errors['users_email'] = "Center e-mail is not valid";
            } else {
                $checkExistsEmail = User::where('email', Input::get('users_email'))->exists();
                if ($checkExistsEmail) {
                    $errors['users_email'] = "Center e-mail already exists";
                }
            }
        }

        /*
         * Check password is empty or not
         */
        if (empty($password) || $password == '') {
            $errors['users_password'] = "Password required";
        }
        /*
         * Check retype password is empty or not
         */
        if (empty($re_password) || $re_password == '') {
            $errors['re_password'] = "Confirm password required";
        }
        /*
         * Check if password and confirm password matched or not
         */
        if (!empty($password)) {
            if (!empty($re_password)) {
                if ($password != $re_password) {
                    $errors['re_password'] = "Password does not match";
                }
            }
            /*
             * Check password length
             */
            if (strlen($password) < 6) {
                $errors['password'] = "Password must be longer than 5 characters in length";
            }

            if (strlen($password) > 15) {
                $errors['password'] = "Password must be at least 15 characters in length";
            }
        }
        if (count($errors) > 0) {
            return redirect()->back()->withInput()->withErrors($errors)->with('errorArray', 'Array Error Occured');
        }
        else{

            $randomNumber = new TrackidModel();
            $trackId = $randomNumber->trackId(5, 10) . "BI" . date("YmdHis");
            $obj = new User();
            $obj->users_name = $users_name;
            $obj->email = $users_email;
            $obj->users_track_id = $trackId;
            $obj->created_at = Carbon::now();
            $obj->users_veryfication_status = 'Active';
            $obj->password = bcrypt($password);
            $obj->users_type="Center";
            $obj->users_profile_completeness="Yes";

            if ($obj->save()) {
                if (!empty(Input::get('users_email'))) {
                    $text = "Hello " . $users_name . ",Now you are registered. Please login with your valid email address ". $users_email ." and  password ". $password;
                    Mail::raw($text, function($message)use($users_email) {
                        $message->from('infogotegary@gmail.com', 'Housecall');
                        $message->to($users_email)->subject('Registration confirmation');
                        Log::info('End of mail processing');
                    });
                }

                /*
                 * Getting IP address
                 */
                $ipAddress = $_SERVER['REMOTE_ADDR'];
                /*
                 * Getting browser information
                 */
                $browser = new BrowserModelBI();
                $browserInfo = $browser->getBrowser();
                $browserName = $browserInfo['name'];
                $browserVersion = $browserInfo['version'];

                $objActivity = new UserActivity();
                $objActivity->users_activity_browser = $browserName;
                $objActivity->users_activity_browser_version = $browserVersion;
                $objActivity->users_activity_ip = $ipAddress;
                $objActivity->users_activity_track_id = $obj->users_track_id;
                $objActivity->created_at = Carbon::now();
                $objActivity->users_activity_details = 'Registration completed successfully';
                if ($objActivity->save()) {
                    return redirect()->back()->with('success', 'Successfully registerd this center');
                }
            } else {
                return redirect()->back()->with('error', 'Sorry !!! Something went wrong, please try again');
            }
        }
    }

    public  function inactiveCenter(Request $request){
        $users_track_id=Input::get('users_track_id');
        $dataList = User::where('users_track_id', $users_track_id)->first();
        $dataList->users_veryfication_status="Inactive";
        $dataList->save();
        return redirect()->back()->with('success','Successfully inactive this center');
    }
    public function activeCenter(Request $request){
        $users_track_id=Input::get('users_track_id');
        $dataList = User::where('users_track_id', $users_track_id)->first();
        $dataList->users_veryfication_status="Active";
        $dataList->save();
        return redirect()->back()->with('success','Successfully active this center');
    }
    public  function removeCenter(){
        $users_track_id=Input::get('users_track_id');
        $dataList = User::where('users_track_id', $users_track_id)->first();
       $dataList->delete();
        return redirect()->back()->with('success','Successfully remove ');
    }
}
