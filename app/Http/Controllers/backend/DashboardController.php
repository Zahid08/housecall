<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;

use App\LeadInformationModel;
use App\RevenueCalculated;
use App\User;
use App\UserActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function superadminDashboard(){
        $balance = DB::table('lead_informations')->where('job_done', 'Yes')->sum('lead_ammount');
        $dataList= UserActivity::orderBy('users_activity_id', 'asc')->paginate(15);
        $manager=User::where('users_type','Qm')->count();
        $qualitytema=User::where('users_type','Qt')->count();
        $technicians=User::where('users_type','Tc')->count();
        $jobdone=LeadInformationModel::where('job_done','Yes')->count();
        return view('backend.superAdminDashboard',compact('manager','technicians','jobdone','balance','qualitytema'));
    }
    public  function  qualityTeamDashboard(){
        $lead = LeadInformationModel::orderBy('lead_trackid', 'asc')->count();
        return view('backend.qualityTeamDashboard',compact('lead'));
    }

    public  function qualityManagementDashboard(){
        $balance = DB::table('lead_informations')->where('Qm_paid_status','Paid')   ->sum('lead_ammount');
        $technicians=User::where('users_type','Tc')->count();
        $lead = LeadInformationModel::where('Qt_status','Pass')->count();
        $jobdone=LeadInformationModel::where('job_done','Yes')->count();
        return view('backend.qualityManagementDashboard',compact('technicians','lead','jobdone','balance'));
    }

    public  function tecnicianDashboard(){
        $trackid=Auth::user()->users_track_id;
        $assignlead= LeadInformationModel::where('tec_trackid', $trackid)
            ->where('job_done','==',NULL)
            ->count();

        $jobdone= LeadInformationModel::where('tec_trackid', $trackid)
            ->where('job_done','Yes')
            ->count();
        $profit = DB::table('revenues')
            ->where('technicians_track_id',$trackid)
            ->sum('amount_given');
        return view('backend.tecnicianDashboard',compact('jobdone','assignlead','profit'));
    }

    public  function centerDashboard(){
        $balance = DB::table('revenues')->where('center_name',\Illuminate\Support\Facades\Auth::user()->users_track_id)->sum('center_taken');
        $dataList= LeadInformationModel::where('center_name', Auth::user()->users_track_id)->count();
        return view('backend.centerDashboard',compact('balance','dataList'));
    }

    public function hrDashboard(){
        $balance = DB::table('lead_informations')->where('Qm_paid_status','Paid')   ->sum('lead_ammount');
        $technicians=User::where('users_type','Tc')->count();
        $lead = LeadInformationModel::where('Qt_status','Pass')->count();
        $jobdone=LeadInformationModel::where('job_done','Yes')->count();
        return view('backend.hrDashboard',compact('technicians','lead','jobdone','balance'));
    }

    public function adminDashboard(){
        $balance = DB::table('lead_informations')->where('job_done', 'Yes')->sum('lead_ammount');
        $dataList= UserActivity::orderBy('users_activity_id', 'asc')->paginate(15);
        $manager=User::where('users_type','Qm')->count();
        $qualitytema=User::where('users_type','Qt')->count();
        $technicians=User::where('users_type','Tc')->count();
        $jobdone=LeadInformationModel::where('job_done','Yes')->count();
        return view('backend.AdminDashboard',compact('manager','technicians','jobdone','balance','qualitytema'));
    }
}
