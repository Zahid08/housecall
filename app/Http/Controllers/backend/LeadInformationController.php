<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;

use App\LeadInformationModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class LeadInformationController extends Controller
{
    public function leadinformation()
    {
        $dataList = LeadInformationModel::orderBy('lead_trackid', 'asc')->get();
        return view('backend.customers.leadInformation', ['dataList' => $dataList]);
    }
    public  function  contactCustomsers(){
        $dataList = LeadInformationModel::where('Qt_status','Pass')->get();
        return view('backend.customers.contactCustomers', ['dataList' => $dataList]);

    }
    //Qt status update and change
    public  function statusUpdateLead(Request $request){
        $lead_trackid=Input::get('lead_trackid');
         $Qt_status=Input::get('Qt_status');
        $appointment_time=Input::get('appointment_time');
        $appointment_time1=Input::get('appointment_time1');
        $Qt_comments=Input::get('Qt_comments');
        $appointment_date=Input::get('appointment_date');

         if ($Qt_status==='Pass') {
             $datalist = LeadInformationModel::where('lead_trackid', $lead_trackid)->first();
             $datalist->Qt_status = $Qt_status;
             $datalist->Qt_name = Auth::user()->users_name;
             $datalist->Timestamp = Carbon::now();
             $datalist->save();
             return redirect('/portal/customersinformation')->with('success','SUccessfully changed');
         }
         else{
             $datalist = LeadInformationModel::where('lead_trackid', $lead_trackid)->first();
             $datalist->Qt_status = $Qt_status;
             $datalist->appointment_time=$appointment_time. 'to' .$appointment_time1;
             $datalist->appointment_date=$appointment_date;
             $datalist->Qt_comments=$Qt_comments;
             $datalist->Qt_name = Auth::user()->users_name;
             $datalist->Timestamp = Carbon::now();
             $datalist->save();
             return redirect('/portal/customersinformation')->with('success','SUccessfully changed');
         }

    }
    //qt commments update
    public  function comentLeadsInfo(Request $request){
        $lead_trackid=Input::get('lead_trackid');
        $Qt_comments=Input::get('Qt_comments');
        $datalist=LeadInformationModel::where('lead_trackid',$lead_trackid)->first();
        $datalist->Qt_comments=$Qt_comments;
        $datalist->save();
        return redirect()->back();
    }
    //add recording list
    public function addRecording(Request $request){
        $lead_trackid=Input::get('lead_trackid');
        $recodinglink=Input::get('recodinglink');
        $datalist=LeadInformationModel::where('lead_trackid',$lead_trackid)->first();
        $datalist->recodinglink=$recodinglink;
        $datalist->save();
        return redirect()->back();
    }
//admin panel
    public function detailsLeadlist($id){
        $datalist=LeadInformationModel::where('lead_trackid',$id)->first();
        return  view('backend.customers.detalsLead',compact('datalist'));
    }
    public function jobDoneList(){
        $dataList=LeadInformationModel::where('job_done','Yes')->paginate(25);
       return view('backend.customers.jobdoneList',compact('dataList'));
    }

    public function updateLeadinformation(Request $request){
        $lead_trackid=Input::get('lead_trackid');
        $owners_name=Input::get('owners_name');
         $phone_number=Input::get('phone_number');
          $street_address=Input::get('street_address');
         $city=Input::get('city');
        $state=Input::get('state');
        $postal_code=Input::get('postal_code');
        $Colsers_name=Input::get('Colsers_name');


        $dataList=LeadInformationModel::where('lead_trackid',$lead_trackid)->first();
        $dataList->owners_name=$owners_name;
        $dataList->phone_number=$phone_number;
        $dataList->street_address=$street_address;
        $dataList->city=$city;
        $dataList->state=$state;
        $dataList->postal_code=$postal_code;
        $dataList->Colsers_name=$Colsers_name;
        $dataList->save();

        return redirect()->back()->with('success',"successfully updated");
    }

    public  function changeLeadstatus(Request $request){
         $lead_trackid=Input::get('lead_trackid');
         $Qt_status=Input::get('Qt_status');
         $recodinglink=Input::get('recordlink');

        $dataList=LeadInformationModel::where('lead_trackid',$lead_trackid)->first();
        $dataList->Qt_status=$Qt_status;
        $dataList->Qt_name=Auth::user()->users_name;

         if (empty($recodinglink)){
             $dataList->recodinglink='None';
         }
         else{
             $dataList->recodinglink=$recodinglink;
         }

         $dataList->save();
       //  return redirect('/portal/qmDashboard/customerinfo')->with('success','Successfully changed');

        return redirect()->back()->with('success','Successfully changed Quality team status');

    }

    public function updateajaxcallLeadinfo(Request $request){

        $lead_trackid = $_POST['lead_trackid'];
        $phone = $_POST['phone'];
        $ownername = $_POST['ownername'];
        $alternativeno = $_POST['alternativeno'];
        $streetaddress = $_POST['streetaddress'];
        $city = $_POST['city'];
        $state = $_POST['state'];
        $postalcode = $_POST['postalcode'];
        $closername = $_POST['closername'];
        $comment = $_POST['comment'];

        $data=LeadInformationModel::where('lead_trackid',$lead_trackid)->first();
        $data->phone_number=$phone;
        $data->owners_name=$ownername;
        $data->emergency_number	=$alternativeno;
        $data->street_address=$streetaddress;
        $data->city=$city;
        $data->state=$state;
        $data->postal_code=$postalcode;
        $data->	Colsers_name=$closername;
        $data->comments=$comment;
        $data->save();
        return response()->json();

    }
}