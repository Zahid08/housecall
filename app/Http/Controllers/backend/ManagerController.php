<?php


namespace App\Http\Controllers\backend;
use App\ConversiationModel;
use App\Http\Controllers\Controller;
use App\LeadInformationModel;
use App\TechniciansModel;
use App\TrackidModel;
use App\User;
use Carbon\Carbon;
use Faker\Provider\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Console\Input\InputOption;

class ManagerController extends Controller
{
    public  function managerList(){


           $dataList = User::orderBy('created_at','dsc')->get();
           return view('backend.manager.userlist',['dataList'=>$dataList]);

//
    }
//    public  function hrList(){
//        $dataList = User::where('users_type', 'HR')->get();
//        return view('backend.manager.hrlistcontent',['dataList'=>$dataList]);
//    }

    public  function inactiveManager(Request $request){
        $users_track_id=Input::get('users_track_id');
        $dataList = User::where('users_track_id', $users_track_id)->first();
//        $dataList->users_type="NULL";
        $dataList->users_veryfication_status="Inactive";
        $dataList->save();
        return redirect()->back();
    }

    public  function activeUser(Request $request){
        $users_track_id=Input::get('users_track_id');
        $dataList = User::where('users_track_id', $users_track_id)->first();
//        $dataList->users_type="NULL";
        $dataList->users_veryfication_status="Active";
        $dataList->save();
        return redirect()->back();
    }

    public function addNewManager(){
        return view('backend.manager.newManager');
    }

//    public  function leadinformation(){
//        $dataList = LeadInformationModel::orderBy('created_at','dsc')->paginate(10);
//        $tecnicianscountrylist= TechniciansModel::orderBy('tec_id', 'asc')->get();
//        return view('backend.customers.leadinformationmanagerDashboard',compact('dataList','tecnicianscountrylist'));
//    }

    public  function leadinformation(){
        $dataList = LeadInformationModel::orderBy('created_at','dsc')->paginate(50);
        $tecnicianscountrylist= TechniciansModel::orderBy('tec_id', 'asc')->get();
        $stateList = LeadInformationModel::distinct()->get(['state']);
        $postalcode = LeadInformationModel::distinct()->get(['postal_code']);

      /*  $centername =User::distinct()
            ->where('users_type','=','Center')
            ->get(['users_name','users_track_id']);*/

        $centername =LeadInformationModel::distinct()
            ->get(['center_name']);

        return view('backend.customers.leadinformationmanagerDashboard',compact('dataList','tecnicianscountrylist','stateList','centername','postalcode'));
    }
    public function leadinformationDetails(){
        $response = LeadInformationModel::orderBy('created_at','dsc')->get();
        return response()->json($response);

    }
    public  function leaddetailsinformation(Request $request){
        $state = $_POST['state'];
         $status = $_POST['status'];
         $center = $_POST['center'];
         $postalcode = $_POST['postalcode'];

        if($state !="stateselect" &&  $status ==="status" && $center ==="center" && $postalcode ==="postalcode"  ) {
            $dataList = LeadInformationModel::where('state', $state)->get();
        }
        else if  ( $status !="status" && $state ==="stateselect" && $center ==="center" && $postalcode ==="postalcode" ){
            $dataList = LeadInformationModel::where('Qm_status', $status)->get();
        }
        else if  ( $status ==="status" && $state ==="stateselect" && $center !="center" && $postalcode ==="postalcode" ){
            $dataList = LeadInformationModel::where('center_name', $center)->get();
        }
        else if  ( $status ==="status" && $state ==="stateselect" && $center ==="center" && $postalcode !="postalcode" ){
            $dataList = LeadInformationModel::where('postal_code', $postalcode)->get();
        }

        else if ($state !="stateselect"  && $status !="status" && $center ==="center" && $postalcode ==="postalcode" ){
            $dataList = LeadInformationModel::where('state', $state)
                ->where('Qm_status', '=', $status)
                ->get();
        }
        else if ($state !="stateselect"  && $status ==="status" && $center !="center" && $postalcode ==="postalcode" ){
            $dataList = LeadInformationModel::where('state', $state)
                ->where('center_name', '=', $center)
                ->get();
        }
        else if ($state !="stateselect"  && $status ==="status" && $center ==="center" && $postalcode !="postalcode" ){
            $dataList = LeadInformationModel::where('state', $state)
                ->where('postal_code', '=', $postalcode)
                ->get();
        }
        else if ($state !="stateselect"  && $status !="status" && $center !="center" && $postalcode ==="postalcode" ){
            $dataList = LeadInformationModel::where('state', $state)
                ->where('Qm_status', '=', $status)
                ->where('center_name', '=', $center)
                ->get();
        }

        else if ($state !="stateselect"  && $status ="status" && $center !="center" && $postalcode !="postalcode" ){
            $dataList = LeadInformationModel::where('state', $state)
                ->where('center_name', '=', $center)
                ->where('postal_code', '=', $postalcode)
                ->get();
        }
        else if ($state !="stateselect"  && $status !="status" && $center ==="center" && $postalcode !="postalcode" ){
            $dataList = LeadInformationModel::where('state', $state)
                ->where('postal_code', '=', $postalcode)
                ->where('Qm_status', '=', $status)
                ->get();
        }
        else if ($state !="stateselect"  && $status !="status" && $center !="center" && $postalcode !="postalcode" ){
            $dataList = LeadInformationModel::where('state', $state)
                ->where('postal_code', '=', $postalcode)
                ->where('Qm_status', '=', $status)
                ->where('center_name', '=', $center)
                ->get();
        }




        foreach ($dataList as $data){
            $centername=$data->center_name;
            $userdata=\App\User::where('users_track_id',$data->center_name)->first();
            $centartaken=\App\RevenueCalculated::where('center_name',$data->center_name)->first();
            $centernamedata=$userdata->users_name;
        }
        $response = array('dataList' => $dataList,'centername'=>$centernamedata);
        return response()->json($response);
    }

    //lead status change
    public function leadStatusChange(){
        $lead_trackid=Input::get('lead_trackid');
        $Qm_status=Input::get('Qm_status');
        $appointment_time=Input::get('appointment_time');
        $appointment_time1=Input::get('appointment_time1');
        $appointment_date=Input::get('appointment_date');

        if($Qm_status==='JD'){
            $datalist=LeadInformationModel::where('lead_trackid',$lead_trackid)->first();
            $datalist->Qm_status=$Qm_status;
            $datalist->	lead_ammount=Input::get('amount');
            $datalist->save();
            //return redirect('/portal/qmDashboard/customerinfo')->with('success','Successfully changed');
            return redirect()->back()->with('success','Successfully changed job status');
        }
        else if ($Qm_status==='CB'){
            $datalist=LeadInformationModel::where('lead_trackid',$lead_trackid)->first();
            $datalist->Qm_status=$Qm_status;
            $datalist->call_date=Carbon::now();
            $datalist->save();
            return redirect('/portal/qmDashboard/customerinfo')->with('success','Successfully changed');
        }
         else if ( $Qm_status==='CAN' ){
            $datalist=LeadInformationModel::where('lead_trackid',$lead_trackid)->first();
            $datalist->Qm_status=$Qm_status;
            $datalist->save();
            return redirect('/portal/qmDashboard/customerinfo')->with('success','Successfully changed');
        }
        else{

            $datalist=LeadInformationModel::where('lead_trackid',$lead_trackid)->first();
            $datalist->Qm_status=$Qm_status;
            $datalist->appointment_time=$appointment_time . ' to' . $appointment_time1;
            $datalist->appointment_date=$appointment_date;
            $datalist->save();
            return redirect('/portal/qmDashboard/customerinfo')->with('success','Successfully changed');

        }

    }
    public  function newTechnicainaddview(){
        return view('backend.manager.newTechnician');
    }
    public function assignTechnicianList($id){
        $leadtrakid=$id;
        $datalead=LeadInformationModel::where('lead_trackid',$id)->first();
        $dataList=TechniciansModel::where('state','=',$datalead->state)->get();
            return view('backend.manager.technicianListForAssign',compact('leadtrakid','dataList'));

            return redirect()->back()->with('dataList',$dataList);

//         $leadtrakid= $state = $_POST['leadtrackid'];
//        $datalead=LeadInformationModel::where('lead_trackid',$leadtrakid)->first();
//        $dataList=TechniciansModel::where('state','=',$datalead->state)->get();
//        $response = array('datalead' => $datalead,'dataList' => $dataList,'leadtrackid'=>$leadtrakid);
//        return response()->json($response);
    }

    public  function listTc(){
       // $dataList= TechniciansModel::orderBy('tec_id', 'desc')->get();
        $dataList= User::where('users_type','Tc')->get();
        return view('backend.manager.listnewTechnician',['dataList'=>$dataList]);
    }
    public function DetailslistTc($id){
        $datalist=User::where('users_type','Tc')
            ->where('users_track_id',$id)
            ->first();
        $terchniciansList=TechniciansModel::where('tec_trackid',$id)->get();
        return view('backend.technicians.detailsTechnicians',compact('datalist','terchniciansList'));
    }

    public  function newTechnicians(Request $request){

        $statename=$request->get('name');


//        if (count($statename)>1){
//             foreach ($statename as $data) {
//                 if(in_array($data,$statename)){
//                     return "founed";
//                 }
//                 else{
//                     return "Not found";
//                 }
//
//             }
//        }
//        else{
//            return "asdsad";
//        }


        $errors = array();
        $tec_name=Input::get('tec_name');
        $email=Input::get('email');
        $tec_phone_number=Input::get('tec_phone_number');
        $tec_gender=Input::get('tec_gender');

//        $number = ['011', '015', '016', '017', '018', '019'];
//        $mobileNumber = str_split($tec_phone_number, 3);
//        $mobileNumber[0];
//
//
//        if (!empty($tec_phone_number)) {
//            if (strlen($tec_phone_number) != 11) {
//                $errors['tec_phone_number'] = "Phone number should be 11 digits";
//            }
//            /*
//             * Checking phone number is numeric value or not
//             */
//
//            if (!is_numeric($tec_phone_number)) {
//                $errors['tec_phone_number'] = "Phone number must be numeric value";
//            }
//            /*
//             * Checking user moble number validation
//             */
//
//            if (!in_array($mobileNumber[0], $number)) {
//                $errors['tec_phone_number'] = "Phone number format does not match";
//            }
//        }

        $tec_birthdate=Input::get('tec_birthdate');
        $tec_country=Input::get('tec_country');
        $tec_street_address=Input::get('tec_street_address');
        $password = Input::get('password');

        if (empty($tec_name) || $tec_name == '') {
            $errors['tec_name'] = "technicians name required";
        }


        if (empty($tec_street_address) || $tec_street_address == '') {
            $errors['tec_street_address'] = "technicians street address  required";
        }

        if (empty($email) || $email == '') {
            $errors['email'] = "email is required";
        }
        if (empty($tec_phone_number) || $tec_phone_number == '') {
            $errors['tec_phone_number'] = "technicinas phonenumber is required";
        }

        if (empty($tec_country) || $tec_country == '') {
            $errors['tec_country'] = "city required";
        }


        if (empty($password) || $password == '') {
            $errors['password'] = "password required";
        }

        if (!empty(Input::get('email'))) {
            if (!filter_var(Input::get('email'), FILTER_VALIDATE_EMAIL) === true) {
                $errors['email'] = " e-mail is not valid";
            } else {
                $checkExistsEmail = User::where('email', Input::get('email'))->exists();
                if ($checkExistsEmail) {
                    $errors['email'] = "user e-mail already exists";
                }
            }
        }




//        if (empty($email) || $email == '') {
//            $errors['email'] = "national id required";
//        }
//        if (empty($tec_phone_number) || $tec_phone_number == '') {
//            $errors['tec_phone_number'] = "gender required";
//        }
//
//        if (empty($tec_nationalid) || $tec_nationalid == '') {
//            $errors['tec_nationalid'] = "birthdate required";
//        }
//        if (empty($tec_gender) || $tec_gender == '') {
//            $errors['tec_gender'] = "phone number required";
//        }
//        if (empty($tec_birthdate) || $tec_birthdate == '') {
//            $errors['tec_birthdate'] = "phone number required";
//        }
//        if (empty($tec_country) || $tec_country == '') {
//            $errors['tec_country'] = "phone number required";
//        }
//        if (empty($tec_street_address) || $tec_street_address == '') {
//            $errors['tec_street_address'] = "phone number required";
//        }


        if (count($errors) > 0) {
            return redirect()->back()->withInput()->withErrors($errors)->with('errorArray', 'Array Error Occured ');
        }
        else{

            $randomNumber = new TrackidModel();
            $trackId = $randomNumber->trackId(5, 10) . "BI" . date("YmdHis");
            $users=new User();
            $users->users_track_id=$trackId;
            $users->password = bcrypt($password);
            $users->users_name =$tec_name;
            $users->email =$email;
            $users->users_phone_number =$tec_phone_number;
            $users->users_gender =$tec_gender;
            $users->users_country =$tec_country;
            $users->users_present_address =$tec_street_address;
            $users->users_type ="Tc";
            $users->users_profile_completeness ="Yes";
            $users->users_veryfication_status ="Inactive";

            $image = $users->image;
            if (!empty($image)) {
                if ($_FILES['image']['name']) {
                    $userImage = public_path("upload/users/{$users->image}"); // get previous image from folder
                    if (File::exists($userImage)) { // unlink or remove previous image from folder
                        unlink($userImage);
                    }
                    $image = $request->file('image');
                    $imageName = date('YmdHis') . "BI" . rand(5, 10) . '.' . $request->image->getClientOriginalExtension();
                    $request->image->move(('upload/users'), $imageName);
                    $users->image = $imageName;
                } else {
                    $users->image = $users->image;
                }
            } else {
                if ($_FILES['image']['name']) {
                    $image = $request->file('image');
                    $imageName = date('YmdHis') . "BI" . rand(5, 10) . '.' . $request->image->getClientOriginalExtension();
                    $request->image->move(('upload/users'), $imageName);
                    $users->image = $imageName;
                }
            }
            $users->save();
        }

        if (count($statename)>1) {
            foreach ($statename as $data) {

                    $obj = new  TechniciansModel();
                    $obj->tec_name = $tec_name;
                    $obj->tec_phone_number = $tec_phone_number;
                    $obj->tec_trackid = $trackId;
                    $obj->manager_trackid = Auth::user()->users_track_id;
                    $obj->tec_gender = $tec_gender;
                    $obj->tec_birthdate = $tec_birthdate;
                    $obj->tec_country = $tec_country;
                    $obj->state = $data;
                    $obj->tec_status = "Inactive";
                    $obj->tec_street_address = $tec_street_address;
                    $obj->email = $email;
                    if ($obj->save()) {
                        if (!empty($email)) {
                            $text = "Hello " . $obj->users_name . ",Now You are registered as a member of technicians into our system. Please login with your valid email address " . $email . " and password " . $password;
                            Mail::raw($text, function ($message) use ($email) {
                                $message->from('housecall@brotherhoodinfotech.com', 'Housecall');
                                $message->to($email)->subject('Registration confirmation');
                                Log::info('End of mail processing');
                            });
                        }
                    }
            }
            return redirect('/portal/qmDashboard/listTc')->with('success','Successfully save technicians');
        }
        else{

            $obj=new  TechniciansModel();
            $obj->tec_name=$tec_name;
            $obj->tec_phone_number=$tec_phone_number;
            $obj->tec_trackid=$trackId;
            $obj->manager_trackid=Auth::user()->users_track_id;
            $obj->tec_gender=$tec_gender;
            $obj->tec_birthdate=$tec_birthdate;
            $obj->tec_country=$tec_country;
            foreach ($statename as $data) {
                $obj->state = $data;
            }
            $obj->tec_status="Inactive";
            $obj->tec_street_address=$tec_street_address;
            $obj->email=$email;
            if ($obj->save()) {
                if (!empty($email)) {
                    $text = "Hello " . $obj->users_name . ",Now You are registered as a member of technicians into our system. Please login with your valid email address ".$email." and password ".$password;
                    Mail::raw($text, function ($message) use ($email) {
                        $message->from('housecall@brotherhoodinfotech.com', 'Housecall');
                        $message->to($email)->subject('Registration confirmation');
                        Log::info('End of mail processing');
                    });
                }
                return redirect('/portal/qmDashboard/listTc')->with('success','Successfully save technicians');
            }

       }
    }

    public  function assigConfirmed(Request $request){
        $lead_trackid=Input::get('lead_trackid');
        $tec_trackid=Input::get('tec_trackid');

        $userlist=User::where('users_track_id',$tec_trackid)->first();
        $obj= LeadInformationModel::where('lead_trackid', $lead_trackid)->first();
        $obj->tec_trackid=$tec_trackid;
        $obj->job_done="No";
        $obj->tec_name=$userlist->users_name;
        $obj->save();

        $dataList = TechniciansModel::where('tec_trackid', $tec_trackid)->first();
        $dataList->assign_leadtrackid=$lead_trackid;
        $email=$dataList->email;
       if ($dataList->save()) {
            if (!empty($email)) {
                $text = "Hello " . $dataList->tec_name . ", You are assigned new Lead please check your system";
                Mail::raw($text, function ($message) use ($email) {
                    $message->from('housecall@brotherhoodinfotech.com', 'Housecall');
                    $message->to($email)->subject('Lead confirmation');
                    Log::info('End of mail processing');
                });
            }
            return redirect('/portal/qmDashboard/customerinfo');
        }
    }
    //job done list retun view
    public  function jobDoneList(){
        $dataList= LeadInformationModel::where('job_done', 'Yes')->get();
        return view('backend.manager.jobdoneList',['dataList'=>$dataList]);
    }
    //contact customer
    public function contactCustomer(){
        $dataList = LeadInformationModel::where('Qt_status','Pass')->get();
        return view('backend.manager.contactCustomer',['dataList'=>$dataList]);
    }
    public function detailsUserList($id){
      $datalist=User::where('users_track_id',$id)->first();
     return view('backend.manager.userdetails',compact('datalist'));
    }

    public function detailsleadinformation($id){
        
        $datalist=LeadInformationModel::where('lead_trackid',$id)->first();
        return  view('backend.customers.detalsLeadMPortal',compact('datalist'));

    }

}
