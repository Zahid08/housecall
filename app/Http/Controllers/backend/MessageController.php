<?php

namespace App\Http\Controllers\backend;
use App\BrowserModelBI;
use App\ConversiationAtachmentModel;
use App\ConversiationModel;
use App\Http\Controllers\Controller;

use App\LeadInformationModel;
use App\TechniciansModel;
use App\TrackidModel;
use App\User;
use App\UserActivity;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use File;
use PhpParser\Node\Stmt\Return_;

class MessageController extends Controller
{
    public  function messageCompose(){

        $dataList = DB::table('technicians')->select('tec_country','tec_street_address','email','tec_trackid')->get();
        return view('backend.message.composeContent',['dataList'=>$dataList]);
    }
    public function getstreetaddress(Request $request){
        $tec_trackid = $_POST['tec_trackid'];
        $streetaddress=TechniciansModel::where('tec_trackid',$tec_trackid)->get();
        $response = array('output' => 'success', 'msg' => 'data found', 'streetaddress' => $streetaddress);
        return response()->json($response);
    }
    public function storeMessageCompose(Request $request){

        $conversation_subject = Input::get('conversation_subject');
        $conversation_body = Input::get('conversation_body');
        $tec_trackid=Input::get('conversation_sent_to');

        $errors = array();

        if (empty($conversation_subject) || $conversation_subject == '') {
            $errors['conversation_subject'] = "Subject required";
        }

        if (empty($conversation_body) || $conversation_body == '') {
            $errors['conversation_body'] = "Message body required";
        }

        /*
         * Checking attachment is empty or not
         */
        if (!empty($request->file('ca_name'))) {
            if ($_FILES['ca_name']['name']) {
                $allowed = array('jpg', 'JPG', 'jpeg', 'JPEG', 'doc', 'docx', 'DOC', 'DOCX', 'pdf', 'PDF', 'png', 'PNG');
                $ext = pathinfo($_FILES['ca_name']['name'], PATHINFO_EXTENSION);
                if (!in_array($ext, $allowed)) {
                    $errors['ca_name'] = "Attachment file should be in JPG, JPEG, JPEG, DOC, PDF format";
                }
                if (File::size(Input::file("ca_name")) > 2048000) {
                    $errors['ca_name'] = "Message attachment file must be less than 2 MB";
                }
            }
        }
        if (count($errors) > 0) {
            return redirect()->back()->withInput()->withErrors($errors)->with('errorArray', 'Array Error Occured');
        } else {

            $randomNumber = new TrackidModel();
            $conversation_track_id = $randomNumber->trackId(5, 10) . "BI" . date("YmdHis");

            $obj = new ConversiationModel();
            $obj->conversation_track_id = $conversation_track_id;
            $obj->conversation_sent_from = Auth::user()->users_track_id;
            $obj->conversation_sent_to = $tec_trackid;
            $obj->conversation_subject = $conversation_subject;
            $obj->conversation_body = $conversation_body;
            $obj->conversation_from_deleted = 'Active';
            $obj->conversation_to_deleted = 'Active';
            $obj->conversation_status = 'Unread';
            $obj->conversation_trash_from_deleted = 'Undeleted';
            $obj->conversation_trash_to_deleted = 'Undeleted';
            $obj->created_at = Carbon::now();
            if ($obj->save()) {
                $conversationAttachment = new ConversiationAtachmentModel();
                $conversationAttachment->ca_conversation_id = $obj->conversation_track_id;
                $conversationAttachment->created_at = Carbon::now();

                if ($_FILES['ca_name']['name']) {

                    $ext = pathinfo($_FILES['ca_name']['name'], PATHINFO_EXTENSION);
                    if ($ext === 'jpeg' || $ext === 'jpg' || $ext === 'JPG' || $ext === 'JPEG' || $ext === 'png' || $ext === 'PNG') {
                        $ca_type = "Image";
                    } else if ($ext === 'doc' || $ext === 'docx' || $ext === 'DOC' || $ext === 'DOCX') {
                        $ca_type = "Doc";
                    } else {
                        $ca_type = "Pdf";
                    }
                    if ($ext === 'jpeg' || $ext === 'jpg' || $ext === 'JPG' || $ext === 'JPEG' || $ext === 'png' || $ext === 'PNG' || $ext === 'doc' || $ext === 'docx' || $ext === 'DOC' || $ext === 'DOCX' || $ext === 'pdf' || $ext === 'PDF') {
                        $imageName = date('YmdHis') . "BI" . rand(5, 10) . '.' . $request->ca_name->getClientOriginalExtension();
                        $request->ca_name->move(('upload/message_attachment/'), $imageName);
                        $conversationAttachment->ca_name = $imageName;
                        $conversationAttachment->ca_type = $ca_type;
                    }
                }
                $conversationAttachment->save();
                    return redirect('/portal/message/tecniciansSent/list')->with('success', 'Congratulations, message send successfully');
        }
        }
    }

    //technicians list
    public  function technicinasList(){
        $dataList = ConversiationModel::leftJoin('conversation_attachment', 'conversation.conversation_track_id', 'conversation_attachment.ca_conversation_id')
            ->leftJoin('technicians', 'conversation.conversation_sent_from', 'technicians.tec_trackid')
            ->where('conversation_sent_from', Auth::user()->users_track_id)
            ->where('conversation_to_deleted', 'Active')
            ->orderBy('conversation.created_at', 'DESC')
            ->select('conversation.*', 'conversation_attachment.*')
            ->paginate(15);
        $wordcount = new ConversiationModel();
        return view('backend.message.sentMessageTechnicinas', compact('dataList', 'wordcount'));

    }
    //manager portal
    //technicians
    public function viewtechnicinas($id){
        $dataList = ConversiationModel::leftJoin('conversation_attachment', 'conversation.conversation_track_id', 'conversation_attachment.ca_conversation_id')
            ->leftJoin('technicians', 'conversation.conversation_sent_to', 'technicians.tec_trackid')
            ->where('conversation_sent_from', Auth::user()->users_track_id)
            ->where('conversation_track_id', $id)
            ->select('conversation.*', 'conversation_attachment.*', 'technicians.tec_name','technicians.tec_street_address','technicians.tec_country')
            ->first();

        return view('backend.message.viewsentMessage', compact('dataList'));
    }
//technicains portal
//message delete
    public function deleteMessage(){
        $id = Input::get('conversation_track_id');
        $dataList = ConversiationModel::where('conversation_track_id', $id)->where('conversation_sent_from', Auth::user()->users_track_id)->first();
        $dataList->conversation_to_deleted = 'Inactive';
        $dataList->save();
        return redirect()->back();

    }

//Technicains portal
// Message send view
    public function techniciansSendmessage(){

        $data=TechniciansModel::where('tec_trackid',Auth::user()->users_track_id)->first();
        $managerid=$data->manager_trackid;
        return view('backend.message.techniciansSendmessage',compact('managerid'));
    }

    //Technicians Portal
    //compose techniciansmessge
    public  function composeTechniciansMessage(Request $request){
        $conversation_subject = Input::get('conversation_subject');
        $conversation_body = Input::get('conversation_body');

        $data=ConversiationModel::where('conversation_sent_to',Auth::user()->users_track_id)->first();
        $managerid=$data->conversation_sent_from;

        $errors = array();
        /*
         * Checking house name is empty or not
         */

        if (empty($conversation_subject) || $conversation_subject == '') {
            $errors['conversation_subject'] = "Subject required";
        }

        if (empty($conversation_body) || $conversation_body == '') {
            $errors['conversation_body'] = "Message body required";
        }

        /*
         * Checking attachment is empty or not
         */
        if (!empty($request->file('ca_name'))) {
            if ($_FILES['ca_name']['name']) {
                $allowed = array('jpg', 'JPG', 'jpeg', 'JPEG', 'doc', 'docx', 'DOC', 'DOCX', 'pdf', 'PDF', 'png', 'PNG');
                $ext = pathinfo($_FILES['ca_name']['name'], PATHINFO_EXTENSION);
                if (!in_array($ext, $allowed)) {
                    $errors['ca_name'] = "Attachment file should be in JPG, JPEG, JPEG, DOC, PDF format";
                }
                if (File::size(Input::file("ca_name")) > 2048000) {
                    $errors['ca_name'] = "Message attachment file must be less than 2 MB";
                }
            }
        }
        if (count($errors) > 0) {
            return redirect()->back()->withInput()->withErrors($errors)->with('errorArray', 'Array Error Occured');
        } else {
            $randomNumber = new TrackidModel();
            $conversation_track_id = $randomNumber->trackId(5, 10) . "BI" . date("YmdHis");
            $obj = new ConversiationModel();
            $obj->conversation_track_id = $conversation_track_id;
            $obj->conversation_sent_from = Auth::user()->users_track_id;
            $obj->conversation_sent_to =$managerid;
            $obj->conversation_subject = $conversation_subject;
            $obj->conversation_body = $conversation_body;
            $obj->conversation_from_deleted = 'Active';
            $obj->conversation_to_deleted = 'Active';
            $obj->conversation_status = 'Unread';
            $obj->conversation_trash_from_deleted = 'Undeleted';
            $obj->conversation_trash_to_deleted = 'Undeleted';
            $obj->created_at = Carbon::now();
            if ($obj->save()) {
                $conversationAttachment = new ConversiationAtachmentModel();
                $conversationAttachment->ca_conversation_id = $obj->conversation_track_id;
                $conversationAttachment->created_at = Carbon::now();
                if ($_FILES['ca_name']['name']) {
                    $ext = pathinfo($_FILES['ca_name']['name'], PATHINFO_EXTENSION);
                    if ($ext === 'jpeg' || $ext === 'jpg' || $ext === 'JPG' || $ext === 'JPEG' || $ext === 'png' || $ext === 'PNG') {
                        $ca_type = "Image";
                    } else if ($ext === 'doc' || $ext === 'docx' || $ext === 'DOC' || $ext === 'DOCX') {
                        $ca_type = "Doc";
                    } else {
                        $ca_type = "Pdf";
                    }
                    if ($ext === 'jpeg' || $ext === 'jpg' || $ext === 'JPG' || $ext === 'JPEG' || $ext === 'png' || $ext === 'PNG' || $ext === 'doc' || $ext === 'docx' || $ext === 'DOC' || $ext === 'DOCX' || $ext === 'pdf' || $ext === 'PDF') {
                        $imageName = date('YmdHis') . "OL" . rand(5, 10) . '.' . $request->ca_name->getClientOriginalExtension();
                        $request->ca_name->move(('upload/backend/message_attachment/'), $imageName);
                        $conversationAttachment->ca_name = $imageName;
                        $conversationAttachment->ca_type = $ca_type;
                    }
                }
                $conversationAttachment->save();
                return redirect('portal/message/texhnicianSent/list')->with('success', 'Congratulations, message send successfully');
            }
        }
    }

    //technicians portal
    //Sent message list Sent

    public  function sentMessagelist(){
        $dataList = ConversiationModel::leftJoin('conversation_attachment', 'conversation.conversation_track_id', 'conversation_attachment.ca_conversation_id')
            ->leftJoin('technicians', 'conversation.conversation_sent_from', 'technicians.tec_trackid')
            ->where('conversation_sent_from', Auth::user()->users_track_id)
            ->where('conversation_to_deleted', 'Active')
            ->orderBy('conversation.created_at', 'DESC')
            ->select('conversation.*', 'conversation_attachment.*')
            ->get();
        $wordcount = new ConversiationModel();
        return view('backend.message.techniciansSentmessageList', compact('dataList', 'wordcount'));

    }

//mnagaer portal
//view message list
    public function inboxMessage(){
        $dataList = ConversiationModel::leftJoin('conversation_attachment', 'conversation.conversation_track_id', 'conversation_attachment.ca_conversation_id')
            ->leftJoin('technicians', 'conversation.conversation_sent_from', 'technicians.tec_trackid')
            ->where('conversation_sent_to', Auth::user()->users_track_id)
            ->where('conversation_to_deleted', 'Active')
            ->orderBy('conversation.created_at', 'DESC')
            ->select('conversation.*', 'conversation_attachment.*', 'technicians.tec_name','technicians.tec_street_address','technicians.tec_country')
            ->get();
        $wordcount = new ConversiationModel();
        return view('backend.message.inboxList', compact('dataList', 'wordcount'));

    }

    //manager portal
    //view Inbox message lsit from technicians
    public  function viewInboxmessage($id){
        $dataList = ConversiationModel::leftJoin('conversation_attachment', 'conversation.conversation_track_id', 'conversation_attachment.ca_conversation_id')
            ->leftJoin('technicians', 'conversation.conversation_sent_from', 'technicians.tec_trackid')
            ->leftJoin('users', 'technicians.tec_trackid', 'users.users_track_id')
            ->where('conversation_sent_to', Auth::user()->users_track_id)
            ->where('conversation_track_id', $id)
            ->select('conversation.*', 'conversation_attachment.*','technicians.tec_name','technicians.tec_street_address','technicians.tec_country')
            ->first();
        $conversationRead = ConversiationModel::where('conversation_track_id', $id)->first();
        $conversationRead->conversation_status = 'Read';
        if ($conversationRead->save()) {
            return view('backend.message.signleMessage', compact('dataList'));
        }

    }

    //manager portal
    //invbox message delete
    public  function inboxDelete(){
        $id = Input::get('conversation_track_id');
        $dataList = ConversiationModel::where('conversation_track_id', $id)->where('conversation_sent_to', Auth::user()->users_track_id)->first();
        $dataList->conversation_to_deleted = 'Inactive';
        $dataList->save();

        /*
       * Getting IP address
       */
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        /*
         * Getting browser information
         */
        $browser = new BrowserModelBI();
        $browserInfo = $browser->getBrowser();
        $browserName = $browserInfo['name'];
        $browserVersion = $browserInfo['version'];

        $objActivity = new UserActivity();
        $objActivity->users_activity_browser = $browserName;
        $objActivity->users_activity_browser_version = $browserVersion;
        $objActivity->users_activity_ip = $ipAddress;
        $objActivity->users_activity_track_id = Auth::user()->users_track_id;
        $objActivity->created_at = Carbon::now();
        $objActivity->users_activity_details = 'Message deleted form inbox successfully';
        if ($objActivity->save()) {
            return redirect()->back()->with('success', 'You successfully delete this message');
        }
    }

    //Manager Portal
    //view replay message
    public function replayMessage($id){
        $dataList = ConversiationModel::where('conversation_track_id', $id)->first();
        $conversationRead = ConversiationModel::where('conversation_track_id', $id)->first();
        $conversationRead->conversation_status = 'Read';
        if ($conversationRead->save()) {
            return view('backend.message.reply', compact('dataList'));
        }
    }

    //manager portal
    //manager portal replay message function replay->each tehcnicians
    public  function  sendReplayMessage(Request $request){
        $conversation_sent_to = Input::get('conversation_sent_to');
        $conversation_sent_from = Input::get('conversation_sent_from');
        $conversation_subject = Input::get('conversation_subject');
        $conversation_body = Input::get('conversation_body');
        $errors = array();
        if (empty($conversation_subject) || $conversation_subject == '') {
            $errors['conversation_subject'] = "Subject required";
        }

        if (empty($conversation_body) || $conversation_body == '') {
            $errors['conversation_body'] = "Message body required";
        }

        /*
         * Checking attachment is empty or not
         */
        if (!empty($request->file('ca_name'))) {
            if ($_FILES['ca_name']['name']) {
                $allowed = array('jpg', 'JPG', 'jpeg', 'JPEG', 'doc', 'docx', 'DOC', 'DOCX', 'pdf', 'PDF', 'png', 'PNG');
                $ext = pathinfo($_FILES['ca_name']['name'], PATHINFO_EXTENSION);
                if (!in_array($ext, $allowed)) {
                    $errors['ca_name'] = "Attachment file should be in JPG, JPEG, JPEG, DOC, PDF format";
                }
                if (File::size(Input::file("ca_name")) > 2048000) {
                    $errors['ca_name'] = "Message attachment file must be less than 2 MB";
                }
            }
        }

        if (count($errors) > 0) {
            return redirect()->back()->withInput()->withErrors($errors)->with('errorArray', 'Array Error Occured');
        } else {
            $randomNumber = new TrackidModel();
            $conversation_track_id = $randomNumber->trackId(5, 10) . "BI" . date("YmdHis");
            $obj = new ConversiationModel();
            $obj->conversation_track_id = $conversation_track_id;
            $obj->conversation_sent_from = Auth::user()->users_track_id;
            $obj->conversation_sent_to =$conversation_sent_from;
            $obj->conversation_subject = $conversation_subject;
            $obj->conversation_body = $conversation_body;
            $obj->conversation_from_deleted = 'Active';
            $obj->conversation_to_deleted = 'Active';
            $obj->conversation_status = 'Unread';
            $obj->conversation_trash_from_deleted = 'Undeleted';
            $obj->conversation_trash_to_deleted = 'Undeleted';
            $obj->created_at = Carbon::now();
            if ($obj->save()) {
                $conversationAttachment = new ConversiationAtachmentModel();
                $conversationAttachment->ca_conversation_id = $obj->conversation_track_id;
                $conversationAttachment->created_at = Carbon::now();
                if ($_FILES['ca_name']['name']) {
                    $ext = pathinfo($_FILES['ca_name']['name'], PATHINFO_EXTENSION);
                    if ($ext === 'jpeg' || $ext === 'jpg' || $ext === 'JPG' || $ext === 'JPEG' || $ext === 'png' || $ext === 'PNG') {
                        $ca_type = "Image";
                    } else if ($ext === 'doc' || $ext === 'docx' || $ext === 'DOC' || $ext === 'DOCX') {
                        $ca_type = "Doc";
                    } else {
                        $ca_type = "Pdf";
                    }
                    if ($ext === 'jpeg' || $ext === 'jpg' || $ext === 'JPG' || $ext === 'JPEG' || $ext === 'png' || $ext === 'PNG' || $ext === 'doc' || $ext === 'docx' || $ext === 'DOC' || $ext === 'DOCX' || $ext === 'pdf' || $ext === 'PDF') {
                        $imageName = date('YmdHis') . "OL" . rand(5, 10) . '.' . $request->ca_name->getClientOriginalExtension();
                        $request->ca_name->move(('upload/backend/message_attachment/'), $imageName);
                        $conversationAttachment->ca_name = $imageName;
                        $conversationAttachment->ca_type = $ca_type;
                    }
                }
                $conversationAttachment->save();

                return redirect('/portal/message/tecniciansSent/list')->with('success', 'Congratulations, message send successfully');
            }

        }
    }

//technicians dashboard
//view sent message dettails
    public  function viewSentMessage($id){
        $dataList = ConversiationModel::leftJoin('conversation_attachment', 'conversation.conversation_track_id', 'conversation_attachment.ca_conversation_id')
            ->leftJoin('technicians', 'conversation.conversation_sent_from', 'technicians.tec_trackid')
            ->where('conversation_sent_from', Auth::user()->users_track_id)
            ->where('conversation_track_id', $id)
            ->select('conversation.*', 'conversation_attachment.*')
            ->first();
        return view('backend.message.techniciansSentMessageView', compact('dataList'));
    }


    //technicains portal
    //delete sent message data
    public  function sentMessageDelete(){
        $id = Input::get('conversation_track_id');
        $dataList = ConversiationModel::where('conversation_track_id', $id)->where('conversation_sent_from', Auth::user()->users_track_id)->first();
        $dataList->conversation_to_deleted = 'Inactive';
        $dataList->save();
        /*
         * Getting IP address
         */
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        /*
         * Getting browser information
         */
        $browser = new BrowserModelBI();
        $browserInfo = $browser->getBrowser();
        $browserName = $browserInfo['name'];
        $browserVersion = $browserInfo['version'];

        $objActivity = new UserActivity();
        $objActivity->users_activity_browser = $browserName;
        $objActivity->users_activity_browser_version = $browserVersion;
        $objActivity->users_activity_ip = $ipAddress;
        $objActivity->users_activity_track_id = Auth::user()->users_track_id;
        $objActivity->created_at = Carbon::now();
        $objActivity->users_activity_details = 'Message deleted form inbox successfully';
        if ($objActivity->save()) {
            return redirect()->back()->with('success', 'You successfully delete this message');
        }

    }
    public  function inboxlistTechnicians(){
        $dataList = ConversiationModel::leftJoin('conversation_attachment', 'conversation.conversation_track_id', 'conversation_attachment.ca_conversation_id')
            ->leftJoin('technicians', 'conversation.conversation_sent_from', 'technicians.tec_trackid')
            ->where('conversation_sent_to', Auth::user()->users_track_id)
            ->where('conversation_to_deleted', 'Active')
            ->orderBy('conversation.created_at', 'DESC')
            ->select('conversation.*', 'conversation_attachment.*')
            ->get();
        $wordcount = new  ConversiationModel();
        return view('backend.message.techniciansInbox', compact('dataList', 'wordcount'));
    }
//technicians poral
//replay message show
    public function techniciansReplayMessage($id){
        $dataList = ConversiationModel::where('conversation_track_id', $id)->first();
        $conversationRead = ConversiationModel::where('conversation_track_id', $id)->first();
        $conversationRead->conversation_status = 'Read';
        if ($conversationRead->save()) {
            return view('backend.message.technicainsReplay', compact('dataList'));
        }
    }

   public function replayMessageTechnicains(Request $request){
       $conversation_sent_to = Input::get('conversation_sent_to');
       $conversation_sent_from = Input::get('conversation_sent_from');
       $conversation_subject = Input::get('conversation_subject');
       $conversation_body = Input::get('conversation_body');

       $data=ConversiationModel::where('conversation_sent_to',Auth::user()->users_track_id)->first();
       $managerid=$data->conversation_sent_from;

       $errors = array();

       /*
        * Checking house name is empty or not
        */


       if (empty($conversation_subject) || $conversation_subject == '') {
           $errors['conversation_subject'] = "Subject required";
       }

       if (empty($conversation_body) || $conversation_body == '') {
           $errors['conversation_body'] = "Message body required";
       }

       /*
        * Checking attachment is empty or not
        */
       if (!empty($request->file('ca_name'))) {
           if ($_FILES['ca_name']['name']) {
               $allowed = array('jpg', 'JPG', 'jpeg', 'JPEG', 'doc', 'docx', 'DOC', 'DOCX', 'pdf', 'PDF', 'png', 'PNG');
               $ext = pathinfo($_FILES['ca_name']['name'], PATHINFO_EXTENSION);
               if (!in_array($ext, $allowed)) {
                   $errors['ca_name'] = "Attachment file should be in JPG, JPEG, JPEG, DOC, PDF format";
               }
               if (File::size(Input::file("ca_name")) > 2048000) {
                   $errors['ca_name'] = "Message attachment file must be less than 2 MB";
               }
           }
       }


       if (count($errors) > 0) {
           return redirect()->back()->withInput()->withErrors($errors)->with('errorArray', 'Array Error Occured');
       } else {

           $randomNumber = new TrackidModel();
           $conversation_track_id = $randomNumber->trackId(5, 10) . "BI" . date("YmdHis");
           $obj = new ConversiationModel();
           $obj->conversation_track_id = $conversation_track_id;
           $obj->conversation_sent_from = Auth::user()->users_track_id;
           $obj->conversation_sent_to =$managerid;
           $obj->conversation_subject = $conversation_subject;
           $obj->conversation_body = $conversation_body;
           $obj->conversation_from_deleted = 'Active';
           $obj->conversation_to_deleted = 'Active';
           $obj->conversation_status = 'Unread';
           $obj->conversation_trash_from_deleted = 'Undeleted';
           $obj->conversation_trash_to_deleted = 'Undeleted';
           $obj->created_at = Carbon::now();
           if ($obj->save()) {
               $conversationAttachment = new ConversiationAtachmentModel();
               $conversationAttachment->ca_conversation_id = $obj->conversation_track_id;
               $conversationAttachment->created_at = Carbon::now();
               if ($_FILES['ca_name']['name']) {
                   $ext = pathinfo($_FILES['ca_name']['name'], PATHINFO_EXTENSION);
                   if ($ext === 'jpeg' || $ext === 'jpg' || $ext === 'JPG' || $ext === 'JPEG' || $ext === 'png' || $ext === 'PNG') {
                       $ca_type = "Image";
                   } else if ($ext === 'doc' || $ext === 'docx' || $ext === 'DOC' || $ext === 'DOCX') {
                       $ca_type = "Doc";
                   } else {
                       $ca_type = "Pdf";
                   }
                   if ($ext === 'jpeg' || $ext === 'jpg' || $ext === 'JPG' || $ext === 'JPEG' || $ext === 'png' || $ext === 'PNG' || $ext === 'doc' || $ext === 'docx' || $ext === 'DOC' || $ext === 'DOCX' || $ext === 'pdf' || $ext === 'PDF') {
                       $imageName = date('YmdHis') . "OL" . rand(5, 10) . '.' . $request->ca_name->getClientOriginalExtension();
                       $request->ca_name->move(('upload/backend/message_attachment/'), $imageName);
                       $conversationAttachment->ca_name = $imageName;
                       $conversationAttachment->ca_type = $ca_type;
                   }
               }
               $conversationAttachment->save();
               return redirect('portal/message/texhnicianSent/list')->with('success', 'Congratulations, message send successfully');

           }

           }
   }

   public  function viewInboxMessageTecnicains($id){
       $dataList = ConversiationModel::leftJoin('conversation_attachment', 'conversation.conversation_track_id', 'conversation_attachment.ca_conversation_id')
           ->leftJoin('technicians', 'conversation.conversation_sent_from', 'technicians.tec_trackid')
           ->where('conversation_sent_to', Auth::user()->users_track_id)
           ->where('conversation_track_id', $id)
           ->select('conversation.*', 'conversation_attachment.*')
           ->first();
       $conversationRead = ConversiationModel::where('conversation_track_id', $id)->first();
       $conversationRead->conversation_status = 'Read';
       if ($conversationRead->save()) {
           return view('backend.message.techniciansInboxMessageView', compact('dataList'));
       }
   }

    public  function singleMessageView($id){
        $conversationRead = ConversiationModel::where('conversation_track_id', $id)->first();
        $conversationRead->conversation_status = 'Read';
        if ($conversationRead->save()) {
            return redirect('/portal/message/techniciansInbox/list');
        }
    }

    public function singleMessageViewManager($id){
        $conversationRead = ConversiationModel::where('conversation_track_id', $id)->first();
        $conversationRead->conversation_status = 'Read';
        if ($conversationRead->save()) {
            return redirect('/portal/message/inbox/messagelist');
        }
    }
}
