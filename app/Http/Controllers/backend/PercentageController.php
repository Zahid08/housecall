<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use App\LeadInformationModel;
use App\PercentageModel;
use App\PercentageTechnicians;
use App\RevenueCalculated;
use App\TechniciansModel;
use App\TrackidModel;
use App\User;
use Couchbase\UserSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class PercentageController extends Controller
{
    public function percentageSetup()
    {
        $dataList = PercentageModel::orderBy('created_at', 'desc')->get();
        $dataListtec = PercentageTechnicians::orderBy('created_at', 'desc')->get();
        $centername=User::where('users_type','Center')->get();
        $techniciansname=User::where('users_type','Tc')->get();
        return view('backend.revenue.setupPercentage', compact('dataList','dataListtec','centername','techniciansname'));
    }

    public function newpercentageSetup(Request $request)
    {
        $center_name = Input::get('center_name');
        $center_percentage = Input::get('percentage_cn');
//        $Technicians_name = Input::get('technicains_name');
//        $Technicians_percentage = Input::get('percentage_tc');

        $errors = array();
        if (empty($center_percentage) || $center_percentage == '') {
            $errors['center_percentage'] = "center percentage required";
        }
        if (count($errors) > 0) {
            return redirect()->back()->withInput()->withErrors($errors)->with('errorArray', 'Array Error Occured ');
        } else {
            $randomNumber = new TrackidModel();
            $trackId = $randomNumber->trackId(5, 10) . "BI" . date("YmdHis");
            $object = new PercentageModel();
            $object->percentage_track_id = $trackId;
            $object->center_name = $center_name;
            $object->percentage_cn = $center_percentage;
//            $object->technicains_name = $Technicians_name;
            $object->status = "Inactive";
//            if (empty($Technicians_percentage)) {
//                $object->percentage_tc = 0;
//            } else {
//                $object->percentage_tc = $Technicians_percentage;
//            }

            $object->save();
            return redirect()->back()->with('success', 'successfully saved');
        }

    }

    public  function newpercentageSetupTech(Request $request){
        $users_track_id=Input::get('technicains_trackid');
        $data=User::where('users_track_id',$users_track_id)->first();
        $Technicians_name =$data->users_name;
        $Technicians_percentage = Input::get('percentage_tc');

        $errors = array();
        if (empty($Technicians_percentage) || $Technicians_percentage == '') {
            $errors['percentage_tc'] = "technicians  percentage required";
        }
        if (count($errors) > 0) {
            return redirect()->back()->withInput()->withErrors($errors)->with('errorArray', 'Array Error Occured ');
        } else {

            $randomNumber = new TrackidModel();
            $trackId = $randomNumber->trackId(5, 10) . "BI" . date("YmdHis");
            $object = new PercentageTechnicians();
            $object->percentage_track_id = $trackId;
            $object->technicains_name = $Technicians_name;
            $object->status = "Active";
            $object->percentage_tc = $Technicians_percentage;
            $object->save();
            return redirect()->back()->with('success', 'successfully saved');
        }
    }

    public function confirmedLeadPercentage(Request $request)
    {
        $lead_trackid = $request->lead_trackid;
        $data = LeadInformationModel::where('lead_trackid', $lead_trackid)->first();
        $technicinas_track_id = $data->tec_trackid;
        $manager_track_id = Auth::user()->users_track_id;
        $amount = $data->lead_ammount;

         $centertrackid=$data->center_name;
         $centerdata=User::where('users_track_id',$centertrackid)->first();
         $centername=$centerdata->users_name;



        $tec_trackid=$data->tec_trackid;
        $techniciansdata=User::where('users_track_id',$tec_trackid)->first();
        $tecname=$techniciansdata->users_name;

        $dataList = PercentageTechnicians::where('status', 'Active')
            ->where('technicains_name','=',$tecname)
            ->first();

         $technicinas_percentage = $dataList->percentage_tc;

        $dataList1 = PercentageModel::where('center_name','=',$centerdata->users_name)
            ->first();
        if ($dataList1) {
            $center_percentage= $dataList1->percentage_cn;
        }
        else{
            $center_percentage =0;
        }

        $technicians_gain_taka = ($amount * $technicinas_percentage) / 100;
        $centar_gain_taka =  $center_percentage;
        $total_taka = $amount - ($technicians_gain_taka + $centar_gain_taka);

        $revnue_counbtrow = RevenueCalculated::orderBy('created_at', 'desc')->count();

        $existence=RevenueCalculated::where('lead_track_id',$lead_trackid)->exists();
        if($existence){
            return redirect()->back()->with('error', 'This lead already paid !! Pleas check another');
        }
        else{
            $revenue = new RevenueCalculated();
            $revenue->lead_track_id = $lead_trackid;
            $revenue->manager_track_id = $manager_track_id;
            $revenue->technicians_track_id = $technicinas_track_id;
            $revenue->amount_given = $technicians_gain_taka;
            $revenue->center_name = $data->center_name;
            $revenue->center_taken = $centar_gain_taka;
            $revenue->save();

            //save total taka
            $savefinaltaka = LeadInformationModel::where('lead_trackid', $lead_trackid)->first();
            $savefinaltaka->lead_ammount = $total_taka;
            $savefinaltaka->Qm_paid_status = "Paid";
            $savefinaltaka->save();
            return redirect()->back()->with('success', 'successfully confirmed');
        }


//        }
//        else{
//            $revenuedata=RevenueCalculated::where('technicians_track_id',$technicinas_track_id)->first();
//           $returndata=$revenuedata->exists();
//           if($revenuedata)
//           {
//               $previousamount=$revenuedata->amount_given;
//               $revenuedata->amount_given=($previousamount+$technicians_gain_taka);
//
//               $revenue=new RevenueCalculated();
//               $revenue->lead_track_id=$lead_trackid;
//               $revenue->manager_track_id=$manager_track_id;
//               $revenue->technicians_track_id=$technicinas_track_id;
//               $revenue->amount_given=$revenuedata;
//               $revenue->save();
//           }
//           else{
//               return "False";
//           }
//
//
//            //save total taka
//            $savefinaltaka=LeadInformationModel::where('lead_trackid',$lead_trackid)->first();
//            $savefinaltaka->lead_ammount=$total_taka;
//            $savefinaltaka->Qm_paid_status="Paid";
//            $savefinaltaka->save();
//            return redirect()->back()->with('success','successfully confirmed');


    }
    //super admin porta;
    public function inactivePercentage(Request $request){
           $percentage_track_id=Input::get('percentage_track_id');
           $dataList=PercentageModel::where('percentage_track_id',$percentage_track_id)->first();
           $dataList->status="Inactive";
           $dataList->save();
           return redirect()->back();
    }

    public  function inactivePercentagetec(Request $request){
        $percentage_track_id=Input::get('percentage_track_id');
        $dataList=PercentageTechnicians::where('percentage_track_id',$percentage_track_id)->first();
        $dataList->status="Inactive";
        $dataList->save();
        return redirect()->back();
    }
    //super admin
    public function activePercentage(Request $request){
        $percentage_track_id=Input::get('percentage_track_id');
        $dataList=PercentageModel::where('percentage_track_id',$percentage_track_id)->first();
        $dataList->status="Active";
        $dataList->save();
        return redirect()->back();
    }

    public function activePercentagetec(Request $request){
        $percentage_track_id=Input::get('percentage_track_id');
        $dataList=PercentageTechnicians::where('percentage_track_id',$percentage_track_id)->first();
        $dataList->status="Active";
        $dataList->save();
        return redirect()->back();
    }
    //supar admin
    public function removePercentage(Request $request){
        $percentage_track_id=Input::get('percentage_track_id');
        $dataList=PercentageModel::where('percentage_track_id',$percentage_track_id)->first();
        $dataList->delete();
        return redirect()->back();
    }
    public  function removePercentagetec(Request $request){
        $percentage_track_id=Input::get('percentage_track_id');
        $dataList=PercentageTechnicians::where('percentage_track_id',$percentage_track_id)->first();
        $dataList->delete();
        return redirect()->back();
    }

    public  function revenueList(){
        $dataList= LeadInformationModel::where('job_done', 'Yes')->get();

        //dd($dataList);
        return view('backend.revenue.revenueList',compact('dataList'));
    }

    //manager portal
    public  function revenueListM(){
        $dataList= LeadInformationModel::where('job_done', 'Yes')
            ->where('Qm_paid_status','Paid')
            ->get();
        return view('backend.manager.revenueList',compact('dataList'));
    }
}
