<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use File;


class ProfileController extends Controller
{

    public function forgotPasswordLink(Request $request)
    {

        $users_email = Input::get('email');
        $errors = array();

        /*
         * Check password is empty or not
         */
        if (empty($users_email) || $users_email == '') {
            $errors['users_email'] = "E-mail required";
        }
        if (count($errors) > 1) {
            return redirect()->back()->withInput()->withErrors($errors)->with('errorArray', 'Array Error Occured');
        } else {
            $checkExistsEmail = User::where('email', $users_email)->first();
            if (!empty($checkExistsEmail)) {
                $users_name = $checkExistsEmail->users_name;
                $users_track_id = $checkExistsEmail->users_track_id;
                if (!empty($users_track_id)) {
                    Mail::send('backend.profile.reset', array(
                        'link' => URL::to('portal/profile/resetPassword/' . $users_track_id),
                        'users_name' => $users_name), function ($message) use ($users_name, $users_email) {
                        $message->from('housecall@brotherhoodinfotech.com', 'Housecall');
                        $message->to($users_email, $users_name)->subject('Reset your password Housecall  account');
                        Log::info('End of mail processing');
                    });
                    return redirect('/')->with('success', 'Link send to you email and reset your password');
                }
            } else {
                return redirect()->back()->withInput()->with('error', 'Email not exists');
            }
        }
    }


//Mail feedback ufter link validation
    public function passwordResetPage($id) {
        $userId = User::where('users_track_id', $id)->first();
        if (empty($userId)) {
            return redirect()->intended('pagenotfound');
        } else {
            return view('backend.profile.restPassword', compact('userId'));
        }
    }

    public function passwordUpdate() {
        $users_track_id = Input::get('users_track_id');
        $password = Input::get('password');
        $re_password = Input::get('re_password');

        $errors = array();

        /*
         * Check password is empty or not
         */
        if (empty($password) || $password == '') {
            $errors['password'] = "New password required";
        }
        /*
         * Check retype password is empty or not
         */
        if (empty($re_password) || $re_password == '') {
            $errors['re_password'] = "Confirm new password required";
        }

        if (!empty($password)) {
            if (!empty($re_password)) {
                if ($password != $re_password) {
                    $errors['password'] = "Password does not match";
                }
            }
            /*
             * Check password length
             */

            if (strlen($password) < 6) {
                $errors['password'] = "Password must be longer than 5 characters in length";
            }

            if (strlen($password) > 15) {
                $errors['password'] = "Password must be at least 15 characters in length";
            }
        }
        if (count($errors) > 0) {
            return redirect()->back()->withInput()->withErrors($errors)->with('errorArray', 'Array Error Occured ');
        } else {
            /*
             * update users password into database
             */
            $dataList = User::where('users_track_id', $users_track_id)->first();
            $dataList->password = bcrypt($password);
            $users_email=$dataList->email;
            $users_name=$dataList->users_name;
            if ($dataList->save()) {
                if(!empty($users_email)) {
//                    $text = "Hello " . $dataList->users_name . ", Your password successfully changed. Your Current Username : " . $dataList->email . " Password : " . $dataList->password;
//                    Mail::raw($text, function ($message) use ($users_email) {
//                        $message->from('housecall@brotherhoodinfotech.com', 'Housecall');
//                        $message->to($users_email)->subject('Confirmation Message');
//                        Log::info('End of mail processing');
//                    });

                    Mail::send('backend.profile.confirmedMessage', array(
                        'users_name' => $users_name,'users_email'=>$users_email,'password'=>$password), function($message) use ($users_name, $users_email) {
                        $message->from('housecall@brotherhoodinfotech.com', 'Housecall');
                        $message->to($users_email, $users_name)->subject('Confirmation Message');
                        Log::info('End of mail processing');
                    });

                }
                return redirect('/')->with('success','Successfully changed your password');
            }
        }
    }


    public  function CompleteProfile(Request $request){
        $errors = array();

        $users_track_id=Input::get('users_track_id');
        $users_phone_number=Input::get('users_phone_number');
        $users_gender=Input::get('users_gender');

        $users_birthdate=Input::get('users_birthdate');
        $users_country=Input::get('users_country');
        $users_nationalid=Input::get('users_nationalid');
        $users_present_address=Input::get('users_present_address');
        $users_permanent_address=Input::get('users_permanent_address');

        $number = ['011', '015', '016', '017', '018', '019'];
        $mobileNumber = str_split($users_phone_number, 3);
        $mobileNumber[0];

        if (!empty($users_phone_number)) {
            if (strlen($users_phone_number) != 11) {
                $errors['users_phone_number'] = "Phone number should be 11 digits";
            }
            /*
             * Checking phone number is numeric value or not
             */

            if (!is_numeric($users_phone_number)) {
                $errors['users_phone_number'] = "Phone number must be numeric value";
            }
            /*
             * Checking user moble number validation
             */

            if (!in_array($mobileNumber[0], $number)) {
                $errors['users_phone_number'] = "Phone number format does not match";
            }
        }


        if (empty($users_phone_number) || $users_phone_number == '') {
          $errors['users_phone_number'] = "phone number required";
        }
        if (empty($users_gender) || $users_gender == '') {
            $errors['users_gender'] = "gender required";
        }

        if (empty($users_birthdate) || $users_birthdate == '') {
            $errors['users_birthdate'] = "birthdate required";
        }
        if (empty($users_country) || $users_country == '') {
            $errors['users_country'] = "country name required";
        }

        if (empty($users_permanent_address) || $users_permanent_address == '') {
            $errors['users_permanent_address'] = "permanaent address required";
        }

        if (!empty($request->file('image'))) {
            if ($_FILES['image']['name']) {
                $allowed = array('jpg', 'JPG', 'jpeg', 'JPEG', 'PNG','png');
                $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                if (!in_array($ext, $allowed)) {
                    $errors['image'] = "Picture should be in JPG, JPEG, PNG format";
                }
                if (File::size(Input::file("image")) > 2048000) {
                    $errors['image'] = "Picture must be less than 2 MB";
                }
            }
        }

        if (count($errors) > 0) {
            return redirect()->back()->withInput()->withErrors($errors)->with('errorArray', 'Array Error Occured ');
        }
        else{
            $datalist=User::where('users_track_id',$users_track_id)->first();
            $datalist->users_phone_number=$users_phone_number;
            $datalist->users_nationalid=$users_nationalid;
            $datalist->users_gender=$users_gender;
            $datalist->users_birthdate=$users_birthdate;
            $datalist->users_country=$users_country;
            $datalist->users_present_address=$users_present_address;
            $datalist->users_permanent_address=$users_permanent_address;
            $datalist->users_profile_completeness="Yes";
            $image = $datalist->image;
            if (!empty($image)) {
                if ($_FILES['image']['name']) {
                    $userImage = public_path("upload/users/{$datalist->image}"); // get previous image from folder
                    if (File::exists($userImage)) { // unlink or remove previous image from folder
                        unlink($userImage);
                    }
                    $image = $request->file('image');
                    $imageName = date('YmdHis') . "BI" . rand(5, 10) . '.' . $request->image->getClientOriginalExtension();
                    $request->image->move(('upload/users'), $imageName);
                    $datalist->image = $imageName;
                } else {
                    $datalist->image = $datalist->image;
                }
            } else {
                if ($_FILES['image']['name']) {
                    $image = $request->file('image');
                    $imageName = date('YmdHis') . "BI" . rand(5, 10) . '.' . $request->image->getClientOriginalExtension();
                    $request->image->move(('upload/users'), $imageName);
                    $datalist->image = $imageName;
                }
            }
            $datalist->save();
            return redirect()->back();
        }

    }

    //super admin profile change
    public  function superAdminProfile(){

        return view('backend.profile.superAdminProfile');
    }
    public function updatePassword(){
        return view('backend.profile.updatePassword');
    }
    public  function updatePasswordConfirmed(Request $request){
        $dataList =User::where('users_track_id',Auth::user()->users_track_id)->first();
        $old_password = Input::get('old_password');
        $password = Input::get('password');
        $re_password = Input::get('re_password');

        $errors = array();
        /*
         * Check old password is empty or not
         */
        if (empty($old_password) || $old_password == '') {
            $errors[] = "Old password required";
        }
        /*
         * Check password is empty or not
         */
        if (empty($password) || $password == '') {
            $errors[] = "Password required";
        }
        /*
         * Check retype password is empty or not
         */
        if (empty($re_password) || $re_password == '') {
            $errors[] = "Confirm new password required";
        }
        /*
         * Check if password and confirm password matched or not
         */
        if (!empty($password)) {
            if (!empty($re_password)) {
                if ($password != $re_password) {
                    $errors[] = "Confirm password does not match with password ";
                }
            }
            /*
             * Check password length
             */

            if (strlen($password) < 6) {
                $errors[] = "Password must be longer than 5 characters in length";
            }

            if (strlen($password) > 15) {
                $errors[] = "Password must be at least 15 characters in length";
            }
        }
        if (count($errors) > 0) {
            return redirect()->back()->withInput()->withErrors($errors)->with('errorArray', 'Array Error Occured ');
        }
        else {

            if (Hash::check($request->get('old_password'), $dataList->password)) {
                if (Hash::check($request->get('password'), $dataList->password)) {
                    return redirect()->back()->withInput()->with('error', 'Sorry !!! This password was used before.Please Re enter diffrent password');
                } else {
                    $dataList->password = bcrypt($password);
                    $email = $dataList->email;
                    if ($dataList->save()) {
                        $text = "Hello " . $dataList->users_name . ", Your Password changed.Your current password " . $password;
                        Mail::raw($text, function ($message) use ($email) {
                            $message->from('housecall@brotherhoodinfotech.com', 'Housecall');
                            $message->to($email)->subject('Password Change');
                            Log::info('End of mail processing');
                        });
                    }
                    return redirect()->back()->with('success', 'Password changed successfully ,Give your reset password in your mail ,Thanks ');
                }

            }
        }
    }

    public function profileEdit(){
        $dataList =User::where('users_track_id',Auth::user()->users_track_id)->first();
        return view('backend.profile.profileEdit',compact('dataList'));
    }

    public function update(Request $request){

        $users_name = Input::get('users_name');
        $users_email = Input::get('email');
        $users_phone = Input::get('users_phone_number');

        $errors = array();
        $number = ['011', '015', '016', '017', '018', '019'];
        $mobileNumber = str_split($users_phone, 3);
        $mobileNumber[0];

        if (empty($users_name) || $users_name == '') {
            $errors['users_name'] = "Name required";
        }

        if (empty($users_phone) || $users_phone == '') {
            $errors['phone'] = "Phone number required";
        }

        if (empty($users_phone) || $users_phone == '') {
            $errors['users_phone_number'] = "Phone number required";
        }

        if (!empty($request->file('image'))) {
            if ($_FILES['image']['name']) {
                $allowed = array('jpg', 'JPG', 'jpeg', 'JPEG', 'PNG','png');
                $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                if (!in_array($ext, $allowed)) {
                    $errors['image'] = "Picture should be in JPG, JPEG, PNG format";
                }
                if (File::size(Input::file("image")) > 2048000) {
                    $errors['image'] = "Picture must be less than 2 MB";
                }
            }
        }

        /*
         * Checking user phone number digit length 11 character or not
         */
        if (!empty($users_phone)) {
            if (strlen($users_phone) != 11) {
                $errors['users_phone_number'] = "Phone number should be 11 digits";
            }
            /*
             * Checking phone number is numeric value or not
             */

            if (!is_numeric($users_phone)) {
                $errors['users_phone_number'] = "Phone number must be numeric value";
            }
            /*
             * Checking user moble number validation
             */

            if (!in_array($mobileNumber[0], $number)) {
                $errors['users_phone_number'] = "Phone number format does not match";
            }
        }

        if (empty($users_email) || $users_email == '') {
            $errors['email'] = "E-mail required";
        }

        if (!empty(Input::get('email'))) {
            if (!filter_var(Input::get('email'), FILTER_VALIDATE_EMAIL) === true) {
                $errors['email'] = "user e-mail is not valid";
            }
        }

        if (count($errors) > 0) {
            return redirect()->back()->withInput()->withErrors($errors)->with('errorArray', 'Array Error Occured ');
        }

        else {
            $datalist=User::where('users_track_id',Auth::user()->users_track_id)->first();
            $datalist->users_name=$users_name;
            $datalist->email=$users_email;
            $datalist->users_phone_number=$users_phone;
            $image = $datalist->image;
            if (!empty($image)) {
                if ($_FILES['image']['name']) {
                    $userImage = public_path("upload/users/{$datalist->image}"); // get previous image from folder
                    if (File::exists($userImage)) { // unlink or remove previous image from folder
                        unlink($userImage);
                    }
                    $image = $request->file('image');
                    $imageName = date('YmdHis') . "BI" . rand(5, 10) . '.' . $request->image->getClientOriginalExtension();
                    $request->image->move(('upload/users'), $imageName);
                    $datalist->image = $imageName;
                } else {
                    $datalist->image = $datalist->image;
                }
            } else {
                if ($_FILES['image']['name']) {
                    $image = $request->file('image');
                    $imageName = date('YmdHis') . "BI" . rand(5, 10) . '.' . $request->image->getClientOriginalExtension();
                    $request->image->move(('upload/users'), $imageName);
                    $datalist->image = $imageName;
                }
            }
            $datalist->save();
            return redirect()->back()->with('success','successfully updated your profile');

        }

    }
}
