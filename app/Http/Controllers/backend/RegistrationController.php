<?php

namespace App\Http\Controllers\backend;
use App\BrowserModelBI;
use App\Http\Controllers\Controller;
use App\TrackidModel;
use App\User;
use App\UserActivity;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class RegistrationController extends Controller
{

    public function registration(){
        return view('backend.registration');
    }

    public  function registrationUser(Request $request){
        $users_name = Input::get('users_name');
        $users_email = Input::get('users_email');
        $password = Input::get('users_password');
        $re_password = Input::get('re_password');

        $errors = array();
        /*
         * Checking user name is empty or not
         */
        if (empty($users_name) || $users_name == '') {
            $errors['users_name'] = "user name required";
        }
        /*
         * Checking user phone is empty or not
         */
        if (empty($users_email) || $users_email == '') {
            $errors['users_email'] = "user e-mail required";
        }
        /*
         * Checking email empty or not
         */
        if (!empty(Input::get('users_email'))) {
            if (!filter_var(Input::get('users_email'), FILTER_VALIDATE_EMAIL) === true) {
                $errors['users_email'] = "user e-mail is not valid";
            } else {
                $checkExistsEmail = User::where('email', Input::get('users_email'))->exists();
                if ($checkExistsEmail) {
                    $errors['users_email'] = "user e-mail already exists";
                }
            }
        }

        /*
         * Check password is empty or not
         */
        if (empty($password) || $password == '') {
            $errors['users_password'] = "Password required";
        }
        /*
         * Check retype password is empty or not
         */
        if (empty($re_password) || $re_password == '') {
            $errors['re_password'] = "Confirm password required";
        }
        /*
         * Check if password and confirm password matched or not
         */
        if (!empty($password)) {
            if (!empty($re_password)) {
                if ($password != $re_password) {
                    $errors['re_password'] = "Password does not match";
                }
            }
            /*
             * Check password length
             */
            if (strlen($password) < 6) {
                $errors['password'] = "Password must be longer than 5 characters in length";
            }

            if (strlen($password) > 15) {
                $errors['password'] = "Password must be at least 15 characters in length";
            }
        }
        if (count($errors) > 0) {
            return redirect()->back()->withInput()->withErrors($errors)->with('errorArray', 'Array Error Occured');
        }
        else{

            $randomNumber = new TrackidModel();
            $trackId = $randomNumber->trackId(5, 10) . "BI" . date("YmdHis");
            $obj = new User();
            $obj->users_name = $users_name;
            $obj->email = $users_email;
            $obj->users_track_id = $trackId;
            $obj->created_at = Carbon::now();
            $obj->users_veryfication_status = 'Inactive';
            $obj->password = bcrypt($password);
            if ($obj->save()) {
                if (!empty(Input::get('users_email'))) {
                    $text = "Hello " . $users_name . ", You are registered into housecall. Your account isn't active yet. please contact brotherhood for more detail.";
                    Mail::raw($text, function($message)use($users_email) {
                        $message->from('infogotegary@gmail.com', 'Housecall');
                        $message->to($users_email)->subject('Registration confirmation');
                        Log::info('End of mail processing');
                    });
                }

                /*
                 * Getting IP address
                 */
                $ipAddress = $_SERVER['REMOTE_ADDR'];
                /*
                 * Getting browser information
                 */
                $browser = new BrowserModelBI();
                $browserInfo = $browser->getBrowser();
                $browserName = $browserInfo['name'];
                $browserVersion = $browserInfo['version'];

                $objActivity = new UserActivity();
                $objActivity->users_activity_browser = $browserName;
                $objActivity->users_activity_browser_version = $browserVersion;
                $objActivity->users_activity_ip = $ipAddress;
                $objActivity->users_activity_track_id = $obj->users_track_id;
                $objActivity->created_at = Carbon::now();
                $objActivity->users_activity_details = 'Registration completed successfully';
                if ($objActivity->save()) {
                    return redirect('/')->with('success', 'Congratulations, your registration has been successfully completed.Please wait some times to active your account.Keep your password confidential. Thank you.');
                }
            } else {
                return redirect()->back()->with('error', 'Sorry !!! Something went wrong, please try again');
            }
        }


    }

//    public  function  registerUserlist(){
//        $dataList=DB::table('users')
//            ->where('users_veryfication_status','=','Inactive')->get();
//        return view('backend.registeruser.registerlist',['dataList'=>$dataList]);
//    }

    public  function registerdUser(Request $request){
        $users_track_id=Input::get('users_track_id');
        $designation=Input::get('designation');

        if($designation=="Qm"){
            $desg="Quality Manager";
        }
        else if ($designation=="Qt"){
            $desg="Quality Team ";
        }
        if($designation=="NULL"){
            return redirect()->back()->with('alert', 'Sorry !!! Please select Designation');
        }
        else{
            $dataList = User::where('users_track_id', $users_track_id)->first();
           $dataList->users_veryfication_status='Active';
            $dataList->users_type =$designation;
            $users_email=$dataList->email;
            if ($dataList->save()) {
                if (!empty($users_email)) {
                    $text = "Hello " . $dataList->users_name . ",Now You are registered as a member of ".$desg . " into our system. Please login with your valid email address and password ";
                    Mail::raw($text, function ($message) use ($users_email) {
                        $message->from('housecall@brotherhoodinfotech.com', 'Housecall');
                        $message->to($users_email)->subject('Registration confirmation');
                        Log::info('End of mail processing');
                    });
                }
                return redirect()->back()->with('success','Successfully changed your password');
            }
        }
    }

    public  function changeDesignation(Request $request){

        $users_track_id=$request->users_track_id;
        $designation=Input::get('designation');

        if($designation=="HR"){
            $desg="HR";
        }
        else if ($designation=="Qt"){
            $desg="Quality Team ";
        }
        else if ($designation=="Admin"){
            $desg="Admin ";
        }
        else if ($designation=="Qm"){
            $desg="Quality manager ";
        }
        if($designation=="NULL"){
            return redirect()->back()->with('alert', 'Sorry !!! Please select Designation');
        }
        else{
            $dataList = User::where('users_track_id', $users_track_id)->first();
            $dataList->users_veryfication_status='Active';
            $dataList->users_type =$designation;
            $users_email=$dataList->email;
            if ($dataList->save()) {
                if (!empty($users_email)) {
                    $text = "Hello " . $dataList->users_name . ",Now You are registered as a member of ".$desg . " into our system. Please login with your valid email address and password ";
                    Mail::raw($text, function ($message) use ($users_email) {
                        $message->from('housecall@brotherhoodinfotech.com', 'Housecall');
                        $message->to($users_email)->subject('Registration confirmation');
                        Log::info('End of mail processing');
                    });
                }
                return redirect()->back()->with('success','Successfully save designation');
            }
        }
    }
}
