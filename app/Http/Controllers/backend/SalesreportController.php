<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;

use App\LeadInformationModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class SalesreportController extends Controller
{
  public function salesReport(){
      $dataList= LeadInformationModel::where('job_done', 'Yes')->get();
      return view('backend.salesreport.salesreportcontent',compact('dataList'));
  }
  public  function LeadData(Request $request){
       $state = $_POST['state'];
      $status = $_POST['status'];

      if($state !="stateselect" &&  $status ==="status") {
          $dataList = LeadInformationModel::where('state', $state)->count();
          $balance = DB::table('lead_informations')->where('state', '=', $state)->sum('lead_ammount');
          $response = array('balance' => $balance, 'dataList' => $dataList,'status' => $status);
      }
      else if  ( $status !="status" && $state ==="stateselect" ){
              $dataList = LeadInformationModel::where('Qm_status', '=', $status)->count();
              $balance = DB::table('lead_informations')->where('Qm_status', '=', $status)->sum('lead_ammount');
              $response = array('balance' => $balance, 'dataList' => $dataList, 'status' => $status);
      }
      else if ($status !="status" && $state !="stateselect"){
          $dataList = LeadInformationModel::where('state', $state)
              ->where('Qm_status', '=', $status)->count();
              $balance = DB::table('lead_informations')->where('state', '=', $state)
                  ->where('Qm_status', '=', $status)
                  ->sum('lead_ammount');
              $response = array('balance' => $balance, 'dataList' => $dataList, 'status' => $status);
      }
      return response()->json($response);
  }

      public function detailsreport(Request $request){
          $state =Input::get('ustate');
           $status = Input::get('ustatus');

          if($state !="stateselect" &&  $status ==="status") {
              $dataList = LeadInformationModel::where('state','=', $state)->get();
          }
          else if  ( $status !="status" && $state ==="stateselect" ){
              $dataList = LeadInformationModel::where('Qm_status', '=', $status)->get();
          }
          else{
              $dataList = LeadInformationModel::where('state', $state)
                  ->where('Qm_status', '=', $status)->get();

          }
          foreach ($dataList as $data){
              $userdata=\App\User::where('users_track_id',$data->center_name)->first();
              $commision=\App\PercentageModel::where('center_name','=',$userdata->users_name)->exists();
          }

          if ($commision){
             $centercommison=$commision;
          }
          return  view('backend.salesreport.salesreportdetails',compact('dataList','centercommison'));

      }
}

