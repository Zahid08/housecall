<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;

use App\LeadInformationModel;
use App\TechniciansModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rules\In;

class TechnicianController extends Controller
{
    public  function cutomerlist(){
        $trackid=Auth::user()->users_track_id;
        $dataList= LeadInformationModel::where('tec_trackid', $trackid)
            ->where('job_done','No')
            ->get();
        return view('backend.technicians.leadlist',['dataList'=>$dataList]);
    }

    public function detailsLead($id){
        $datalist=LeadInformationModel::where('lead_trackid',$id)->first();
        return  view('backend.technicians.detailsLead',compact('datalist'));
    }

    public  function jobDonelist(){

        $trackid=Auth::user()->users_track_id;
        $dataList= LeadInformationModel::where('tec_trackid', $trackid)
            ->where('job_done','Yes')
            ->where('Tc_status','Pass')
            ->get();
        return view('backend.technicians.jobDonelist',['dataList'=>$dataList]);
    }

    public function jobdone(Request $request){
        $lead_trackid=Input::get('lead_trackid');
        $dataList= LeadInformationModel::where('lead_trackid', $lead_trackid)->first();
        $dataList->job_done="Yes";
        $dataList->lead_ammount=0;
        $dataList->Save();
        return  redirect()->back()->with('success','Successfullly jobe done');

    }

    public function paidAmmount(Request $request){
        $lead_trackid=$request->lead_trackid;
        $amount=$request->lead_ammount;
        $datalist=LeadInformationModel::where('lead_trackid',$lead_trackid)->first();
         $datalist->lead_ammount=$amount;
        $datalist->job_done="Yes";
         $datalist->save();
      return redirect()->back()->with('success',"successfully paid");

    }
    //qm dashboard
    public function inactiveTechnicians(Request $request){
       $tec_trackid=Input::get('tec_trackid');
       $fromuser=User::where('users_track_id',$tec_trackid)->first();
       $fromuser->users_veryfication_status="Inactive";
       $fromuser->save();
//       $dataList=TechniciansModel::where('tec_trackid',$tec_trackid)->first();
//       $dataList->tec_status="Inactive";
//       $dataList->save();
       return redirect()->back()->with('success',' Successfully inactive this technicians');
    }
    public  function activeTecnicians(Request $request){
        $tec_trackid=Input::get('tec_trackid');
        $fromuser=User::where('users_track_id',$tec_trackid)->first();
        $fromuser->users_veryfication_status="Active";
        $fromuser->save();

//        $dataList=TechniciansModel::where('tec_trackid',$tec_trackid)->first();
//        $dataList->tec_status="Active";
//        $dataList->save();
        return redirect()->back()->with('success','Successfully activet this technicians');
    }
    public function removeTechnicians(Request $request){
        $tec_trackid=Input::get('tec_trackid');
        $fromuser=User::where('users_track_id',$tec_trackid)->first();
        $fromuser->delete();
        $dataList=TechniciansModel::where('tec_trackid',$tec_trackid)->first();
        $dataList->delete();

        return redirect()->back()->with('success','Successfully  Remove this technicians');
    }

    public  function changeStatus(Request $request){
        $lead_trackid=Input::get('lead_trackid');
        $Tc_status=Input::get('Tc_status');
        $Tc_comments=Input::get('Tc_comments');

        if ($Tc_status==='Pass') {
            $datalist = LeadInformationModel::where('lead_trackid', $lead_trackid)->first();
            $datalist->Tc_status = $Tc_status;
            $datalist->save();
            return redirect('/portal/tecDashboard/leadlist')->with('success','SUccessfully changed');
        }

        else{
            $datalist = LeadInformationModel::where('lead_trackid', $lead_trackid)->first();
            $datalist->Tc_status = $Tc_status;
            $datalist->Tc_comments=$Tc_comments;
            $datalist->save();
            return redirect('/portal/tecDashboard/leadlist')->with('success','SUccessfully changed');
        }

    }
}
