<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class TechnicianMiddleware
{

    public function handle($request, Closure $next) {
        if (!empty(Auth::user()->users_type)) {
            if (Auth::user()->users_type === 'Tc') {
                return $next($request);
            } else {
                Auth::logout();
                return redirect()->intended('pagenotfound');
            }
        } else {
            Auth::logout();
            return redirect()->intended('pagenotfound');
        }
    }
}
