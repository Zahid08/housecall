<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class LeadInformationModel extends Model
{
    use Notifiable;

    protected $table = "lead_informations";
    protected $primaryKey = 'lead_id';
    protected $fillable = ['lead_trackid','tec_name','call_date','Tc_status','Tc_comments','Qm_paid_status','lead_ammount','job_done','tec_trackid','Qt_name','Timestamp','recodinglink','center_name', 'agent_name','owners_name','phone_number','emergency_number','street_address','city','state','postal_code','appointment_date','appointment_time','Colsers_name','comments','Qt_status','Qt_comments','Qm_status'];
    protected $guarded = ['lead_id'];

}
