<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class PercentageModel extends Model
{
    use Notifiable;

    protected $table = "percentage_stup";
    protected $primaryKey = 'percentage_id';
    protected $fillable = ['percentage_track_id','status','center_name', 'percentage_cn','technicains_name','percentage_tc'];
    protected $guarded = ['percentage_id'];
}
