<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class PercentageTechnicians extends Model
{
    use Notifiable;

    protected $table = "percentage_tech";
    protected $primaryKey = 'percentage_id';
    protected $fillable = ['percentage_track_id','status','technicains_name','percentage_tc'];
    protected $guarded = ['percentage_id'];
}
