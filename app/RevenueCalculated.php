<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class RevenueCalculated extends Model
{
    use Notifiable;
    protected $table = "revenues";
    protected $primaryKey = 'revenue_id';
    protected $fillable = ['lead_track_id','manager_track_id', 'technicians_track_id','amount_given','center_name','center_taken'];
    protected $guarded = ['revenue_id'];
}
