<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class TechniciansModel extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'technicians';
    protected $primaryKey = 'tec_id';
    protected $guraded = ['tec_id'];
    protected $fillable = [
        'tec_name',
        'tec_trackid',
        'manager_trackid',
        'email',
        'tec_phone_number',
        'state',
        'tec_gender',
        'tec_birthdate',
        'tec_country',
        'tec_street_address',
        'tec_status',
        'assign_leadtrackid'
    ];
}
