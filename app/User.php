<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $primaryKey = 'users_id';
    protected $guraded = ['users_id'];
    protected $fillable = [
        'users_name',
        'email',
        'image',
        'password',
        'users_phone_number',
        'users_nationalid',
        'users_gender',
        'users_birthdate',
        'users_country',
        'users_present_address',
        'users_permanent_address',
        'users_profile_completeness',
        'users_veryfication_status',
        'users_track_id',
        'users_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'users_password', 'remember_token',
    ];

}