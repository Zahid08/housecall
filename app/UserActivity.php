<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class UserActivity extends Model
{
    use Notifiable;

    protected $table = "users_activity";
    protected $primaryKey = 'users_activity_id';
    protected $fillable = ['users_activity_browser','user_name', 'users_activity_browser_version', 'users_activity_ip', 'users_activity_track_id', 'users_activity_details'];
    protected $guarded = ['users_activity_id'];

}
