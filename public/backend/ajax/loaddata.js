$(function(){

    $('#statelist,#status').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '/ajax/leadinfo',
                dataType: 'json',
                data:{
                    state: $('#statelist').val(),
                    status: $('#status').val()
                },
                success: function(response){
                    $('#displayInfo').show('slow');
                    if($('#statelist').val()=='stateselect'){
                        $('#statename').html('None');
                    }
                    else{
                        $('#statename').html($('#statelist').val());
                    }
                    if($('#status').val()=="status"){
                        $('#sts').html('None');
                    }
                    else {
                        $('#sts').html($('#status').val());
                    }
                    $('#ustate').val($('#statelist').val());
                    $('#ujstatus').val($('#status').val());

                    $('#totallead').html(response.dataList);
                    $('#balance').html(response.balance);
                }
            });

    });
})




