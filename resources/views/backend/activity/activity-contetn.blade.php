<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@extends('layout.backend.master')
@section('content')

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12 " >
                    <div class="panel-group">
                        <!--Dashboard Section-->
                        <div class="panel panel-primary">

                            <div class="panel-heading" id="" style="background-color:#537171; color: white;"> <i class="fa fa-list"></i> <SPAN>Activity log</SPAN>

                            </div>
                            @if (session('success'))
                                <div class="alert alert-success" id="success-alert">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>{{ session('success') }}</strong>
                                </div>
                        @endif

                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                    <tr>
                                        <th>Serial</th>
                                        <th>Browser</th>
                                        <th>Browser Version</th>
                                        <th>User Activity IP</th>
                                        <th>Users Activity Details</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $count = 1;
                                    @endphp
                                    @if(!empty($dataList))
                                        @foreach($dataList as $data)

                                            <tr class="odd gradeX">
                                                <td>{{$count}}</td>
                                                <td>{{$data->users_activity_browser}}</td>
                                                <td>{{$data->users_activity_browser_version}}</td>
                                                <td>{{$data->users_activity_ip}}</td>
                                                <td>{{$data->users_activity_details}}</td>
                                                <td>
                                                    <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#delete{{ $data->users_activity_id }}">
                                                        <span style="color: #489D48"><i class="fa fa-remove"></i></span>  Remove
                                                    </a>

                                                    <!--  Model For Delete link item-->
                                                    <div id="delete{{ $data->users_activity_id }}" class="modal fade" role="dialog">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <form method="POST" action="{{ URL::to('/portal/activitylog/remove') }}">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="users_activity_id" id="house_item_track_id" value="{{ $data->users_activity_id }}"/>
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h5 class="modal-title" style="text-align: center;">Do you want to remove this activity?</h5>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-danger center-block"><i class="fa fa-trash"></i>&nbsp;Remove</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div> <!-- End delete Model -->

                                                </td>

                                            </tr>

                                            @php
                                                $count++;
                                            @endphp
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                                {{ $dataList->links() }}
                            </div>

                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <script>
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#houseList').DataTable({
                "aaSorting": []
            });
        });
    </script>

@endsection