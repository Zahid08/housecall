<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>


@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        @if (session('success'))
                            <div class="alert alert-success" id="success-alert">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>{{ session('success') }}</strong>
                            </div>
                    @endif

                        <!--Dashboard Section-->
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"><i class="fa fa-plus"></i> <SPAN>Link Generate</SPAN></div>
                            <fieldset style="border: 1px solid #537171 !important;border-radius: 0px;">
                                <div class="panel-body" id="testApp">
                                    <!--Profile Section  Section-->
                                    <div class="panel-body">

                                        <form id="loginForm" method="POST" action="{{url('/portal/agentlinkgenerate')}}">
                                            {{csrf_field()}}
                                            <div class="col-sm-12">
                                                <div class="col-sm-6 form-group">
                                                    <label for="link_name">Center Name :<span class="mark">*</span> </label>
                                                    <select class="form-control" name="link_name">
                                                       @foreach($centername as $data)
                                                        <option value="{{$data->users_track_id}}">{{$data->users_name}}</option>
                                                           @endforeach
                                                    </select>
                                                    {{--<input type="text" class="form-control requiredOL" name="link_name" id="link_name" value=""  />--}}
                                                    <small class="text-danger"></small>
                                                </div>
                                                <div class="col-sm-2 form-group" style="margin-top: 20px;">
                                                    <button class="btn btn-default btn-block " style="background-color: #537171;border-color: #537171;color: white" type="submit" id="btnLogin" name="btnLogin"> Generate Link</button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                    <!--End Profile Section -->

                                </div>
                            </fieldset>
                        </div>
                        <!--Dashboard Section end-->

                        <!--Dashboard Section-->
                        <div class="panel panel-primary" style="margin-top: 70px;">
                            <div class="panel-heading panel-style"><i class="fa fa-list"></i> <SPAN>List Of Link</SPAN></div>
                            <fieldset style="border: 1px solid #537171 !important;border-radius: 0px;">
                                <div class="panel-body" id="testApp">


                                    <!-- /.panel-heading -->
                                    <div class="panel-body">
                                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                            <tr>
                                                <th>Serial</th>
                                                <th>Center Name</th>
                                                <th>Link Name</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php
                                                $count = 1;
                                            @endphp
                                            @if(!empty($dataList))
                                                @foreach($dataList as $data)

                                            <tr class="odd gradeX">
                                                <td>{{$count}}</td>
                                                <td>
                                                    <?php
                                                    $user=\App\User::where('users_track_id',$data->link_name)->first();
                                                    $centername=$user->users_name;
                                                    ?>
                                                    {{$centername}}
                                                </td>
                                                <td>http://housecall.brotherhoodinfotech.com/linktrack/{{$data->link_name}}/{{$data->link_trackid}}</td>
                                                <td>
                                                    @if($data->status === 'Active')
                                                        <span class="label label-success" style="color: #fff;font-size: 10px"> Active</span>
                                                    @else
                                                        <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Inactive</span>
                                                    @endif

                                                </td>
                                                <td>
                                                    @if($data->status === 'Active')
                                                        <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#inActive{{ $data->link_trackid }}">
                                                            <span style="color: #C4302E"><i class="fa fa-ban"></i></span> Inactive
                                                        </a>
                                                    @elseif($data->status === 'Inactive')
                                                        <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#active{{ $data->link_trackid }}">
                                                            <span style="color: #489D48"><i class="fa fa-check"></i></span>  Active
                                                        </a>
                                                    @endif


                                                        <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#delete{{ $data->link_trackid }}">
                                                            <span style="color: #489D48"><i class="fa fa-remove"></i></span>  Remove
                                                        </a>

                                                        <!--  Model For Delete link item-->
                                                        <div id="delete{{ $data->link_trackid }}" class="modal fade" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <form method="POST" action="{{ URL::to('/portal/agentLink/remove') }}">
                                                                        {{ csrf_field() }}
                                                                        <input type="hidden" name="link_trackid" id="house_item_track_id" value="{{ $data->link_trackid }}"/>
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                            <h5 class="modal-title" style="text-align: center;">Are you want to remove this link item?</h5>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-danger center-block"><i class="fa fa-trash"></i>&nbsp;Remove</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div> <!-- End delete Model -->


                                                    <!--  Model For Inactive house item-->
                                                        <div id="inActive{{ $data->link_trackid }}" class="modal fade" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <form method="POST" action="{{ URL::to('/portal/agentLink/inActive') }}">
                                                                        {{ csrf_field() }}
                                                                        <input type="hidden" name="link_trackid" id="house_item_track_id" value="{{ $data->link_trackid }}"/>
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                            <h5 class="modal-title" style="text-align: center;">Are you want to inactive this link item?</h5>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-danger center-block"><i class="fa fa-trash"></i>&nbsp;Inactive</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div> <!-- End Inactive Model -->
                                                        <!-- Modal For Active house item  -->
                                                        <div id="active{{ $data->link_trackid }}" class="modal fade" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <form method="POST" action="{{ URL::to('/portal/agentLink/active') }}">
                                                                        {{ csrf_field() }}
                                                                        <input type="hidden" name="link_trackid" id="house_item_track_id" value="{{ $data->link_trackid }}"/>
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                            <h5 class="modal-title" style="text-align: center;">Are you want to active this link item?</h5>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-success center-block"><i class="fa fa-check"></i>&nbsp;Active</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div> <!--- End Active Modal -->




                                                </td>

                                                </tr>

                                            @php
                                                $count++;
                                            @endphp
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </fieldset>
                        </div>
                        <!--Dashboard Section end-->

                    </div>
                </div>
            </div>
        </section>
    </div>


    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script>
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
        });
    </script>
@endsection

