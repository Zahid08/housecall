<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>
@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"><i class="fa fa-list"></i> <SPAN>Revenue  List <label style="margin-left: 750px;color: white;">Total Revenue :
                                        <?php
                                        $balance = DB::table('revenues')->where('center_name',\Illuminate\Support\Facades\Auth::user()->users_track_id)->sum('center_taken');
                                        ?>
                                        <span class="label label-success" style="color: #fff;font-size: 10px">{{$balance}}</span>
                                    </label></SPAN></div>
                            <fieldset style="border: 1px solid #435452 !important;border-radius: 0px;">
                                <div class="panel-body" style="padding: 0px;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- Alert Section-->
                                            @if (session('errorArray'))
                                                <div class="alert alert-danger">
                                                    @foreach($errors->all() AS $key => $value)
                                                        <strong><i class="fa fa-warning"></i> {{ $value }}</strong><br>
                                                    @endforeach
                                                </div>
                                            @endif
                                            @if (session('error'))
                                                <div class="alert alert-danger"  id="error">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('error') }}</strong>
                                                </div>
                                            @endif
                                            @if (session('success'))
                                                <div class="alert alert-success"  id="success">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('success') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="box-body table-responsive no-padding">
                                        <table id="houseList" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                            <thead style="background: #537171;color: white;">
                                            <tr>
                                                <th> CenterName  </th>
                                                <th> Commision </th>
                                                <th> OwnerName </th>
                                                <th> Paid Status </th>
                                                <th>  Lead Status </th>
                                                <th>  Action </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($dataList))
                                                @foreach($dataList as $data)
                                                    <tr>
                                                        <td>
                                                            <?php
                                                            $centername=$data->center_name;
                                                            $userdata=\App\User::where('users_track_id',$data->center_name)->first();
                                                            $centartaken=\App\RevenueCalculated::where('center_name',$data->center_name)->first();
                                                            ?>
                                                            <span class="label label-default" style="color: #fff;font-size: 10px"> {{$userdata->users_name}}
                                                            </span>

                                                        </td>
                                                        <td>
                                                            @if(!empty($centartaken))
                                                            {{$centartaken->center_taken}}

                                                            @endif
                                                        </td>
                                                        <td>{{ $data->owners_name }}</td>
                                                        <td>
                                                            <span class="label label-default" style="color: #fff;font-size: 10px"> {{$data->Qm_paid_status}}</span>
                                                        </td>
                                                        <td>
                                                            <span class="label label-default" style="color: #fff;font-size: 10px"> {{$data->Qm_status}}</span>
                                                        </td>
                                                        <td>
                                                            <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="{{url('/portal/customersinformation/details/center/'. $data->lead_trackid)}}"  style="color: red;width: 30px;">
                                                                <span style="color: #C53431"><i class="fa fa-list"></i> </span>Details
                                                            </a>&nbsp;

                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                            <tfoot>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <script>
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#houseList').DataTable({
                "aaSorting": []
            });
        });
    </script>

@endsection