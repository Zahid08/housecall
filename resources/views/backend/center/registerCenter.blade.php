<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <!--Dashboard Section-->
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"><i class="fa fa-plus"></i> <SPAN>Registation Center  </SPAN></div>
                            <fieldset style="border: 1px solid #537171 !important;border-radius: 0px;">
                                <div class="panel-body" id="testApp">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success-alert">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>{{ session('success') }}</strong>
                                        </div>
                                    @endif

                                    <form id="loginForm" method="POST" action="{{url('/portal/dashboard/centerRegistration/add')}}">
                                        {{csrf_field()}}
<div class="col-sm-12">
    <div class="col-sm-6">
                                        <div class="form-group {{ $errors->has('users_name') ? ' has-error' : '' }}">
                                            <label for="users_name">Center Name :<span class="mark">*</span> </label>
                                            <input type="text" class="form-control requiredOL" name="users_name" id="users_name" value="{{old('users_name')}}"  />
                                            <small class="text-danger">{{ $errors->first('users_name') }}</small>
                                        </div>
    </div>
    <div class="col-sm-6">
                                        <div class="form-group {{ $errors->has('users_email') ? ' has-error' : '' }}">
                                            <label for="users_email">Email Address<span class="mark">*</span> </label>
                                            <input type="text" class="form-control requiredOL" name="users_email" id="users_email" value="{{old('users_email')}}"   />
                                            <small class="text-danger">{{ $errors->first('users_email') }}</small>
                                        </div>
    </div>
</div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">

                                        <div class="form-group {{ $errors->has('users_password') ? ' has-error' : '' }}">
                                            <label for="password">Password<span class="mark">*</span></label>
                                            <input type="password" class="form-control requiredOL" name="users_password" id="password" value="" />
                                            <small class="text-danger">{{ $errors->first('users_password') }}</small>
                                        </div>
                                            </div>
                                            <div class="col-sm-6">

                                        <div class="form-group {{ $errors->has('re_password') ? ' has-error' : '' }}">
                                            <label for="re_password">Confirmed Password<span class="mark">*</span></label>
                                            <input type="password" class="form-control requiredOL" name="re_password" id="re_password" value="" />
                                            <small class="text-danger">{{ $errors->first('re_password') }}</small>
                                        </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <button class="btn btn-default btn-block " style="background-color: #537171;border-color: #537171;color: white" type="submit" id="btnLogin" name="btnLogin"> Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
<hr/>
                                <!--Dashboard Section-->
                                <div class="panel panel-primary" style="margin-top: 10px;">
                                    <div class="panel-heading panel-style"><i class="fa fa-list"></i> <SPAN>List Of Center</SPAN></div>
                                    <fieldset style="border: 1px solid #537171 !important;border-radius: 0px;">
                                        <div class="panel-body" id="testApp">


                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                    <thead>
                                                    <tr>
                                                        <td>Serial</td>
                                                        <th>Centername</th>
                                                        <th>Email Address</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @php
                                                        $count = 1;
                                                    @endphp
                                                    @if(!empty($dataList))
                                                        @foreach($dataList as $data)

                                                            <tr class="odd gradeX">
                                                                <td>{{$count}}</td>
                                                                <td>{{$data->users_name}}</td>
                                                                <td>{{$data->email}}</td>
                                                                <td>
                                                                    @if($data->users_veryfication_status === 'Active')
                                                                        <span class="label label-success" style="color: #fff;font-size: 10px"> Active</span>
                                                                    @else
                                                                        <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Inactive</span>
                                                                    @endif

                                                                </td>
                                                                <td>
                                                                    @if($data->users_veryfication_status === 'Active')
                                                                        <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#inActive{{ $data->users_track_id }}">
                                                                            <span style="color: #C4302E"><i class="fa fa-ban"></i></span> Inactive
                                                                        </a>
                                                                    @elseif($data->users_veryfication_status === 'Inactive')
                                                                        <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#active{{ $data->users_track_id }}">
                                                                            <span style="color: #489D48"><i class="fa fa-check"></i></span>  Active
                                                                        </a>
                                                                    @endif


                                                                    <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#delete{{ $data->users_track_id }}">
                                                                        <span style="color: #489D48"><i class="fa fa-remove"></i></span>  Remove
                                                                    </a>

                                                                    <!--  Model For Delete link item-->
                                                                    <div id="delete{{ $data->	users_track_id }}" class="modal fade" role="dialog">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <form method="POST" action="{{ URL::to('/portal/dashboard/centerRegistration/remove') }}">
                                                                                    {{ csrf_field() }}
                                                                                    <input type="hidden" name="users_track_id" id="house_item_track_id" value="{{ $data->users_track_id }}"/>
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                        <h5 class="modal-title" style="text-align: center;">Are you want to remove this link item?</h5>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-danger center-block"><i class="fa fa-trash"></i>&nbsp;Remove</button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div> <!-- End delete Model -->


                                                                    <!--  Model For Inactive house item-->
                                                                    <div id="inActive{{ $data->	users_track_id }}" class="modal fade" role="dialog">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <form method="POST" action="{{ URL::to('/portal/dashboard/centerRegistration/inActive') }}">
                                                                                    {{ csrf_field() }}
                                                                                    <input type="hidden" name="users_track_id" id="house_item_track_id" value="{{ $data->users_track_id }}"/>
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                        <h5 class="modal-title" style="text-align: center;">Are you want to inactive this link item?</h5>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-danger center-block"><i class="fa fa-trash"></i>&nbsp;Inactive</button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div> <!-- End Inactive Model -->
                                                                    <!-- Modal For Active house item  -->
                                                                    <div id="active{{ $data->	users_track_id }}" class="modal fade" role="dialog">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <form method="POST" action="{{ URL::to('/portal/dashboard/centerRegistration/active') }}">
                                                                                    {{ csrf_field() }}
                                                                                    <input type="hidden" name="users_track_id" id="house_item_track_id" value="{{ $data->	users_track_id }}"/>
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                        <h5 class="modal-title" style="text-align: center;">Are you want to active this link item?</h5>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-success center-block"><i class="fa fa-check"></i>&nbsp;Active</button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div> <!--- End Active Modal -->


                                                                </td>

                                                            </tr>

                                                            @php
                                                                $count++;
                                                            @endphp
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </fieldset>
                                </div>
                                <!--Dashboard Section end-->

                            </fieldset>
                        </div>
                        <!--Dashboard Section end-->
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script>
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
        });
    </script>

@endsection

