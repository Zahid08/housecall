<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12 " >
                    <div class="panel-group">
                        <!--Dashboard Section-->
                        <div class="panel panel-primary">
                            @if(Auth::user()->users_profile_completeness=="Yes")
                                <div class="panel-heading" id="" style="background-color:#537171; color: white;"> <i class="fa fa-dashboard"></i> <SPAN>Dashboard</SPAN>
                                    @else
                                        <div class="panel-heading" id="" style="background-color:#537171; color: white;"> <i class="fa fa-user"></i> <SPAN>Please Update Your Profile</SPAN>
                                            @endif
                                        </div>
                                        <div class="panel-body" id="testApp">

                                            <section class="content">
                                                <!-- Small boxes (Stat box) -->

                                                <div class="row">

                                                    <div class="col-lg-3 col-xs-6">
                                                        <!-- small box -->
                                                        <div class="small-box bg-green">
                                                            <div class="inner">
                                                                <h3>{{$dataList}}<sup style="font-size: 20px"></sup></h3>
                                                                <p>Total Lead</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-user"></i>
                                                            </div>
                                                            <a href="{{url('/portal/centerDashboard/leadinformation')}}" class="small-box-footer">More information <i class="fa fa-arrow-circle-right"></i></a>
                                                        </div>
                                                    </div>

                                                    <!-- ./col -->
                                                    <div class="col-lg-3 col-xs-6">
                                                        <!-- small box -->
                                                        <div class="small-box bg-yellow">
                                                            <div class="inner">
                                                                <h3>{{$balance}}</h3>
                                                                <p>Commision</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-dollar"></i>
                                                            </div>
                                                            <a href="#" class="small-box-footer">More information <i class="fa fa-arrow-circle-right"></i></a>
                                                        </div>
                                                    </div>

                                                </div>

                                            </section>
                                        </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection