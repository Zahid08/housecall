<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"> Detail of All Leads</div>
                            <div class="panel-body">
                                <div class="panel panel-primary">
                                    <div class="panel-body">
                                        <div class="col-md-4">
                                                <h5> Lead Status - <span class="label label-default"> Active  </span></h5>
                                        </div>
                                        <div class="col-md-4">
                                            <h5>Lead Created  - <span class="label label-default"> {{ $datalist->created_at->format('d-m-Y') }}</span></h5>
                                        </div>

                                        <div class="col-md-4">
                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#inActive{{ $datalist->	lead_trackid }}"  style="float: right;margin-top:5px;color: black;font-size: 16px;">
                                                <span class="label label-default"> Update Lead Information</span>
                                            </a>

                                            <div id="inActive{{ $datalist->	lead_trackid }}" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <form method="POST" action="{{ URL::to('/portal/leadformation/details/lead/update') }}">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="lead_trackid" id="house_item_track_id" value="{{ $datalist->lead_trackid }}"/>
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h5 class="modal-title" style="text-align: center;"></h5>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="col-sm-12">
                                                                    <div class="col-sm-6">
                                                                        <label for="" style="color: black;">Owner Name&nbsp;<span id="mark">*</span></label>
                                                                        <input type="text" value="{{$datalist->owners_name}}" name="owners_name" class="form-control" >
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <label style="color: black;">Phone Number</label>
                                                                        <input type="text" value="{{$datalist->	phone_number}}" name="phone_number" class="form-control" >
                                                                    </div>
                                                                </div>
                                                                <hr/>

                                                                <div class="col-sm-12" style="margin-top: 10px;">
                                                                    <div class="col-sm-6">
                                                                        <label style="color: black;">Alternative number</label>
                                                                        <input type="text" value="{{$datalist->emergency_number}}" name="emergency_number" class="form-control" >
                                                                    </div>

                                                                    <div class="col-sm-6">
                                                                        <label style="color: black;">street address</label>
                                                                        <input type="text" value="{{$datalist->	street_address}}" name="street_address" class="form-control" >
                                                                    </div>
                                                                </div>

                                                                <br>
                                                                <div class="col-sm-12" style="margin-top: 10px;">
                                                                    <div class="col-sm-6">
                                                                        <label style="color: black;">city</label>
                                                                        <input type="text" value="{{$datalist->city}}" name="city" class="form-control" >
                                                                    </div>

                                                                    <div class="col-sm-6">
                                                                        <label style="color: black;">state</label>
                                                                        <input type="text" value="{{$datalist->state}}" name="state" class="form-control" >
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12" style="margin-top: 10px;">
                                                                    <div class="col-sm-6">
                                                                        <label style="color: black;">postal code</label>
                                                                        <input type="text" value="{{$datalist->	postal_code}}" name="postal_code" class="form-control" >
                                                                    </div>

                                                                    <div class="col-sm-6">
                                                                        <label style="color: black;">closer name</label>
                                                                        <input type="text" value="{{$datalist->Colsers_name}}" name="Colsers_name" class="form-control" >
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-success center-block"><i class="fa fa-save"></i>&nbsp;Update</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div> <!-- End Inactive Model -->

                                        </div>

                                        <div class="clearfix"></div>
                                        @if(\Illuminate\Support\Facades\Auth::user()->users_type==="Super Admin" ||  \Illuminate\Support\Facades\Auth::user()->users_type==="Qt"  || \Illuminate\Support\Facades\Auth::user()->users_type==="Qm"  || \Illuminate\Support\Facades\Auth::user()->users_type==="HR" || \Illuminate\Support\Facades\Auth::user()->users_type==="Admin")
                                        <div class="panel panel-default">
                                            <div class="panel-heading" id="" style="background-color:#537171; color: white;">Agent Lead Information</div>
                                            <div class="panel-body">
                                                <div class="col-md-12">
                                                    <table class="table table-responsive table-striped table-bordered">
                                                        <tr>
                                                        <th> Time stamp  </th>
                                                        <th> {{$datalist->Timestamp}} </th>
                                                        </tr>
                                                        <tr>
                                                            <td>CenterName</td>
                                                            <td>
                                                                <?php
                                                                $centername=$datalist->center_name;
                                                                $userdata=\App\User::where('users_track_id',$datalist->center_name)->first();
                                                                $centartaken=\App\RevenueCalculated::where('center_name',$datalist->center_name)->first();
                                                                ?>
                                                                <span class="label label-default" style="color: #fff;font-size: 10px"> {{$userdata->users_name}}
                                                            </span>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <th> Agent Name  </th>
                                                            <th> {{$datalist->agent_name}}</th>
                                                        </tr>
                                                        <tr>
                                                            <td>Owner Name </td>
                                                            <td>{{$datalist->owners_name}}</td>

                                                        </tr>
                                                        <tr>
                                                            <th> Phone Number  </th>
                                                            <th> {{$datalist->phone_number}}  </th>


                                                        </tr>
                                                        <tr>
                                                            <td> ALternative Number  </td>
                                                            <td> {{$datalist->emergency_number}}   </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Street Address</td>
                                                            <td>{{$datalist->street_address}} </td>
                                                        </tr>

                                                        <tr>
                                                            <th> City  </th>
                                                            <th> {{$datalist->city}}   </th>
                                                        </tr>

                                                        <tr>
                                                            <td>State</td>
                                                            <td>{{$datalist->state}} </td>
                                                        </tr>
                                                        </tr>

                                                        <tr>
                                                            <th> Postal Code   </th>
                                                            <th> {{$datalist->postal_code}}   </th>
                                                        </tr>

                                                        <tr>
                                                            <th> Appointment Date   </th>
                                                            <th> {{$datalist->appointment_date}}   </th>
                                                        </tr>
                                                        <tr>
                                                            <td>Appointment Time</td>
                                                            <td>{{$datalist->appointment_time}} </td>
                                                        </tr>

                                                        <tr>
                                                            <th> Closer Name   </th>
                                                            <th> {{$datalist->Colsers_name}}   </th>
                                                        </tr>
                                                        <tr>
                                                            <td>Comments</td>
                                                            <td>{{$datalist->comments}} </td>
                                                        </tr>
                                                       <tr>
                                                           <th>Quality tem Status  </th>
                                                           <th>
                                                               @if($datalist->Qt_status === 'Pass')
                                                                   <span class="label label-success" style="color: #fff;font-size: 10px"> Pass</span>
                                                               @elseif($datalist->Qt_status === 'Hold')
                                                                   <span class="label label-info" style="background-color: red; color: #fff;font-size: 10px">Hold</span>
                                                               @elseif($datalist->Qt_status === 'Cancel')
                                                                   <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Cancel </span>
                                                               @endif

                                                           </th>
                                                       </tr>
                                                        <tr>
                                                            <td>Quality Team comment</td>
                                                            <td>
                                                                <span class="label label-success" style="color: #fff;font-size: 10px">   {{$datalist->Qt_comments}}</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th> Recording Link  </th>
                                                            <th><span class="label label-success" style="color: #fff;font-size: 10px"> {{$datalist->recodinglink}}</span></th>
                                                        </tr>
                                                        <tr>
                                                            <td>Manager Status </td>
                                                            <td>
                                                                @if($datalist->Qm_status === 'Pass')
                                                                    <span class="label label-success" style="color: #fff;font-size: 10px"> Pass</span>
                                                                @elseif($datalist->Qm_status === 'Hold')
                                                                    <span class="label label-info" style="background-color: red; color: #fff;font-size: 10px">Hold</span>
                                                                @elseif($datalist->Qm_status === 'Cancel')
                                                                    <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Cancel </span>
                                                                @endif
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        @endif

                                        @if(\Illuminate\Support\Facades\Auth::user()->users_type==="Qt" || \Illuminate\Support\Facades\Auth::user()->users_type==="HR")
                                            <div class="panel panel-default">
                                                <div class="panel-heading" id="" style="background-color:#537171; color: white;">Change Status
                                                </div>
                                                <div class="panel-body">
                                                    <div class="col-md-12">
                                                        <form method="POST" action="{{ URL::to('/portal/lead/change') }}">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="lead_trackid" id="house_item_track_id" value="{{ $datalist->lead_trackid }}"/>
                                                            <div class="col-sm-12">
                                                                <div class="form-group col-sm-6 col-sm-offset-1 ">
                                                                    <label for="users_name">Select Status :<span class="mark">*</span> </label>
                                                                    <select class="form-control" id="users_gender" name="Qt_status">
                                                                        <option value="NULL">Select Status------</option>
                                                                        <option value="Pass" id="pass" >Pass</option>
                                                                        <option value="Cancel" id="show_pay">Cancel</option>
                                                                        <option value="Hold" id="show_pay1">Hold</option>
                                                                    </select>
                                                                    <small class="text-danger"></small>
                                                                </div>

                                                            </div>
                                                            <div id="pay_block">
                                                                <div class="col-sm-12 col-sm-offset-1 ">
                                                                <div class="form-group col-sm-3 ">
                                                                    <label for="users_email">Start Time :<span class="mark">*</span></label>
                                                                    <input class="form-control" type="text" id="firstime"  name="appointment_time"/>
                                                                    <small class="text-danger"></small>
                                                                </div>
                                                                    <div class="form-group col-sm-3 ">
                                                                        <label for="users_email">End Time :<span class="mark">*</span></label>
                                                                        <input type="text" class="form-control " id="secondtime" value="" name="appointment_time1">
                                                                        <small class="text-danger"></small>
                                                                    </div>
                                                                    <div class="col-sm-3" style="margin-top: 30px;">
                                                                        <label class="radio-inline">
                                                                            <input type="radio" name="optradio" id="twohours" value="2">2 hours
                                                                        </label>
                                                                        <label class="radio-inline">
                                                                            <input type="radio" name="optradio" id="threehours" value="3">3 hours
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <script>
                                                                    $('#firstime').datetimepicker({
                                                                        format: 'HH:mm a'

                                                                    });
                                                                </script>

                                                                <div class="col-sm-12">
                                                                <div class="form-group col-sm-6 col-sm-offset-1 ">
                                                                    <label for="users_birthdate">Appointment Date :<span class="mark">*</span> </label>
                                                                    <div class="input-group ">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                        <input type="text" class="form-control pull-right" id="datepicker" name="appointment_date" required>

                                                                        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                                                                        <link rel="stylesheet" href="/resources/demos/style.css">
                                                                        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                                                                        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

                                                                        <script>

                                                                            $( function() {
                                                                                $( "#datepicker" ).datepicker();
                                                                            } );

                                                                        </script>

                                                                    </div>
                                                                    <small class="text-danger">{{ $errors->first('users_birthdate') }}</small>
                                                                </div>
                                                                    <div class="col-sm-6"></div>
                                                                    </div>

                                                                <div class="col-sm-12 col-sm-offset-1">
                                                                    <div class="form-group col-sm-6 ">
                                                                        <label for="users_email">Comment :<span class="mark">*</span></label>
                                                                        <textarea class="form-control" name="Qt_comments" rows="5"  cols="75" placeholder="comments here--------"></textarea>
                                                                        <small class="text-danger"></small>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-3 col-sm-offset-4" style="margin-top: 20px;" >
                                                                    <button  id="btn" type="submit" return="false" name="btnSaveClass" class="btn  btn-sm btn-block" style="background-color:#537171;color: white;height: 35px;"><i class="fa fa-edit"></i> Update Status </button>
                                                                </div>
                                                            </div>
                                                        </form>

                                                            <div id="pay_block1">
                                                                <form method="POST" action="{{ URL::to('/portal/lead/change') }}">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="lead_trackid" id="house_item_track_id" value="{{ $datalist->lead_trackid }}"/>
                                                                <input type="hidden" name="Qt_status" value="Pass">
                                                                <div class="col-sm-3 col-sm-offset-3" style="margin-top: 20px;" >
                                                                    <button  id="btn" type="submit" return="false" name="btnSaveClass" class="btn  btn-sm btn-block" style="background-color:#537171;color: white;height: 35px;"><i class="fa fa-edit"></i> Update Status </button>
                                                                </div>
                                                                </form>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                                            <script>
                                                $("#pay_block").hide();
                                                $("#pay_block1").hide();
                                                $("select").change(function(){
                                                    if($("#show_pay").is(":selected")){
                                                        $("#pay_block1").slideUp("slow");
                                                        $("#pay_block").slideDown("slow");
                                                    }
                                                    else if($("#show_pay1").is(":selected")){
                                                        $("#pay_block1").slideUp("slow");
                                                        $("#pay_block").slideDown("slow");
                                                    }
                                                    else if($("#pass").is(":selected")){
                                                        $("#pay_block1").slideDown("slow");
                                                        $("#pay_block").slideUp("slow");
                                                    }
                                                    else { $("#pay_block").slideUp("slow"); }
                                                });

                                            </script>


                                            <script>
                                                $(document).ready(function(){
                                                    $("#twohours").click(function(){

                                                        var  ftime=$("#firstime").val();

                                                        var hour=(parseInt(ftime.split(":")[0])+2)%12;
                                                        var min=ftime.split(":")[1].split(" ")[0];
                                                        am=ftime.split(":")[1].split(" ")[1];

                                                        if((parseInt(ftime.split(":")[0])+2)/12>=1)
                                                        {  console.log("hi");
                                                            if(am=="am")
                                                                am="pm";
                                                            else
                                                                am="am";
                                                        }

                                                        $("#secondtime").val(hour+":"+min+" "+am);
                                                    });

                                                    $("#threehours").click(function(){

                                                        var  ftime=$("#firstime").val();

                                                        var hour=(parseInt(ftime.split(":")[0])+3)%12;
                                                        var min=ftime.split(":")[1].split(" ")[0];
                                                        am=ftime.split(":")[1].split(" ")[1];

                                                        if((parseInt(ftime.split(":")[0])+2)/12>=1)
                                                        {  console.log("hi");
                                                            if(am=="am")
                                                                am="pm";
                                                            else
                                                                am="am";
                                                        }

                                                        $("#secondtime").val(hour+":"+min+" "+am);
                                                    });

                                                });


                                            </script>

                                        @endif

    @if(\Illuminate\Support\Facades\Auth::user()->users_type==="Super Admin")
    {{--<div class="panel panel-default">--}}
        {{--<div class="panel-heading" id="" style="background-color:#537171; color: white;"> {{$datalist->Qt_name}}  Change Status  </div>--}}
        {{--<div class="panel-body">--}}
            {{--<div class="col-md-12">--}}
                {{--<table class="table table-responsive table-striped table-bordered">--}}
                   {{--<tr>--}}
                       {{--<th>Status Change</th>--}}
                       {{--<th> <span class="label label-default"> {{$datalist->Qt_status}} </span></th>--}}
                   {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Comments </td>--}}
                        {{--<td>{{$datalist->Qt_comments}}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<th>Recording Link</th>--}}
                        {{--<th>{{$datalist->recodinglink}}</th>--}}
                    {{--</tr>--}}
                {{--</table>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="panel panel-default">--}}
        {{--<div class="panel-heading" id="" style="background-color:#537171; color: white;">Quality Manager lead Confirmation </div>--}}
        {{--<div class="panel-body">--}}
            {{--<div class="col-md-12">--}}
                {{--<table class="table table-responsive table-striped table-bordered">--}}
                    {{--<thead style="background-color: #B5BBC8">--}}
                    {{--<tr>--}}
                        {{--<th>Status Change</th>--}}
                        {{--<th> <span class="label label-default"> {{$datalist->Qm_status}} </span></th>--}}
                    {{--</tr>--}}

                    {{--</tbody>--}}
                {{--</table>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    @elseif(\Illuminate\Support\Facades\Auth::user()->users_type==="Center")
        <div class="panel panel-default">
            <div class="panel-heading" id="" style="background-color:#537171; color: white;">Agent Lead Information</div>
            <div class="panel-body">
                <div class="col-md-12">
                    <table class="table table-responsive table-striped table-bordered">
                        <tr>
                            <th> Time stamp  </th>
                            <th> {{$datalist->Timestamp}} </th>
                        </tr>
                        <tr>
                            <td>CenterName</td>
                            <td>
                                <?php
                                $userdata=\App\User::where('users_track_id',$datalist->center_name)->first();
                                $centartaken=\App\RevenueCalculated::where('center_name',$datalist->center_name)->first();
                                ?>
                                {{$userdata->users_name}}
                            </td>

                        </tr>
                        <tr>
                            <th> Agent Name  </th>
                            <th> {{$datalist->agent_name}}</th>
                        </tr>
                        <tr>
                            <td>Owner Name </td>
                            <td>{{$datalist->owners_name}}</td>
                        </tr>
                        <tr>
                            <th> Phone Number  </th>
                            <th> {{$datalist->phone_number}}  </th>
                        </tr>
                        <tr>
                            <td> Emergency Number  </td>
                            <td> {{$datalist->emergency_number}}   </td>
                        </tr>
                        <tr>
                            <td>Street Address</td>
                            <td>{{$datalist->street_address}} </td>
                        </tr>   <tr>
                            <th> City  </th>
                            <th> {{$datalist->city}}   </th>
                        </tr>
                        <tr>
                            <td>State</td>
                            <td>{{$datalist->state}} </td>
                        </tr>

                        </tr>   <tr>
                            <th> Postal Code   </th>
                            <th> {{$datalist->postal_code}}   </th>
                        </tr>

                        <tr>
                            <th> Appointment Date   </th>
                            <th> {{$datalist->appointment_date}}   </th>
                        </tr>
                        <tr>
                            <td>Appointment Time</td>
                            <td>{{$datalist->appointment_time}} </td>
                        </tr>
                        <tr>
                            <th> Closer Name   </th>
                            <th> {{$datalist->Colsers_name}}   </th>
                        </tr>
                        <tr>
                            <td>Comments</td>
                            <td>{{$datalist->comments}} </td>
                        </tr>
                        <tr>
                            <td ><span class="label label-default" style="color: #fff;font-size: 10px">Commision</span> </td>
                            <td><span class="label label-success" style="color: #fff;font-size: 10px">
                                    @if(!empty($centartaken))
                                        {{$centartaken->center_taken}}
                                    @endif
                                </span>  </td>
                        </tr>
                        <tr>
                            <td ><span class="label label-default" style="color: #fff;font-size: 10px">Paid Status</span> </td>
                            <td>
                                <span class="label label-success" style="color: #fff;font-size: 10px"> {{$datalist->Qm_paid_status}}</span>
                            </td>
                        </tr>
                        <tr>
                            <td ><span class="label label-default" style="color: #fff;font-size: 10px">Lead Status</span> </td>
                            <td>
                                <span class="label label-success" style="color: #fff;font-size: 10px"> {{$datalist->Qm_status}}</span>
                            </td>
                        </tr>


                    </table>

                </div>
            </div>
        </div>
    @endif
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>

@stop