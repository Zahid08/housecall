<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@extends('layout.backend.master')
@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"> Detail of All Leads <span>
                                </span></div>
                            @if (session('success'))
                                <div class="alert alert-success"  id="success">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>{{ session('success') }}</strong>
                                </div>
                            @endif
                            <div class="panel-body">
                                <div class="panel panel-primary">
                                    <div class="panel-body">
                                        <div class="col-md-4">
                                            <h5> Lead Status - <span class="label label-default"> Active  </span></h5>
                                        </div>
                                        <div class="col-md-4">
                                            <h5>Lead Created  - <span class="label label-default"> {{ $datalist->created_at->format('d-m-Y') }}</span></h5>
                                        </div>
                                        <div class="col-md-4">

                                            {{--<a href="javascript:void(0);" data-toggle="modal" data-target="#inActive{{ $datalist->	lead_trackid }}"  style="float: right;margin-top:5px;color: black;font-size: 16px;">--}}
                                               {{--<span class="label label-default"> Update Lead Information</span>--}}
                                            {{--</a>--}}
                                            <span id="ulead">
                                            <a onclick="edit()" data-toggle="modal"  style="float: right;margin-top:5px;color: black;font-size: 16px;cursor: pointer">
                                               <span class="label label-default"> Update Lead Information</span>
                                            </a>
                                            </span>
                                        </div>

                                        <script type="text/javascript">
                                            function edit() {
                                                alert("asdsadsad")
                                                document.getElementById("ulead").innerHTML="<a style='float: right;margin-top:5px;color: black;font-size: 16px;cursor: pointer' onclick='save()' ><span class='label label-defaultlabel label-success'>Save Lead Information</span> </a>";
                                                phone=document.getElementById("phone").value;
                                                ownername=document.getElementById("ownername").value;
                                                alternativeno=document.getElementById("alternative").value;
                                                streetaddress=document.getElementById("streeta").value;
                                                city=document.getElementById("city").value;
                                                state=document.getElementById("state").value;
                                                postalcode=document.getElementById("postalcode").value;
                                                closername=document.getElementById("closername").value;
                                                comment=document.getElementById("comment").value;

                                                document.getElementById("pn").innerHTML="<input type='text' class='form-control' id='uphone' style='border: 1px solid green;' value='"+phone+"'>";
                                                document.getElementById("own").innerHTML="<input type='text' class='form-control' id='uownername' style='border: 1px solid green;'  value='"+ownername+"'>";
                                                document.getElementById("aln").innerHTML="<input type='text' class='form-control' id='ualternative' style='border: 1px solid green;'  value='"+alternativeno+"'>";
                                                document.getElementById("sa").innerHTML="<input type='text' class='form-control' id='ustreeta' style='border: 1px solid green;' value='"+streetaddress+"'>";
                                                document.getElementById("c").innerHTML="<input type='text' class='form-control' id='city' style='border: 1px solid green;' value='"+city+"'>";
                                                document.getElementById("s").innerHTML="<input type='text' class='form-control' id='ustate' style='border: 1px solid green;' value='"+state+"'>";
                                                document.getElementById("pc").innerHTML="<input type='text' class='form-control' id='upostalcode'  style='border: 1px solid green;' value='"+postalcode+"'>";
                                                document.getElementById("cn").innerHTML="<input type='text' class='form-control' id='uclosername' style='border: 1px solid green;'  value='"+closername+"'>";
                                                document.getElementById("cmnt").innerHTML="<input type='text' class='form-control' id='ucomment' style='border: 1px solid green;'  value='"+comment+"'>";
                                            }
                                            function save() {
                                                lead_trackid=document.getElementById("lead_trackid").value;
                                                phone=document.getElementById("uphone").value;
                                                ownername=document.getElementById("uownername").value;
                                                alternativeno=document.getElementById("ualternative").value;
                                                streetaddress=document.getElementById("ustreeta").value;
                                                city=document.getElementById("city").value;
                                                state=document.getElementById("ustate").value;
                                                postalcode=document.getElementById("upostalcode").value;
                                                closername=document.getElementById("uclosername").value;
                                                comment=document.getElementById("ucomment").value;

                                                $.ajaxSetup({
                                                    headers: {
                                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                    }
                                                });

                                                $.ajax({
                                                    type: 'POST',
                                                    url: '/portal/leadformation/details/lead/updateajax',
                                                    dataType: 'json',
                                                    data:{
                                                        lead_trackid:lead_trackid,
                                                        phone:phone,
                                                        ownername:ownername,
                                                        alternativeno:alternativeno,
                                                        streetaddress:streetaddress,
                                                        city:city,
                                                        state:state,
                                                        postalcode:postalcode,
                                                        closername:closername,
                                                        comment:comment
                                                    },
                                                    success: function(){
                                                        document.getElementById("pn").innerHTML=phone;
                                                        document.getElementById("own").innerHTML=ownername;
                                                        document.getElementById("aln").innerHTML=alternativeno;
                                                        document.getElementById("sa").innerHTML=streetaddress;
                                                        document.getElementById("c").innerHTML=city;
                                                        document.getElementById("s").innerHTML=state;
                                                        document.getElementById("pc").innerHTML=postalcode;
                                                        document.getElementById("cn").innerHTML=closername;
                                                        document.getElementById("cmnt").innerHTML=comment;

                                                    }
                                                });

                                            }
                                        </script>


                                        <div class="clearfix"></div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" id="" style="background-color:#537171; color: white;">Agent Lead Information</div>
                                                <div class="panel-body">
                                                    <div class="col-md-12">
                                                        <table class="table table-responsive table-striped table-bordered">
                                                            <input type="hidden" id="lead_trackid" value="{{$datalist->lead_trackid}}" >
                                                            <tr>
                                                            <tr>
                                                                <th> Qc <span class="label label-success" style="color: #fff;font-size: 10px;margin-left: 100px;">Qualit Team</span>  </th>
                                                                <th><span class="label label-success" style="color: #fff;font-size: 10px"> {{$datalist->Qt_name}}</span></th>
                                                            </tr>
                                                            <tr>
                                                                <th>Qc Status  </th>
                                                                <th>
                                                                    @if($datalist->Qt_status === 'Pass')
                                                                        <span class="label label-success" style="color: #fff;font-size: 10px"> Pass</span>
                                                                    @elseif($datalist->Qt_status === 'Hold')
                                                                        <span class="label label-info" style="background-color: red; color: #fff;font-size: 10px">Hold</span>
                                                                    @elseif($datalist->Qt_status === 'Cancel')
                                                                        <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Cancel </span>
                                                                    @endif

                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th style="width: 500px;"> Time stamp  </th>
                                                                <th> {{$datalist->Timestamp}} </th>
                                                            </tr>


                                                                <td>CenterName</td>
                                                                <td>
                                                                    <?php
                                                                    $centername=$datalist->center_name;
                                                                    $userdata=\App\User::where('users_track_id',$datalist->center_name)->first();
                                                                    $centartaken=\App\RevenueCalculated::where('center_name',$datalist->center_name)->first();
                                                                    ?>
                                                                    <span class="label label-default" style="color: #fff;font-size: 10px"> {{$userdata->users_name}}
                                                              </span>
                                                                </td>

                                                            </tr>


                                                            <tr>
                                                                <th> Agent Name  </th>
                                                                <th> {{$datalist->agent_name}}</th>
                                                            </tr>

                                                            <tr>
                                                                <th> Appointment Date   </th>
                                                                <th id="ad" > {{$datalist->appointment_date}} </th>
                                                                <input type="hidden" id="apointment" value="{{$datalist->appointment_date}}">

                                                            </tr>
                                                            <tr>
                                                                <td>Appointment Time</td>
                                                                <th id="ad" > {{$datalist->appointment_time}} </th>
                                                                <input type="hidden" id="apointment" value="{{$datalist->appointment_time}}">

                                                            </tr>

                                                            <tr>
                                                                <td>Owner Name </td>
                                                                <th id="own" > {{$datalist->owners_name}}
                                                                    <input type="hidden" id="ownername" value="{{$datalist->owners_name}}">
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th> Phone Number  </th>
                                                                <th id="pn" > {{$datalist->phone_number}} </th>
                                                                <input type="hidden" id="phone" value="{{$datalist->phone_number}}">
                                                            </tr>
                                                            <tr>
                                                                <td> ALternative Number  </td>
                                                                <th id="aln" > {{$datalist->emergency_number}} </th>
                                                                <input type="hidden" id="alternative" value="{{$datalist->emergency_number}}">

                                                            </tr>
                                                            <tr>
                                                                <td>Street Address</td>
                                                                <th id="sa" > {{$datalist->street_address}} </th>
                                                                <input type="hidden" id="streeta" value="{{$datalist->street_address}}">

                                                            </tr>


                                                            <tr>
                                                                <th> City  </th>
                                                                <th id="c" > {{$datalist->city}} </th>
                                                                <input type="hidden" id="city" value="{{$datalist->city}}">
                                                            </tr>

                                                            <tr>
                                                                <td>State</td>
                                                                <th id="s" > {{$datalist->state}} </th>
                                                                <input type="hidden" id="state" value="{{$datalist->state}}">
                                                            </tr>

                                                            <tr>
                                                                <th> Postal Code   </th>
                                                                <th id="pc" > {{$datalist->postal_code}} </th>
                                                                <input type="hidden" id="postalcode" value="{{$datalist->postal_code}}">
                                                            </tr>

                                                            <tr>
                                                                <td>Comments</td>
                                                                <th id="cmnt" > {{$datalist->comments}} </th>
                                                                <input type="hidden" id="comment" value="{{$datalist->comments}}">

                                                            </tr>

                                                            <tr>
                                                                <th> Closer Name   </th>
                                                                <th id="cn" > {{$datalist->Colsers_name}} </th>
                                                                <input type="hidden" id="closername" value="{{$datalist->Colsers_name}}">


                                                                </th>
                                                            </tr>

                                                            </tr>

                                                             <table class="table table-responsive table-striped table-bordered">
                                                                 <tr>
                                                                     <td style="width: 500px;">Call Date  <span class="label label-success" style="color: #fff;font-size: 10px;margin-left: 100px;">Qualit Manager</span> </td>
                                                                     <td>
                                                                          {{$datalist->call_date}}
                                                                     </td>
                                                                 </tr>
                                                                 <tr>
                                                                     <th>Technicians Name  </th>
                                                                     <th><span class="label label-info" style="color: #fff;font-size: 10px"> {{$datalist->tec_name}}</span></th>
                                                                 </tr>
                                                                 <tr>
                                                                     <th> Appointment Date   </th>
                                                                     <th> {{$datalist->appointment_date}}   </th>
                                                                 </tr>
                                                                 <td>CenterName</td>
                                                                 <td>
                                                                     <?php
                                                                     $centername=$datalist->center_name;
                                                                     $userdata=\App\User::where('users_track_id',$datalist->center_name)->first();
                                                                     $centartaken=\App\RevenueCalculated::where('center_name',$datalist->center_name)->first();
                                                                     ?>
                                                                     <span class="label label-default" style="color: #fff;font-size: 10px"> {{$userdata->users_name}}
                                                            </span>
                                                                 </td>
                                                                 <tr>
                                                                     <th> Confirmed By   </th>
                                                                     <th> {{$datalist->appointment_date}}   </th>
                                                                 </tr>

                                                                 <tr>
                                                                     <th> Booking Date  </th>
                                                                     <th> {{$datalist->appointment_date}}   </th>
                                                                 </tr>

                                                                 <tr>
                                                                     <th> Agent Name  </th>
                                                                     <th> {{$datalist->agent_name}}</th>
                                                                 </tr>
                                                                 <tr>
                                                                     <td>Owner Name </td>
                                                                     <td>{{$datalist->owners_name}}</td>

                                                                 </tr>
                                                                 <tr>
                                                                     <th> Phone Number  </th>
                                                                     <th> {{$datalist->phone_number}}  </th>
                                                                 </tr>

                                                                 <tr>
                                                                     <th> City  </th>
                                                                     <th> {{$datalist->city}}   </th>
                                                                 </tr>

                                                                 <tr>
                                                                     <td>State</td>
                                                                     <td>{{$datalist->state}} </td>
                                                                 </tr>

                                                                 <tr>
                                                                     <th> Postal Code   </th>
                                                                     <th> {{$datalist->postal_code}}   </th>
                                                                 </tr>

                                                                 <tr>
                                                                     <td>Job Status </td>
                                                                     <td>
                                                                         @if($datalist->Qm_status === 'CB')
                                                                             <span class="label label-info" style="color: #fff;font-size: 10px"> CALL BACK</span>
                                                                         @elseif($datalist->Qm_status === 'Hold')
                                                                             <span class="label label-info" style="background-color: red; color: #fff;font-size: 10px">Hold</span>
                                                                         @elseif($datalist->Qm_status === 'Cancel')
                                                                             <span class="label label-info" style="background-color: red; color: #fff;font-size: 10px">Cancel </span>
                                                                         @endif
                                                                     </td>
                                                                 </tr>

                                                                 <tr>
                                                                     <th> Amount   </th>
                                                                     <th> {{$datalist->	lead_ammount}}   </th>
                                                                 </tr>


                                                                 <tr>
                                                                     <th> Recording Link  </th>
                                                                     <th><span class="label label-success" style="color: #fff;font-size: 10px"> {{$datalist->recodinglink}}</span></th>
                                                                 </tr>


                                                            <tr>
                                                                <th> Assign Status  </th>
                                                                <td>
                                                                    @if($datalist->tec_trackid !=NULL)
                                                                        <span class="label label-success" style="color: #fff;font-size: 10px"> Assign</span>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                                 <tr>
                                                                     <th>
                                                                        Action
                                                                     </th>
                                                                     <td>
                                                                         <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;"  href="{{url('/portal/qmDashboard/assigntechnicianlist/'. $datalist->lead_trackid)}}"  style="color: red">
                                                                         <span style="color: #C53431"><i class="fa fa-save"></i> </span>Assign
                                                                         </a>&nbsp;
                                                                         <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;"   style="color: red" onclick="jobstus()" id="jobstatus">
                                                                             <span style="color: #C53431"><i class="fa fa-save"></i> </span>JobStatus
                                                                         </a>&nbsp;
                                                                         <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;"   style="color: red" onclick="Qmstatus()" id="qmstatus">
                                                                             <span style="color: #C53431"><i class="fa fa-save"></i> </span>QmStatus
                                                                         </a>&nbsp;

                                                                     </td>
                                                                 </tr>
                                                             </table>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        <script>
                                            function jobstus() {
                                                $('#displayInfo').show('slow');
                                                $('#displayInfoqm').hide('slow');
                                            }
                                            function Qmstatus() {
                                                $('#displayInfoqm').show('slow');
                                                $('#displayInfo').hide('slow');
                                            }
                                            function assigntec() {

                                            }
                                        </script>

                                            <div class="panel panel-default" style="display:none;" id="displayInfo" >
                                                <div class="panel-heading" id="" style="background-color:#537171; color: white;">Job Done Status Change
                                                </div>
                                                <div class="panel-body">
                                                    <div class="col-md-12">
                                                        <form method="POST" action="{{ URL::to('/portal/lead/changeQm') }}">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="lead_trackid" id="house_item_track_id" value="{{ $datalist->lead_trackid }}"/>
                                                            <div class="col-sm-12">
                                                                <div class="form-group col-sm-6 col-sm-offset-1 ">
                                                                    <label for="users_name">Select Status :<span class="mark">*</span> </label>
                                                                    <select class="form-control" id="users_gender" name="Qm_status">
                                                                        <option value="NULL">Select Status------</option>
                                                                        <option value="JD" id="pass" >Job Done</option>
                                                                        <option value="CAN" id="cancel">Cancel</option>
                                                                        <option value="RE" id="reshediule">Reshedule</option>
                                                                        <option value="Hold" id="callback">Call Back</option>
                                                                    </select>
                                                                    <small class="text-danger"></small>
                                                                </div>

                                                            </div>
                                                            <div id="pay_block">
                                                                <div class="col-sm-12 col-sm-offset-1 ">
                                                                    <div class="form-group col-sm-3 ">
                                                                        <label for="users_email">Start Time :<span class="mark">*</span></label>
                                                                        <input class="form-control" type="text" id="firstime"  name="appointment_time"/>
                                                                        <small class="text-danger"></small>
                                                                    </div>
                                                                    <div class="form-group col-sm-3 ">
                                                                        <label for="users_email">End Time :<span class="mark">*</span></label>
                                                                        <input type="text" class="form-control " id="secondtime" value="" name="appointment_time1">
                                                                        <small class="text-danger"></small>
                                                                    </div>
                                                                    <div class="col-sm-3" style="margin-top: 30px;">
                                                                        <label class="radio-inline">
                                                                            <input type="radio" name="optradio" id="twohours" value="2">2 hours
                                                                        </label>
                                                                        <label class="radio-inline">
                                                                            <input type="radio" name="optradio" id="threehours" value="3">3 hours
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <script>
                                                                    $('#firstime').datetimepicker({
                                                                        format: 'HH:mm a'

                                                                    });
                                                                </script>

                                                                <div class="col-sm-12">
                                                                    <div class="form-group col-sm-6 col-sm-offset-1 ">
                                                                        <label for="users_birthdate">Appointment Date :<span class="mark">*</span> </label>
                                                                        <div class="input-group ">
                                                                            <div class="input-group-addon">
                                                                                <i class="fa fa-calendar"></i>
                                                                            </div>
                                                                            <input type="text" class="form-control pull-right" id="datepicker" name="appointment_date" required>

                                                                            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                                                                            <link rel="stylesheet" href="/resources/demos/style.css">
                                                                            <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                                                                            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

                                                                            <script>

                                                                                $( function() {
                                                                                    $( "#datepicker" ).datepicker();
                                                                                } );

                                                                            </script>

                                                                        </div>
                                                                        <small class="text-danger">{{ $errors->first('users_birthdate') }}</small>
                                                                    </div>
                                                                    <div class="col-sm-6"></div>
                                                                </div>

                                                                <div class="col-sm-3 col-sm-offset-4" style="margin-top: 20px;" >
                                                                    <button  id="btn" type="submit" return="false" name="btnSaveClass" class="btn  btn-sm btn-block" style="background-color:#537171;color: white;height: 35px;"><i class="fa fa-edit"></i> Update Status </button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        <div id="pay_block1">
                                                            <form method="POST" action="{{ URL::to('/portal/lead/changeQm') }}">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="lead_trackid" id="house_item_track_id" value="{{ $datalist->lead_trackid }}"/>
                                                                <input type="hidden" name="Qm_status" value="JD">

                                                                <div class="form-group col-sm-3 col-sm-offset-1" >
                                                                    <label for="users_email">Amount :<span class="mark">*</span></label>
                                                                    <input type="text" class="form-control " id="secondtime" value="" name="amount">
                                                                    <small class="text-danger"></small>
                                                                </div>

                                                                <div class="col-sm-3" style="margin-top: 20px;" >
                                                                    <button  id="btn" type="submit" return="false" name="btnSaveClass" class="btn  btn-sm btn-block" style="background-color:#537171;color: white;height: 35px;"><i class="fa fa-edit"></i> Update Status </button>
                                                                </div>
                                                            </form>
                                                        </div>

                                                        <div id="cancel_payblock">
                                                            <form method="POST" action="{{ URL::to('/portal/lead/changeQm') }}">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="lead_trackid" id="house_item_track_id" value="{{ $datalist->lead_trackid }}"/>
                                                                <input type="hidden" name="Qm_status" value="CAN">
                                                                <div class="col-sm-3 col-sm-offset-3" style="margin-top: 20px;" >
                                                                    <button  id="btn" type="submit" return="false" name="btnSaveClass" class="btn  btn-sm btn-block" style="background-color:#537171;color: white;height: 35px;"><i class="fa fa-edit"></i> Update Status </button>
                                                                </div>
                                                            </form>
                                                        </div>

                                                        <div id="callback_payroll">
                                                            <form method="POST" action="{{ URL::to('/portal/lead/changeQm') }}">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="lead_trackid" id="house_item_track_id" value="{{ $datalist->lead_trackid }}"/>
                                                                <input type="hidden" name="Qm_status" value="CB">
                                                                <div class="col-sm-3 col-sm-offset-3" style="margin-top: 20px;" >
                                                                    <button  id="btn" type="submit" return="false" name="btnSaveClass" class="btn  btn-sm btn-block" style="background-color:#537171;color: white;height: 35px;"><i class="fa fa-edit"></i> Update Status </button>
                                                                </div>
                                                            </form>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>

                                            <script>
                                                $("#pay_block").hide();
                                                $("#pay_block1").hide();
                                                $("#cancel_payblock").hide();
                                                $("#callback_payroll").hide();

                                                $("select").change(function(){
                                                    if($("#cancel").is(":selected")){
                                                        $("#pay_block1").slideUp("slow");
                                                        $("#pay_block").slideUp("slow");
                                                        $("#cancel_payblock").slideDown("slow");
                                                    }
                                                    else if($("#reshediule").is(":selected")){
                                                        $("#pay_block1").slideUp("slow");
                                                        $("#cancel_payblock").slideUp("slow");
                                                        $("#pay_block").slideDown("slow");
                                                    }
                                                    else if($("#callback").is(":selected")){
                                                        $("#pay_block1").slideUp("slow");
                                                        $("#cancel_payblock").slideUp("slow");
                                                        $("#pay_block").slideUp("slow");
                                                        $("#callback_payroll").slideDown("slow");
                                                    }

                                                    else if($("#pass").is(":selected")){
                                                        $("#pay_block1").slideDown("slow");
                                                        $("#cancel_payblock").slideUp("slow");
                                                        $("#pay_block").slideUp("slow");
                                                        $("#callback_payroll").slideUp("slow");
                                                    }
                                                    else { $("#pay_block").slideUp("slow"); }
                                                });

                                            </script>


                                            <script>
                                                $(document).ready(function(){
                                                    $("#twohours").click(function(){

                                                        var  ftime=$("#firstime").val();

                                                        var hour=(parseInt(ftime.split(":")[0])+2)%12;
                                                        var min=ftime.split(":")[1].split(" ")[0];
                                                        am=ftime.split(":")[1].split(" ")[1];

                                                        if((parseInt(ftime.split(":")[0])+2)/12>=1)
                                                        {  console.log("hi");
                                                            if(am=="am")
                                                                am="pm";
                                                            else
                                                                am="am";
                                                        }

                                                        $("#secondtime").val(hour+":"+min+" "+am);
                                                    });

                                                    $("#threehours").click(function(){

                                                        var  ftime=$("#firstime").val();

                                                        var hour=(parseInt(ftime.split(":")[0])+3)%12;
                                                        var min=ftime.split(":")[1].split(" ")[0];
                                                        am=ftime.split(":")[1].split(" ")[1];

                                                        if((parseInt(ftime.split(":")[0])+2)/12>=1)
                                                        {  console.log("hi");
                                                            if(am=="am")
                                                                am="pm";
                                                            else
                                                                am="am";
                                                        }

                                                        $("#secondtime").val(hour+":"+min+" "+am);
                                                    });

                                                });

                                            </script>

                                        <div class="panel panel-default" style="display:none;" id="displayInfoqm">
                                            <div class="panel-heading" id="" style="background-color:#537171; color: white;">Quality team  Status
                                             </div>
                                            <form method="POST" action="{{ URL::to('/portal/lead/changestatusqm') }}">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="lead_trackid" id="house_item_track_id" value="{{ $datalist->lead_trackid }}"/>
                                                <div class="col-sm-12" style="margin-top: 20px;">
                                                    <div class="form-group col-sm-4  ">
                                                        <label for="users_name">Select Status :<span class="mark">*</span> </label>
                                                        <select class="form-control" id="users_gender" name="Qt_status">
                                                            <option value="NULL">Select Status------</option>
                                                            <option value="Pass" id="pass" >Pass</option>
                                                            <option value="Cancel" i>Cancel</option>
                                                            <option value="Hold">Hold</option>
                                                        </select>
                                                        <small class="text-danger"></small>
                                                    </div>
                                                    <div class="form-group col-sm-4 " style="margin-top: 20px;">
                                                         <input type="text" name="recordlink" class="form-control" placeholder="record link add.................">
                                                    </div>
                                                    <div class="col-sm-3 "  style="margin-top: 20px;">
                                                        <button  id="btn" type="submit" return="false" name="btnSaveClass" class="btn  btn-sm btn-block" style="background-color:#537171;color: white;height: 35px;"><i class="fa fa-edit"></i> Update Status </button>
                                                    </div>

                                                </div>
                                            </form>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <script>
        $("#success-alert").fadeTo(2000, 500).slideUp(900, function(){
            $("#success-alert").slideUp(500);
        });
    </script>

@stop