<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>
@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"><i class="fa fa-list"></i> <SPAN>Lead Information</SPAN></div>
                            <fieldset style="border: 1px solid #435452 !important;border-radius: 0px;">
                                <div class="panel-body" style="padding: 0px;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- Alert Section-->
                                            @if (session('errorArray'))
                                                <div class="alert alert-danger">
                                                    @foreach($errors->all() AS $key => $value)
                                                        <strong><i class="fa fa-warning"></i> {{ $value }}</strong><br>
                                                    @endforeach
                                                </div>
                                            @endif
                                            @if (session('error'))
                                                <div class="alert alert-danger"  id="error">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('error') }}</strong>
                                                </div>
                                            @endif
                                            @if (session('success'))
                                                <div class="alert alert-success"  id="success">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('success') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="box-body table-responsive no-padding">
                                        <table id="houseList" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                            <thead style="background: #537171;color: white;">
                                            <tr>
                                                <th>SL</th>
                                                <th> QT Status
                                                    <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#filtering" style="color: green">
                                                        <span style="color: #C53431"><i class="fa fa-filter" title="Add recording link"></i></span>
                                                    </a>

                                                    <div id="filtering" class="modal fade" role="dialog">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <form method="POST" action="{{ URL::to('/portal/lead/addrecording') }}">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h5 class="modal-title" style="text-align: center;">Search Status</h5>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="col-sm-6">
                                                                            <input type="checkbox" name="Qt_status[]" value="1">   <span style="color: black;">Pass</span>
                                                                            <input type="checkbox" name="Qt_status[]" value="1">   <span style="color: black;">Cancel </span>
                                                                            <input type="checkbox" name="Qt_status[]" value="1">   <span style="color: black;">HOld</span>
                                                                             </div>
                                                                        <div class="col-sm-6">
                                                                            <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-success center-block"><i class="fa fa-plus"></i>&nbsp Search</button>

                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div> <!--- End Active Modal -->


                                                </th>
                                                <th> CenterName  </th>
                                                <th> Owner Name </th>
                                                <th> Phone No </th>
                                                <th> Appointment Time  </th>
                                                <th> Appointment Date  </th>
                                                <th class="no-sort"> Action  </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php
                                                $count = 1;
                                            @endphp

                                            @if(!empty($dataList))
                                                @foreach($dataList as $data)
                                                    <tr>
                                                        <td>{{$count}}</td>

                                                        <td>
                                                            @if($data->Qt_status === 'Pass')
                                                                <span class="label label-success" style="color: #fff;font-size: 10px"> Pass</span>
                                                            @elseif($data->Qt_status === 'Hold')
                                                                <span class="label label-info" style="background-color: red; color: #fff;font-size: 10px">Hold</span>
                                                            @elseif($data->Qt_status === 'Cancel')
                                                                <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Cancel </span>
                                                            @endif
                                                        </td>
                                                        <td>
<!--                                                            --><?php
//                                                            $technicianStatsu=\App\TechniciansModel::where('tec_trackid',$data->center_name)->first();
//                                                            ?>

                                                            <?php
                                                            $centername=$data->center_name;
                                                            $userdata=\App\User::where('users_track_id',$data->center_name)->first();
                                                            ?>

                                                            <span class="label label-default" style="color: #fff;font-size: 10px"> {{$userdata->users_name}}
                                                            </span>


                                                        </td>
                                                        <td>{{ $data->owners_name }}</td>
                                                        <td>{{ $data->phone_number }}</td>
                                                        <td>{{ $data->appointment_time }}</td>
                                                        <td>{{ $data->appointment_date }}</td>
                                                        <td >
                                                            @if(Auth::user()->users_type == "Qt" || Auth::user()->users_type == "HR")

                                                              <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#recordinglink{{ $data->lead_trackid }}" style="color: red">
                                                                <span style="color: #C53431"><i class="fa fa-plus-square-o" title="Add recording link"></i> RecordLink Add</span>
                                                              </a>

                                                                <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="{{url('/portal/customersinformation/details/lead/'. $data->lead_trackid)}}"  style="color: red;width: 30px;">
                                                                    <span style="color: #C53431"><i class="fa fa-list"></i> </span>Details
                                                                </a>&nbsp;

                                                                <div id="recordinglink{{ $data->lead_trackid }}" class="modal fade" role="dialog">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <form method="POST" action="{{ URL::to('/portal/lead/addrecording') }}">
                                                                                {{ csrf_field() }}
                                                                                <input type="hidden" name="lead_trackid" id="house_item_track_id" value="{{ $data->lead_trackid }}"/>
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                    <h5 class="modal-title" style="text-align: center;">Recording Link Box</h5>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    <div class="col-sm-6">
                                                                                        <input type="text" class="form-control" placeholder="input recording link---" name="recodinglink">
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-success center-block"><i class="fa fa-plus"></i>&nbsp;Add Link</button>

                                                                                    </div>
                                                                                              </div>
                                                                                <div class="modal-footer">
                                                                                                 </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div> <!--- End Active Modal -->

                                                            @else
                                                            <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="{{url('/portal/customersinformation/details/'. $data->lead_trackid)}}"  style="color: red;width: 30px;">
                                                                <span style="color: #C53431"><i class="fa fa-list"></i> </span>Details
                                                            </a>&nbsp;
                                                            @endif
                                                                <!-- End Inactive Model -->
                                                            <!-- Modal For Active User Admin -->
                                                        </td>
                                                    </tr>
                                                    @php
                                                        $count++;
                                                    @endphp
                                                @endforeach
                                            @endif
                                            </tbody>
                                            <tfoot>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <script>
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#houseList').DataTable({
                "aaSorting": []
            });
        });
    </script>
@endsection