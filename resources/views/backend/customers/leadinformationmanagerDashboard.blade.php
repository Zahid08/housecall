<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"><i class="fa fa-list"></i> <SPAN>Lead Information</SPAN>
                            <span onclick="filter();" style="float: right;cursor:pointer;">Filter</span>
                            </div>
                            <script type="text/javascript">
                             function filter() {
                            $('#filtering').slideToggle();
                             }
                            </script>
                            <fieldset style="border: 1px solid #435452 !important;border-radius: 0px;">
                                <div class="panel-body" style="padding: 0px;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- Alert Section-->
                                            @if (session('errorArray'))
                                                <div class="alert alert-danger">
                                                    @foreach($errors->all() AS $key => $value)
                                                        <strong><i class="fa fa-warning"></i> {{ $value }}</strong><br>
                                                    @endforeach
                                                </div>
                                            @endif
                                            @if (session('error'))
                                                <div class="alert alert-danger"  id="error">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('error') }}</strong>
                                                </div>
                                            @endif
                                            @if (session('success'))
                                                <div class="alert alert-success"  id="success">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('success') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-12" id="filtering" style="display: none" >
                                        <div style="width: 120px;" class="col-sm-1">
                                            <select class="form-control" onchange="demo();" id="leadstatelist">
                                                <option value="stateselect">State</option>
                                                @foreach($stateList as $data)
                                                <option value="{{$data->state}}">{{$data->state}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div style="width: 120px;" class="col-sm-1">
                                            <select class="form-control" onchange="demo();" id="status">
                                                <option value="status">status</option>
                                                <option value="JD" id="pass">Job Done</option>
                                                <option value="CAN" id="cancel">Cancel</option>
                                                <option value="RE" id="reshediule">Reshedule</option>
                                                <option value="CB" id="callback">Call Back</option>
                                            </select>
                                        </div>
                                        <div style="width: 120px;" class="col-sm-1">
                                            <select class="form-control" onchange="demo();" id="center">
                                                <option value="center">center</option>
                                                @foreach($centername as $data)
                                                    <?php
                                                    $centername=\App\User::where('users_track_id','=',$data->center_name)->first();
                                                    ?>
                                                <option value="{{$data->center_name}}">{{$centername->users_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div style="width: 150px;" class="col-sm-2">
                                            <select class="form-control" onchange="demo();" id="postalcode" >
                                                <option value="postalcode">postalcode</option>
                                                @foreach($postalcode as $data)
                                                <option  value="{{$data->postal_code}}">{{$data->postal_code}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div style="width: 200px;" class="col-sm-2">
                                            <input type="text" class="form-control pull-right" id="datepicker" name="appointment_date"  placeholder="call date">
                                            <script>
                                                $( function() {
                                                    $( "#datepicker" ).datepicker();
                                                } );

                                            </script>
                                        </div>
                                        <div style="width: 200px;" class="col-sm-2">
                                            <input type="text" class="form-control pull-right" id="datepicker1" name="appointment_date"  placeholder="Appointment date">
                                            <script>

                                                $( function() {
                                                    $( "#datepicker1" ).datepicker();
                                                } );

                                            </script>
                                        </div>
                                    </div>
                                    <div class="box-body table-responsive no-padding">
                                        <table id="leadlist" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%" style="margin-top: 10px;">
                                            <thead style="background: #537171;color: white;">
                                            <tr>
                                                <th>QT Name
                                                </th>
                                                <th>QTStaus</th>
                                                <th>Call Date</th>
                                                <th> Tech Name</th>
                                                <th> Appointment Date  </th>
                                                <th> Center Name  </th>
                                                <th>Confirmed By</th>
                                                <th>Booked Date</th>
                                                <th> Owner Name </th>
                                                <th> State  </th>
                                                <th> Status </th>
                                                <th> Assign </th>
                                                <th class="no-sort"> Action  </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($dataList))
                                                @foreach($dataList as $data)
                                                    <tr>
                                                        <td id="qt_status">
                                                            {{$data->Qt_name}}
                                                        </td>
                                                        <td>
                                                            @if($data->Qt_status === 'Pass')
                                                                <span class="label label-success" style="color: #fff;font-size: 10px"> Pass</span>
                                                            @elseif($data->Qt_status === 'Hold')
                                                                <span class="label label-info" style="background-color: red; color: #fff;font-size: 10px">Hold</span>
                                                            @elseif($data->Qt_status === 'Cancel')
                                                                <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Cancel </span>
                                                            @endif
                                                        </td>

                                                        <td>
                                                            {{$data->call_date}}
                                                        </td>
                                                        <td>
                                                            {{$data->tec_name}}
                                                        </td>
                                                        <td>{{ $data->appointment_date }}</td>
                                                        <td>
                                                            <?php
                                                            $centername=$data->center_name;
                                                            $userdata=\App\User::where('users_track_id',$data->center_name)->first();
                                                            $centartaken=\App\RevenueCalculated::where('center_name',$data->center_name)->first();
                                                            ?>
                                                            <span class="label label-default" style="color: #fff;font-size: 10px"> {{$userdata->users_name}}
                                                            </span>

                                                        </td>
                                                        <td>
                                                            {{$data->Qt_name}}
                                                        </td>
                                                        <td>
                                                            bok date
                                                        </td>
                                                        </td>
                                                        <td>{{ $data->owners_name }}</td>
                                                        <td>
                                                            <span class="label label-success" style="color: #fff;font-size: 10px">  {{ $data->state }}</span>
                                                        </td>

                                                        <td>
                                                            @if($data->Qm_status === 'JD')
                                                                <span class="label label-success" style="color: #fff;font-size: 10px"> JD</span>
                                                            @elseif($data->Qm_status === 'CAN')
                                                                <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">CAN</span>
                                                            @elseif($data->Qm_status === 'RE')
                                                                <span class="label label-info" style="background-color: red; color: #fff;font-size: 10px">RE </span>
                                                            @elseif($data->Qm_status === 'CB')
                                                                <span class="label label-warning" style="background-color: red; color: #fff;font-size: 10px">CB </span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if($data->tec_trackid !=NULL)
                                                                <span class="label label-success" style="color: #fff;font-size: 10px"> Assign</span>
                                                            @endif
                                                        </td>
                                                        <td style="width: 200px;">
                                                            <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#assign{{ $data->lead_trackid}}">
                                                                <span style="color: #C4302E"><i class="fa fa-list"></i></span> Assign
                                                            </a>

                                                            <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#inActive{{ $data->lead_trackid}}">
                                                                <span style="color: #C4302E"><i class="fa fa-list"></i></span> Details
                                                            </a>

                                                            <div id="assign{{ $data->lead_trackid}}" class="modal " role="dialog">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content" style="width: 900px;height: 1800px;">
                                                                        <input type="hidden" name="tec_trackid" id="house_item_track_id" value="{{ $data->lead_trackid}}"/>
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        </div>
                                                                        <div class="modal-body" style="height: 1800px;">

                                                                            <?php
                                                                            $leadtrakid=$data->lead_trackid;
                                                                            $datalead=\App\LeadInformationModel::where('lead_trackid',$leadtrakid)->first();
                                                                            $dataListassign=\App\TechniciansModel::where('state','=',$datalead->state)->get();
                                                                            ?>

                                                                            <div class="box-body table-responsive no-padding">
                                                                                <table id="houseList" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                                                    <thead style="background: #537171;color: white;">
                                                                                    <tr>
                                                                                        <th> TechniciansName</th>
                                                                                        <th> PhoneNumber </th>
                                                                                        <th> Email </th>
                                                                                        <th> State </th>
                                                                                        <th> Country  </th>
                                                                                        <th> StreetAddress </th>
                                                                                        <th> Status </th>
                                                                                        <th> Action </th>

                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    @if(!empty($dataListassign))
                                                                                        @foreach($dataListassign as $dataassign)
                                                                                            <tr>


                                                                                                <td>{{$dataassign->tec_name}}</td>
                                                                                                <td>{{$dataassign->tec_phone_number}}</td>
                                                                                                <td>{{$dataassign->email}}</td>
                                                                                                <td> {{$dataassign->state}}</td>
                                                                                                <td><span class="label label-success" style="color: #fff;font-size: 10px">{{$dataassign->tec_country}}</span></td>
                                                                                                <td><span class="label" style="color: #fff;font-size: 10px"><a href="" target="_blank">{{$dataassign->tec_street_address}}</a></span></td>
                                                                                                <td>

                                                                                                    @if($dataassign->tec_status === 'Active')
                                                                                                        <span class="label label-success" style="color: #fff;font-size: 10px"> Active</span>
                                                                                                    @else
                                                                                                        <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Inactive</span>
                                                                                                    @endif
                                                                                                </td>
                                                                                                <td>
                                                                                                    <form method="POST" action="{{ URL::to('portal/qmDashboard/customerinfo/assign') }}">
                                                                                                        {{ csrf_field() }}
                                                                                                        <input type="hidden" name="tec_trackid" id="users_track_id" value="{{ $data->tec_trackid }}"/>
                                                                                                        <input type="hidden" name="lead_trackid" id="users_track_id" value="{{ $leadtrakid }}"/>
                                                                                                        <button  style="padding: 0px 6px;font-size: 12px;"  type="submit" id="btnDelete" name="btnDelete" class="btn btn-success center-block"><i class="fa fa-save"></i>&nbsp;Yes</button>
                                                                                                    </form>
                                                                                                </td>
                                                                                            </tr>
                                                                                        @endforeach
                                                                                    @endif
                                                                                    </tbody>
                                                                                    <tfoot>
                                                                                    </tfoot>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div id="inActive{{ $data->lead_trackid}}" class="modal " role="dialog">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content" style="width: 900px;height: 1800px;">
                                                                            <input type="hidden" name="tec_trackid" id="house_item_track_id" value="{{ $data->lead_trackid}}"/>
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                            </div>
                                                                            <div class="modal-body" style="height: 1800px;">

                                                                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

                                                                                <div class="panel-body">
                                                                                                            <div class="panel panel-primary">
                                                                                                                <div class="panel-body">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <h5> Lead Status - <span class="label label-default"> Active  </span></h5>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-4">
                                                                                                                        <h5>Lead Created  - <span class="label label-default"> {{ $data->created_at->format('d-m-Y') }}</span></h5>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-4"><span id="ulead{{$data->lead_trackid}}">
                                                                                                                    <a onclick="edit('{{ $data->lead_trackid}}')" data-toggle="modal"  style="float: right;margin-top:5px;color: black;font-size: 16px;cursor: pointer">
                                                                                                                       <span class="label label-default"> Update Lead Information</span>
                                                                                                                    </a>
                                                                                                                    </span>
                                                                                                                    </div>


                                                                                                                    <div class="clearfix"></div>
                                                                                                                    <div class="panel panel-default">
                                                                                                                        <div class="panel-heading" id="" style="background-color:#537171; color: white;">Agent Lead Information</div>
                                                                                                                        <div class="panel-body">
                                                                                                                            <div class="col-sm-12">
                                                                                                                                <table class="table table-responsive table-striped table-bordered">
                                                                                                                                    <tr>
                                                                                                                                        <th> Qc <span class="label label-success" style="color: #fff;font-size: 10px;margin-left: 100px;">Qualit Team</span>  </th>
                                                                                                                                        <th style="width: 500px;"><span class="label label-success" style="color: #fff;font-size: 10px"> {{$data->Qt_name}}</span></th>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <th>Qc Status  </th>
                                                                                                                                        <th>
                                                                                                                                            @if($data->Qt_status === 'Pass')
                                                                                                                                                <span class="label label-success" style="color: #fff;font-size: 10px"> Pass</span>
                                                                                                                                            @elseif($data->Qt_status === 'Hold')
                                                                                                                                                <span class="label label-info" style="background-color: red; color: #fff;font-size: 10px">Hold</span>
                                                                                                                                            @elseif($data->Qt_status === 'Cancel')
                                                                                                                                                <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Cancel </span>
                                                                                                                                            @endif

                                                                                                                                        </th>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <th style="width: 500px;"> Time stamp  </th>
                                                                                                                                        <th> {{$data->Timestamp}} </th>
                                                                                                                                    </tr>
                                                                                                                                    <td>CenterName</td>
                                                                                                                                    <td>
                                                                                                                                        <?php
                                                                                                                                        $centername=$data->center_name;
                                                                                                                                        $userdata=\App\User::where('users_track_id',$data->center_name)->first();
                                                                                                                                        $centartaken=\App\RevenueCalculated::where('center_name',$data->center_name)->first();
                                                                                                                                        ?>
                                                                                                                                        <span class="label label-default" style="color: #fff;font-size: 10px"> {{$userdata->users_name}}
                                                                                                                                        </span>
                                                                                                                                    </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <th> Agent Name  </th>
                                                                                                                                        <th> {{$data->agent_name}}</th>
                                                                                                                                    </tr>

                                                                                                                                    <tr>
                                                                                                                                        <th> Appointment Date   </th>
                                                                                                                                        <th id="ad" > {{$data->appointment_date}} </th>
                                                                                                                                        <input type="hidden" id="apointment" value="{{$data->appointment_date}}">

                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td>Appointment Time</td>
                                                                                                                                        <th id="ad" > {{$data->appointment_time}} </th>
                                                                                                                                        <input type="hidden" id="apointment" value="{{$data->appointment_time}}">

                                                                                                                                    </tr>

                                                                                                                                    <tr>
                                                                                                                                        <td>Owner Name </td>
                                                                                                                                        <th id="own{{$data->lead_trackid}}" > {{$data->owners_name}}
                                                                                                                                            <input type="hidden" id="ownername{{$data->lead_trackid}}" value="{{$data->owners_name}}">
                                                                                                                                        </th>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <th> Phone Number  </th>
                                                                                                                                        <th id="pn{{$data->lead_trackid}}" > {{$data->phone_number}} </th>
                                                                                                                                        <input type="hidden" id="phone{{$data->lead_trackid}}" value="{{$data->phone_number}}">
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td> ALternative Number  </td>
                                                                                                                                        <th id="aln{{$data->lead_trackid}}" > {{$data->emergency_number}} </th>
                                                                                                                                        <input type="hidden" id="alternative{{$data->lead_trackid}}" value="{{$data->emergency_number}}">

                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td>Street Address</td>
                                                                                                                                        <th id="sa{{$data->lead_trackid}}" > {{$data->street_address}} </th>
                                                                                                                                        <input type="hidden" id="streeta{{$data->lead_trackid}}" value="{{$data->street_address}}">

                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <th> City  </th>
                                                                                                                                        <th id="c{{$data->lead_trackid}}" > {{$data->city}} </th>
                                                                                                                                        <input type="hidden" id="city{{$data->lead_trackid}}" value="{{$data->city}}">
                                                                                                                                    </tr>

                                                                                                                                    <tr>
                                                                                                                                        <td>State</td>
                                                                                                                                        <th id="s{{$data->lead_trackid}}" > {{$data->state}} </th>
                                                                                                                                        <input type="hidden" id="state{{$data->lead_trackid}}" value="{{$data->state}}">
                                                                                                                                    </tr>

                                                                                                                                    <tr>
                                                                                                                                        <th> Postal Code   </th>
                                                                                                                                        <th id="pc{{$data->lead_trackid}}" > {{$data->postal_code}} </th>
                                                                                                                                        <input type="hidden" id="postalcode{{$data->lead_trackid}}" value="{{$data->postal_code}}">
                                                                                                                                    </tr>

                                                                                                                                    <tr>
                                                                                                                                        <td>Comments</td>
                                                                                                                                        <th id="cmnt{{$data->lead_trackid}}" > {{$data->comments}} </th>
                                                                                                                                        <input type="hidden" id="comment{{$data->lead_trackid}}" value="{{$data->comments}}">

                                                                                                                                    </tr>

                                                                                                                                    <tr>
                                                                                                                                        <th> Closer Name   </th>
                                                                                                                                        <th id="cn{{$data->lead_trackid}}" > {{$data->Colsers_name}} </th>
                                                                                                                                        <input type="hidden" id="closername{{$data->lead_trackid}}" value="{{$data->Colsers_name}}">

                                                                                                                                        </th>
                                                                                                                                    </tr>
                                                                                                                                </table>

                                                                                                                                <table class="table table-responsive table-striped table-bordered">
                                                                                                                                    <tr>
                                                                                                                                        <td style="width: 500px;">Call Date  <span class="label label-success" style="color: #fff;font-size: 10px;margin-left: 100px;">Qualit Manager</span> </td>
                                                                                                                                        <td style="width: 500px;">
                                                                                                                                            {{$data->call_date}}
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <th>Technicians Name  </th>
                                                                                                                                        <th><span class="label label-info" style="color: #fff;font-size: 10px"> {{$data->tec_name}}</span></th>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <th> Appointment Date   </th>
                                                                                                                                        <th> {{$data->appointment_date}}   </th>
                                                                                                                                    </tr>
                                                                                                                                    <td>CenterName</td>
                                                                                                                                    <td>
                                                                                                                                        <?php
                                                                                                                                        $centername=$data->center_name;
                                                                                                                                        $userdata=\App\User::where('users_track_id',$data->center_name)->first();
                                                                                                                                        $centartaken=\App\RevenueCalculated::where('center_name',$data->center_name)->first();
                                                                                                                                        ?>
                                                                                                                                        <span class="label label-default" style="color: #fff;font-size: 10px"> {{$userdata->users_name}}
                                                                                                                                                </span>
                                                                                                                                    </td>
                                                                                                                                    <tr>
                                                                                                                                        <th> Confirmed By   </th>
                                                                                                                                        <th> {{$data->appointment_date}}   </th>
                                                                                                                                    </tr>

                                                                                                                                    <tr>
                                                                                                                                        <th> Booking Date  </th>
                                                                                                                                        <th> {{$data->appointment_date}}   </th>
                                                                                                                                    </tr>

                                                                                                                                    <tr>
                                                                                                                                        <th> Agent Name  </th>
                                                                                                                                        <th> {{$data->agent_name}}</th>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td>Owner Name </td>
                                                                                                                                        <td>{{$data->owners_name}}</td>

                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <th> Phone Number  </th>
                                                                                                                                        <th> {{$data->phone_number}}  </th>
                                                                                                                                    </tr>

                                                                                                                                    <tr>
                                                                                                                                        <th> City  </th>
                                                                                                                                        <th> {{$data->city}}   </th>
                                                                                                                                    </tr>

                                                                                                                                    <tr>
                                                                                                                                        <td>State</td>
                                                                                                                                        <td>{{$data->state}} </td>
                                                                                                                                    </tr>

                                                                                                                                    <tr>
                                                                                                                                        <th> Postal Code   </th>
                                                                                                                                        <th> {{$data->postal_code}}   </th>
                                                                                                                                    </tr>

                                                                                                                                    <tr>
                                                                                                                                        <td>Job Status </td>
                                                                                                                                        <td>
                                                                                                                                            @if($data->Qm_status === 'CB')
                                                                                                                                                <span class="label label-info" style="color: #fff;font-size: 10px"> CALL BACK</span>
                                                                                                                                            @elseif($data->Qm_status === 'Hold')
                                                                                                                                                <span class="label label-info" style="background-color: red; color: #fff;font-size: 10px">Hold</span>
                                                                                                                                            @elseif($data->Qm_status === 'Cancel')
                                                                                                                                                <span class="label label-info" style="background-color: red; color: #fff;font-size: 10px">Cancel </span>
                                                                                                                                            @endif
                                                                                                                                        </td>
                                                                                                                                    </tr>

                                                                                                                                    <tr>
                                                                                                                                        <th> Amount   </th>
                                                                                                                                        <th> {{$data->	lead_ammount}}   </th>
                                                                                                                                    </tr>


                                                                                                                                    <tr>
                                                                                                                                        <th> Recording Link  </th>
                                                                                                                                        <th><span class="label label-success" style="color: #fff;font-size: 10px"> {{$data->recodinglink}}</span></th>
                                                                                                                                    </tr>

                                                                                                                                    <tr>
                                                                                                                                        <th> Assign Status  </th>
                                                                                                                                        <td>
                                                                                                                                            @if($data->tec_trackid !=NULL)
                                                                                                                                                <span class="label label-success" style="color: #fff;font-size: 10px"> Assign</span>
                                                                                                                                            @endif
                                                                                                                                        </td>
                                                                                                                                    </tr>

                                                                                                                                    <tr>
                                                                                                                                        <th>
                                                                                                                                            Action
                                                                                                                                        </th>
                                                                                                                                        <td>
                                                                                                                                            <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;"   style="color: red" onclick="jobstus('{{$data->lead_trackid}}')" id="jobstatus">
                                                                                                                                                <span style="color: #C53431"><i class="fa fa-save"></i> </span>JobStatus
                                                                                                                                            </a>&nbsp;
                                                                                                                                            <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;"   style="color: red" onclick="Qmstatus('{{$data->lead_trackid}}')" id="qmstatus">
                                                                                                                                                <span style="color: #C53431"><i class="fa fa-save"></i> </span>QmStatus
                                                                                                                                            </a>&nbsp;
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="panel panel-default" style="display:none;" id="displayInfo{{$data->lead_trackid}}" >
                                                                                                                        <div class="panel-heading" id="" style="background-color:#537171; color: white;">Job Done Status Change
                                                                                                                        </div>
                                                                                                                        <div class="panel-body">
                                                                                                                            <div class="col-md-12">
                                                                                                                                <form method="POST" action="{{ URL::to('/portal/lead/changeQm') }}">
                                                                                                                                    {{ csrf_field() }}
                                                                                                                                    <input type="hidden" name="lead_trackid" id="house_item_track_id" value="{{ $data->lead_trackid }}"/>
                                                                                                                                    <div class="col-sm-12">
                                                                                                                                        <div class="form-group col-sm-6 col-sm-offset-1 ">
                                                                                                                                            <label for="users_name">Select Status :<span class="mark">*</span> </label>
                                                                                                                                            <select class="form-control" id="statusinfo" name="Qm_status" onchange="changestatus('{{$data->lead_trackid}}');">
                                                                                                                                                <option value="none">Select Status------</option>
                                                                                                                                                <option value="JD" id="pass{{$data->lead_trackid}}" >Job Done</option>
                                                                                                                                                <option value="CAN" id="cancel{{$data->lead_trackid}}">Cancel</option>
                                                                                                                                                <option value="RE" id="reshediule{{$data->lead_trackid}}">Reshedule</option>
                                                                                                                                                <option value="Hold" id="callback{{$data->lead_trackid}}">Call Back</option>
                                                                                                                                            </select>
                                                                                                                                            <small class="text-danger"></small>
                                                                                                                                        </div>

                                                                                                                                    </div>
                                                                                                                                    <div id="pay_block{{$data->lead_trackid}}" style="display: none">
                                                                                                                                        <div class="col-sm-12 col-sm-offset-1 ">
                                                                                                                                            <div class="form-group col-sm-3 ">
                                                                                                                                                <label for="users_email">Start Time :<span class="mark">*</span></label>
                                                                                                                                                <input class="form-control" type="text" id="firstime"  name="appointment_time"/>
                                                                                                                                                <small class="text-danger"></small>
                                                                                                                                            </div>
                                                                                                                                            <div class="form-group col-sm-3 ">
                                                                                                                                                <label for="users_email">End Time :<span class="mark">*</span></label>
                                                                                                                                                <input type="text" class="form-control " id="secondtime" value="" name="appointment_time1">
                                                                                                                                                <small class="text-danger"></small>
                                                                                                                                            </div>
                                                                                                                                            <div class="col-sm-3" style="margin-top: 30px;">
                                                                                                                                                <label class="radio-inline">
                                                                                                                                                    <input type="radio" name="optradio" id="twohours" value="2">2 hours
                                                                                                                                                </label>
                                                                                                                                                <label class="radio-inline">
                                                                                                                                                    <input type="radio" name="optradio" id="threehours" value="3">3 hours
                                                                                                                                                </label>
                                                                                                                                            </div>
                                                                                                                                        </div>

                                                                                                                                        <script>
                                                                                                                                            $('#firstime').datetimepicker({
                                                                                                                                                format: 'HH:mm a'

                                                                                                                                            });
                                                                                                                                        </script>

                                                                                                                                        <div class="col-sm-12">
                                                                                                                                            <div class="form-group col-sm-6 col-sm-offset-1 ">
                                                                                                                                                <label for="users_birthdate">Appointment Date :<span class="mark">*</span> </label>
                                                                                                                                                <div class="input-group ">
                                                                                                                                                    <div class="input-group-addon">
                                                                                                                                                        <i class="fa fa-calendar"></i>
                                                                                                                                                    </div>
                                                                                                                                                    <input type="text" class="form-control pull-right" id="datepicker" name="appointment_date" required>

                                                                                                                                                    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                                                                                                                                                    <link rel="stylesheet" href="/resources/demos/style.css">
                                                                                                                                                    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                                                                                                                                                    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

                                                                                                                                                    <script>

                                                                                                                                                        $( function() {
                                                                                                                                                            $( "#datepicker" ).datepicker();
                                                                                                                                                        } );

                                                                                                                                                    </script>

                                                                                                                                                </div>
                                                                                                                                                <small class="text-danger">{{ $errors->first('users_birthdate') }}</small>
                                                                                                                                            </div>
                                                                                                                                            <div class="col-sm-6"></div>
                                                                                                                                        </div>

                                                                                                                                        <div class="col-sm-3 col-sm-offset-4" style="margin-top: 20px;" >
                                                                                                                                            <button  id="btn" type="submit" return="false" name="btnSaveClass" class="btn  btn-sm btn-block" style="background-color:#537171;color: white;height: 35px;"><i class="fa fa-edit"></i> Update Status </button>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </form>
                                                                                                                                <div id="pay_block1{{$data->lead_trackid}}" style="display: none">
                                                                                                                                    <form method="POST" action="{{ URL::to('/portal/lead/changeQm') }}">
                                                                                                                                        {{ csrf_field() }}
                                                                                                                                        <input type="hidden" name="lead_trackid" id="house_item_track_id" value="{{ $data->lead_trackid }}"/>
                                                                                                                                        <input type="hidden" name="Qm_status" value="JD">

                                                                                                                                        <div class="form-group col-sm-3 col-sm-offset-1" >
                                                                                                                                            <label for="users_email">Amount :<span class="mark">*</span></label>
                                                                                                                                            <input type="text" class="form-control " id="secondtime" value="" name="amount">
                                                                                                                                            <small class="text-danger"></small>
                                                                                                                                        </div>

                                                                                                                                        <div class="col-sm-3" style="margin-top: 20px;" >
                                                                                                                                            <button  id="btn" type="submit" return="false" name="btnSaveClass" class="btn  btn-sm btn-block" style="background-color:#537171;color: white;height: 35px;"><i class="fa fa-edit"></i> Update Status </button>
                                                                                                                                        </div>
                                                                                                                                    </form>
                                                                                                                                </div>

                                                                                                                                <div id="cancel_payblock{{$data->lead_trackid}}" style="display: none">
                                                                                                                                    <form method="POST" action="{{ URL::to('/portal/lead/changeQm') }}">
                                                                                                                                        {{ csrf_field() }}
                                                                                                                                        <input type="hidden" name="lead_trackid" id="house_item_track_id" value="{{ $data->lead_trackid }}"/>
                                                                                                                                        <input type="hidden" name="Qm_status" value="CAN">
                                                                                                                                        <div class="col-sm-3 col-sm-offset-3" style="margin-top: 20px;" >
                                                                                                                                            <button  id="btn" type="submit" return="false" name="btnSaveClass" class="btn  btn-sm btn-block" style="background-color:#537171;color: white;height: 35px;"><i class="fa fa-edit"></i> Update Status </button>
                                                                                                                                        </div>
                                                                                                                                    </form>
                                                                                                                                </div>
                                                                                                                                <div id="callback_payroll{{$data->lead_trackid}}" style="display: none">
                                                                                                                                    <form method="POST" action="{{ URL::to('/portal/lead/changeQm') }}">
                                                                                                                                        {{ csrf_field() }}
                                                                                                                                        <input type="hidden" name="lead_trackid" id="house_item_track_id" value="{{ $data->lead_trackid }}"/>
                                                                                                                                        <input type="hidden" name="Qm_status" value="CB">
                                                                                                                                        <div class="col-sm-3 col-sm-offset-3" style="margin-top: 20px;" >
                                                                                                                                            <button  id="btn" type="submit" return="false" name="btnSaveClass" class="btn  btn-sm btn-block" style="background-color:#537171;color: white;height: 35px;"><i class="fa fa-edit"></i> Update Status </button>
                                                                                                                                        </div>
                                                                                                                                    </form>
                                                                                                                                </div>


                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="panel panel-default" style="display:none;" id="displayInfoqm{{$data->lead_trackid}}">
                                                                                                                        <div class="panel-heading" id="" style="background-color:#537171; color: white;">Quality team  Status
                                                                                                                        </div>
                                                                                                                        <form method="POST" action="{{ URL::to('/portal/lead/changestatusqm') }}">
                                                                                                                            {{ csrf_field() }}
                                                                                                                            <input type="hidden" name="lead_trackid" id="house_item_track_id" value="{{ $data->lead_trackid }}"/>
                                                                                                                            <div class="col-sm-12" style="margin-top: 20px;">
                                                                                                                                <div class="form-group col-sm-4  ">
                                                                                                                                    <label for="users_name">Select Status :<span class="mark">*</span> </label>
                                                                                                                                    <select class="form-control" id="users_gender" name="Qt_status">
                                                                                                                                        <option value="NULL">Select Status------</option>
                                                                                                                                        <option value="Pass" id="pass" >Pass</option>
                                                                                                                                        <option value="Cancel" i>Cancel</option>
                                                                                                                                        <option value="Hold">Hold</option>
                                                                                                                                    </select>
                                                                                                                                    <small class="text-danger"></small>
                                                                                                                                </div>
                                                                                                                                <div class="form-group col-sm-4 " style="margin-top: 20px;">
                                                                                                                                    <input type="text" name="recordlink" class="form-control" placeholder="record link add.................">
                                                                                                                                </div>
                                                                                                                                <div class="col-sm-3 "  style="margin-top: 20px;">
                                                                                                                                    <button  id="btn" type="submit" return="false" name="btnSaveClass" class="btn  btn-sm btn-block" style="background-color:#537171;color: white;height: 35px;"><i class="fa fa-edit"></i> Update Status </button>
                                                                                                                                </div>

                                                                                                                            </div>
                                                                                                                        </form>
                                                                                                                    </div>

                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                            <tfoot>
                                            </tfoot>
                                        </table>
                                        <table id="tbDetails"  class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%" style="margin-top: 10px;display: none">
                                            <thead style="background: #537171;color: white;">
                                            <tr>
                                                <th>QT Name
                                                </th>
                                                <th>QT Staus</th>
                                                <th>Call Date</th>
                                                <th> Tech Name</th>
                                                <th> Appointment Date  </th>
                                                <th> Center Name  </th>
                                                <th>Confirmed By</th>
                                                <th>Booked Date</th>
                                                <th> Owner Name </th>
                                                <th> State  </th>
                                                <th> Status </th>
                                                <th> Assign </th>
                                                <th class="no-sort"> Action  </th>
                                            </tr>
                                            </thead>
                                            <tbody id="leadinfo">
                                            </tbody>
                                            <tfoot>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <script>
        function jobstus(x) {
            $("#displayInfo"+x).show('slow');
            $('#displayInfoqm'+x).hide('slow');
        }
        function Qmstatus(x) {
            $('#displayInfoqm'+x).show('slow');
            $('#displayInfo'+x).hide('slow');
        }
        function assigntec() {

        }
    </script>

    <script>
        function changestatus(x) {
            if ($("#cancel"+x).is(":selected")) {
                $("#pay_block1"+x).slideUp("slow");
                $("#pay_block"+x).slideUp("slow");
                $("#cancel_payblock"+x).slideDown("slow");
            }
            else if ($("#reshediule"+x).is(":selected")) {
                $("#pay_block1"+x).slideUp("slow");
                $("#cancel_payblock"+x).slideUp("slow");
                $("#pay_block"+x).slideDown("slow");
            }
            else if ($("#callback"+x).is(":selected")) {
                $("#pay_block1"+x).slideUp("slow");
                $("#cancel_payblock"+x).slideUp("slow");
                $("#pay_block"+x).slideUp("slow");
                $("#callback_payroll"+x).slideDown("slow");
            }

            else if ($("#pass" + x).is(":selected")) {
                $("#pay_block1"+x).slideDown("slow");
                $("#cancel_payblock"+x).slideUp("slow");
                $("#pay_block"+x).slideUp("slow");
                $("#callback_payroll"+x).slideUp("slow");
            }
            else {
                $("#pay_block"+x).slideUp("slow");
            }
        }


    </script>


    <script>
        $(document).ready(function(){
            $("#twohours").click(function(){

                var  ftime=$("#firstime").val();

                var hour=(parseInt(ftime.split(":")[0])+2)%12;
                var min=ftime.split(":")[1].split(" ")[0];
                am=ftime.split(":")[1].split(" ")[1];

                if((parseInt(ftime.split(":")[0])+2)/12>=1)
                {  console.log("hi");
                    if(am=="am")
                        am="pm";
                    else
                        am="am";
                }

                $("#secondtime").val(hour+":"+min+" "+am);
            });

            $("#threehours").click(function(){

                var  ftime=$("#firstime").val();

                var hour=(parseInt(ftime.split(":")[0])+3)%12;
                var min=ftime.split(":")[1].split(" ")[0];
                am=ftime.split(":")[1].split(" ")[1];

                if((parseInt(ftime.split(":")[0])+2)/12>=1)
                {  console.log("hi");
                    if(am=="am")
                        am="pm";
                    else
                        am="am";
                }

                $("#secondtime").val(hour+":"+min+" "+am);
            });

        });

    </script>

    <script type="text/javascript">
        function  demo() {
            $("#tbDetails tbody").find('tr').remove();
            var row = "";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: '/portal/qmDashboard/customerinfo/search',
                dataType: 'json',
                data: {
                    state: $('#leadstatelist').val(),
                    status: $('#status').val(),
                    center: $('#center').val(),
                    postalcode: $('#postalcode').val(),
                },
                success: function (response) {
                    $('#tbDetails').show('slow')
                    var data = response.datalist;
                    $('#leadlist').hide('slow');
                    $('#searchlist').show('slow');
                    for (i = 0; i < response.dataList.length; i++) {
                        var leadtrackid=response.dataList[i].lead_trackid;

                        var qtname=response.dataList[i].Qt_name;

                        var qtstatus=response.dataList[i].Qt_status;
                        var calldate=response.dataList[i].call_date;
                        var technamae=response.dataList[i].tec_name;
                        var apointmentdate=response.dataList[i].appointment_date;
                        var centername=response.centername;
                        var confirmedby=response.dataList[i].Qt_name;
                        var bookeddate='book date';
                        var ownername=response.dataList[i].owners_name;
                        var stateone=response.dataList[i].state;
                        var status=response.dataList[i].Qm_status;
                        var assign=response.dataList[i].tec_trackid;
                        if(assign !=null){
                            var asiigntechstatus="Asign";
                        }
                        var baseUrl=window.location.protocol+"//"+window.location.hostname+":"+window.location.port+"/";
                        var action='<a type="button" href="'+baseUrl+'portal/leadformation/details/lead/'+leadtrackid+'"  id="details" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" style="color: red;width: 30px;">\n' +
                            '                                                                <span style="color: #C53431"><i class="fa fa-list"></i> </span>Details\n' +
                            '                                                            </a>&nbsp;'


                        var row = '<tr><td> ' + qtname + ' </td> <td> ' +qtstatus + ' </td> <td>' + calldate + '</td> <td>' + technamae+ '</td><td>'+ apointmentdate+ '</td><td>'+ centername+ '</td><td>'+ confirmedby+ '</td><td>'+ bookeddate+ '</td><td>'+ ownername+ '</td><td>'+ stateone+ '</td><td>'+ status+ '</td><td>'+ asiigntechstatus+ '</td><td>'+ action+ '</td>  </tr>'
                        $("#tbDetails tbody").append(row);
                    }
                }
            });
        }
        $(document).ready(function () {
            demo();
        });

    </script>


    <script type="text/javascript">
        function edit(x) {
            document.getElementById("ulead"+x).innerHTML="<a style='float: right;margin-top:5px;color: black;font-size: 16px;cursor: pointer' onclick=save('"+x+"') ><span class='label label-defaultlabel label-success'>Save Lead Information</span> </a>";
            phone=document.getElementById("phone"+x).value;
            ownername=document.getElementById("ownername"+x).value;

            alternativeno=document.getElementById("alternative"+x).value;
            streetaddress=document.getElementById("streeta"+x).value;
            city=document.getElementById("city"+x).value;
            state=document.getElementById("state"+x).value;
            postalcode=document.getElementById("postalcode"+x).value;
            closername=document.getElementById("closername"+x).value;
            comment=document.getElementById("comment"+x).value;

            document.getElementById("pn"+x).innerHTML="<input type='text' class='form-control' id='uphone"+x+"' style='border: 1px solid green;' value='"+phone+"'>";
            document.getElementById("own"+x).innerHTML="<input type='text' class='form-control' id='uownername"+x+"' style='border: 1px solid green;'  value='"+ownername+"'>";
            document.getElementById("aln"+x).innerHTML="<input type='text' class='form-control' id='ualternative"+x+"' style='border: 1px solid green;'  value='"+alternativeno+"'>";
            document.getElementById("sa"+x).innerHTML="<input type='text' class='form-control' id='ustreeta"+x+"' style='border: 1px solid green;' value='"+streetaddress+"'>";
            document.getElementById("c"+x).innerHTML="<input type='text' class='form-control' id='city"+x+"' style='border: 1px solid green;' value='"+city+"'>";
            document.getElementById("s"+x).innerHTML="<input type='text' class='form-control' id='ustate"+x+"' style='border: 1px solid green;' value='"+state+"'>";
            document.getElementById("pc"+x).innerHTML="<input type='text' class='form-control' id='upostalcode"+x+"'  style='border: 1px solid green;' value='"+postalcode+"'>";
            document.getElementById("cn"+x).innerHTML="<input type='text' class='form-control' id='uclosername"+x+"' style='border: 1px solid green;'  value='"+closername+"'>";
            document.getElementById("cmnt"+x).innerHTML="<input type='text' class='form-control' id='ucomment"+x+"' style='border: 1px solid green;'  value='"+comment+"'>";
        }
        function save(x) {
            phone=document.getElementById("uphone"+x).value;
            ownername=document.getElementById("uownername"+x).value;
            alternativeno=document.getElementById("ualternative"+x).value;
            streetaddress=document.getElementById("ustreeta"+x).value;
            city=document.getElementById("city"+x).value;
            state=document.getElementById("ustate"+x).value;
            postalcode=document.getElementById("upostalcode"+x).value;
            closername=document.getElementById("uclosername"+x).value;
            comment=document.getElementById("ucomment"+x).value;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '/portal/leadformation/details/lead/updateajax',
                dataType: 'json',
                data:{
                    lead_trackid:x,
                    phone:phone,
                    ownername:ownername,
                    alternativeno:alternativeno,
                    streetaddress:streetaddress,
                    city:city,
                    state:state,
                    postalcode:postalcode,
                    closername:closername,
                    comment:comment
                },
                success: function(){
                    document.getElementById("pn"+x).innerHTML=phone;
                    document.getElementById("own"+x).innerHTML=ownername;
                    document.getElementById("aln"+x).innerHTML=alternativeno;
                    document.getElementById("sa"+x).innerHTML=streetaddress;
                    document.getElementById("c"+x).innerHTML=city;
                    document.getElementById("s"+x).innerHTML=state;
                    document.getElementById("pc"+x).innerHTML=postalcode;
                    document.getElementById("cn"+x).innerHTML=closername;
                    document.getElementById("cmnt"+x).innerHTML=comment;

                }
            });
        }
    </script>


    <script type="text/javascript">
        function getstreetaddress(id) {
            var tec_trackid = id;
            if (tec_trackid !== '') {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: "/getstreetaddress",
                    dataType: 'json',
                    data: {
                        tec_trackid: tec_trackid
                    },
                    success: function (response) {
                        var obj = response;
                        if (obj.output === "success") {
                            var html = '<select  class="form-control requiredOL" id="conversation_sent_to" name="conversation_sent_to"><option value=""> Select street address </option>';
                            if (obj.streetaddress == "") {
                                html += '<option value=""> street address are not available in this country </option>';
                            } else {
                                $.each(obj.streetaddress, function (key, Event) {
                                    html += '<option value="' + Event.tec_trackid + '">' + Event.tec_street_address + '</option>';
                                });
                            }
                            html += '</select>';
                            $("#streetaddresshdiv").html(html);
                            $(this).parent("div").addClass("has-error");
                            $(this).siblings(".text-danger").removeClass("hide");
                        } else {
                            alert(obj.msg);
                        }
                    }
                });
            }
        }

    </script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script>
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#asd').DataTable({
                "aaSorting": []
            });
        });
    </script>

@endsection