<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>
@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"><i class="fa fa-list"></i> <SPAN>Job Done List <label style="margin-left: 750px;color: #DA8028;">Total Job Done :
                                        <?php
                                        $count=\App\LeadInformationModel::where('job_done','Yes')->count();
                                        ?>
                                        <span class="label label-success" style="color: #fff;font-size: 10px">{{$count}}</span>
                                    </label></SPAN></div>
                            <fieldset style="border: 1px solid #435452 !important;border-radius: 0px;">
                                <div class="panel-body" style="padding: 0px;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- Alert Section-->
                                            @if (session('errorArray'))
                                                <div class="alert alert-danger">
                                                    @foreach($errors->all() AS $key => $value)
                                                        <strong><i class="fa fa-warning"></i> {{ $value }}</strong><br>
                                                    @endforeach
                                                </div>
                                            @endif
                                            @if (session('error'))
                                                <div class="alert alert-danger"  id="error">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('error') }}</strong>
                                                </div>
                                            @endif
                                            @if (session('success'))
                                                <div class="alert alert-success"  id="success">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('success') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="box-body table-responsive no-padding">
                                        <table id="houseList" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                            <thead style="background: #537171;color: white;">
                                            <tr>
                                                <th> CenterName  </th>
                                                <th> OwnerName </th>
                                                <th> PhoneNo </th>
                                                <th> StreetAddress </th>
                                                <th> City  </th>
                                                <th> Status  </th>
                                                <th> Job Done By </th>
                                                <th> Paid Staus (TC) </th>
                                                <th> Amount </th>
                                                <th> Confirmed paid </th>
                                                <th> Action </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($dataList))
                                                @foreach($dataList as $data)
                                                    <tr>
                                                        <td>  <span class="label label-default" style="color: #fff;font-size: 10px">
                                                                <?php
                                                                $centername=$data->center_name;
                                                                $userdata=\App\User::where('users_track_id',$data->center_name)->first();
                                                                $centartaken=\App\RevenueCalculated::where('center_name',$data->center_name)->first();
                                                                ?>
                                                                <span class="label label-default" style="color: #fff;font-size: 10px"> {{$userdata->users_name}}
                                                            </span>


                                                            </span></td>
                                                        <td>{{ $data->owners_name }}</td>
                                                        <td>{{ $data->phone_number }}</td>
                                                        <td>{{ $data->street_address }}</td>
                                                        <td>{{ $data->city }}</td>
                                                        <td>
                                                            @if($data->job_done=="Yes")
                                                                <span class="label label-success" style="color: #fff;font-size: 10px">Job Done</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                        <?php
                                                            $technicianStatsu=\App\TechniciansModel::where('tec_trackid',$data->tec_trackid)->first();
                                                        ?>
                                                            @if(!empty($technicianStatsu->tec_name))
                                                            <span class="label label-success" style="color: #fff;font-size: 10px"> {{$technicianStatsu->tec_name}}</span>
                                                                @else
                                                                <span class="label label-success" style="color: #fff;font-size: 10px"></span>
                                                            @endif

                                                        </td>
                                                        <td>
                                                            @if($data->lead_ammount ===0)
                                                                <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Non Paid</span>
                                                            @else
                                                                <span class="label label-success" style="color: #fff;font-size: 10px"> Paid</span>
                                                            @endif
                                                        </td>
                                                        <td>
{{$data->lead_ammount}}
                                                        </td>

                                                        <td>
                                                            @if($data->	Qm_paid_status ==="Paid")
                                                                <span class="label label-success" style="color: #fff;font-size: 10px"> Paid</span>
                                                            @else
                                                                <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Non Paid</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#inActive{{ $data->lead_trackid }}" style="color: red">
                                                                <span style="color: #C53431"><i class="fa fa-save"></i> </span>Confirmed
                                                            </a>&nbsp;
                                                            <!--  Model For Inactive Any user Admin-->
                                                            <div id="inActive{{ $data->lead_trackid }}" class="modal fade" role="dialog">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <form method="POST" action="{{ URL::to('/portal/qmDashboard/jobdonelist/confirmation') }}">
                                                                            {{ csrf_field() }}
                                                                            <input type="hidden" name="lead_trackid" id="users_track_id" value="{{ $data->lead_trackid }}"/>
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                <h5 class="modal-title" style="text-align: center;">Do you want to confirmed this lead ?</h5>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit"  name="btnDelete" class="btn btn-success center-block"><i class="fa fa-save"></i>&nbsp;Confirmed</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div> <!-- End Inactive Model -->

                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                            <tfoot>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <script>
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#houseList').DataTable({
                "aaSorting": []
            });
        });
    </script>
@endsection