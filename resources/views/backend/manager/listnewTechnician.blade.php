<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>
@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"><i class="fa fa-list"></i> <SPAN>Technicians List</SPAN></div>
                            <fieldset style="border: 1px solid #435452 !important;border-radius: 0px;">
                                <div class="panel-body" style="padding: 0px;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- Alert Section-->
                                            @if (session('errorArray'))
                                                <div class="alert alert-danger">
                                                    @foreach($errors->all() AS $key => $value)
                                                        <strong><i class="fa fa-warning"></i> {{ $value }}</strong><br>
                                                    @endforeach
                                                </div>
                                            @endif
                                            @if (session('error'))
                                                <div class="alert alert-danger"  id="error">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('error') }}</strong>
                                                </div>
                                            @endif
                                            @if (session('success'))
                                                <div class="alert alert-success"  id="success">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('success') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="box-body table-responsive no-padding">
                                        <table id="houseList" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                            <thead style="background: #537171;color: white;">
                                            <tr>
                                                <th> TechniciansName</th>
                                                <th> PhoneNumber </th>
                                                <th> Email </th>
                                                <th> Country  </th>
                                                <th> StreetAddress </th>
                                                <th> Status </th>
                                                <th> Action </th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($dataList))
                                                @foreach($dataList as $data)
                                            <tr>

                                                <td>{{$data->users_name}}</td>
                                                <td>{{$data->users_phone_number}}</td>
                                                <td> {{$data->email}}</td>
                                                <td><span class="label label-success" style="color: #fff;font-size: 10px">{{$data->users_country}}</span></td>
                                                <td><span class="label label-success" style="color: #fff;font-size: 10px">

                                                        {{$data->	users_present_address	}}
                                                    </span></td>
                                                <td>

                                                    @if($data->users_veryfication_status === 'Active')
                                                        <span class="label label-success" style="color: #fff;font-size: 10px"> Active</span>
                                                    @else
                                                        <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Inactive</span>
                                                    @endif

                                                </td>
                                                <td>
                                                    @if($data->users_veryfication_status === 'Active')
                                                        <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#inActive{{ $data->users_track_id }}">
                                                            <span style="color: #C4302E"><i class="fa fa-ban"></i></span> Inactive
                                                        </a>
                                                    @elseif($data->users_veryfication_status === 'Inactive')
                                                        <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#active{{ $data->users_track_id }}">
                                                            <span style="color: #489D48"><i class="fa fa-check"></i></span>  Active
                                                        </a>
                                                    @endif

                                                    <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#delete{{ $data->users_track_id }}">
                                                        <span style="color: #C53431"><i class="fa fa-remove"></i></span>  Remove
                                                    </a>

                                                        <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="{{url('/portal/qmDashboard/listTc/details/'. $data->users_track_id)}}"  style="color: red;width: 30px;">
                                                            <span style="color: #C53431"><i class="fa fa-list"></i> </span>Details
                                                        </a>&nbsp;

                                                    <!--  Model For Delete link item-->
                                                    <div id="delete{{ $data->users_track_id }}" class="modal fade" role="dialog">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <form method="POST" action="{{ URL::to('/portal/qmDashboard/listTc/remove') }}">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="tec_trackid" id="house_item_track_id" value="{{ $data->users_track_id }}"/>
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h5 class="modal-title" style="text-align: center;">Are you want to remove this link item?</h5>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-danger center-block"><i class="fa fa-trash"></i>&nbsp;Remove</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div> <!-- End delete Model -->


                                                    <!--  Model For Inactive house item-->
                                                    <div id="inActive{{ $data->users_track_id }}" class="modal fade" role="dialog">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <form method="POST" action="{{ URL::to('/portal/qmDashboard/listTc/inactive') }}">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="tec_trackid" id="house_item_track_id" value="{{ $data->users_track_id }}"/>
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h5 class="modal-title" style="text-align: center;">Are you want to inactive this link item?</h5>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-danger center-block"><i class="fa fa-trash"></i>&nbsp;Inactive</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div> <!-- End Inactive Model -->
                                                    <!-- Modal For Active house item  -->
                                                    <div id="active{{ $data->users_track_id }}" class="modal fade" role="dialog">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <form method="POST" action="{{ URL::to('/portal/qmDashboard/listTc/active') }}">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="tec_trackid" id="house_item_track_id" value="{{ $data->users_track_id }}"/>
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h5 class="modal-title" style="text-align: center;">Are you want to active this link item?</h5>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-success center-block"><i class="fa fa-check"></i>&nbsp;Active</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div> <!--- End Active Modal -->


                                                </td>



                                            </tr>

                                            @endforeach
                                            @endif
                                            </tbody>
                                            <tfoot>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <script>
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#houseList').DataTable({
                "aaSorting": []
            });
        });
    </script>
@endsection