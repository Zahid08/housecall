<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <!--Dashboard Section-->
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"><i class="fa fa-plus"></i> <SPAN>Add New Technicians</SPAN></div>
                            <fieldset style="border: 1px solid #537171 !important;border-radius: 0px;">
                                <div class="panel-body" id="testApp">
                                    <div class="panel-body">
                                        <form id="loginForm" method="POST" action="{{url('/portal/qmDashboard/addTecnicians/new')}}" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <div class="col-sm-12">
                                            <div class="form-group col-sm-6">
                                                <label for="users_name">Name :<span class="mark">*</span> </label>
                                                <input type="text" class="form-control requiredOL" name="tec_name" id="users_name" value=""  />
                                                <small class="text-danger">{{ $errors->first('tec_name') }}</small>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="users_email">Email :<span class="mark">*</span></label>
                                                <input type="text" class="form-control requiredOL" name="email" id="users_email" value=""  />
                                                <small class="text-danger">{{ $errors->first('email') }}</small>
                                            </div>
                                            </div>
<div class="col-sm-12">
                                            <div class="form-group col-sm-6">
                                                <label for="users_phone_number">Phone Number :<span class="mark">*</span> </label>
                                                <input type="text" class="form-control requiredOL" name="tec_phone_number" id="users_phone_number" value="" />
                                                <small class="text-danger">{{ $errors->first('tec_phone_number') }}</small>
                                            </div>
    <div class="form-group col-sm-6">
        <label for="users_gender">Gender :<span class="mark">*</span> </label>
        <select class="form-control" id="users_gender" name="tec_gender">
            <option value="NULL">Select Gender------</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
        </select>

        <small class="text-danger">{{ $errors->first('tec_gender') }}</small>
    </div>
</div>

                                            <div class="col-sm-12">

                                            <div class="form-group col-sm-6">
                                                <label for="image">Image :<span class="mark">*</span></label>
                                                <input type="file" id="image" name="image" value="" class="form-control" />
                                                <small class="text-danger"></small>
                                            </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="sel1">City:</label>
                                                    <input type="text" class="form-control requiredOL" name="tec_country" id="users_nationalid" value="" />

                                                    <small class="text-danger">{{ $errors->first('tec_country') }}</small>
                                                    {{--</select>--}}
                                                </div>
                                            </div>

<div class="col-sm-12">
    <div class="form-group col-sm-6">
        <label for="users_permanent_address">Street  Address:<span class="mark">*</span></label>
        <textarea class="form-control" rows="3" placeholder="Enter permanent address" name="tec_street_address"></textarea>
        <small class="text-danger">{{ $errors->first('tec_street_address') }}</small>
    </div>

                                            <div class="form-group col-sm-6">
                                                <label for="users_nationalid">Password:<span class="mark">*</span></label>
                                                <input type="password" class="form-control requiredOL" name="password" id="users_nationalid" value="" />
                                                <small class="text-danger">{{ $errors->first('password') }}</small>
                                            </div>
</div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-6" id="box">
                                                    <label for="users_nationalid">State:</label>
                                                    <a href="#" id="add" style="float: right;" >Add More State</a>

                                                    {{--<input name="name[]" type="text" id="name" class="name form-control" placeholder="State 1">--}}

                                                    <select class="form-control"  name="name[]" id="name"  >
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District Of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>




                                                </div>

                                                <div class="form-group col-sm-4" style="margin-top: 20px;">
                                                <button class="btn btn-default btn-block " style="background-color: #537171;border-color: #537171;color: white" type="submit" id="btnLogin" name="btnLogin"> Submit</button>
                                            </div>
                                            </div>

                                            <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
                                            <script>

                                                $(document).ready(function(){

                                                    $('#add').click(function(){

                                                        var inp = $('#box');

                                                        var i = $('input').size() + 1;

                                                        $('<div id="box' + i +'">' +
                                                            ' <select class="form-control" id="name" class="name form-control" style="margin-top: 20px;" name="name[]" placeholder="State \'+i+\'"  >\n' +
                                                            '                                                        <option value="AL">Alabama</option>\n' +
                                                            '                                                        <option value="AK">Alaska</option>\n' +
                                                            '                                                        <option value="AZ">Arizona</option>\n' +
                                                            '                                                        <option value="AR">Arkansas</option>\n' +
                                                            '                                                        <option value="CA">California</option>\n' +
                                                            '                                                        <option value="CO">Colorado</option>\n' +
                                                            '                                                        <option value="CT">Connecticut</option>\n' +
                                                            '                                                        <option value="DE">Delaware</option>\n' +
                                                            '                                                        <option value="DC">District Of Columbia</option>\n' +
                                                            '                                                        <option value="FL">Florida</option>\n' +
                                                            '                                                        <option value="GA">Georgia</option>\n' +
                                                            '                                                        <option value="HI">Hawaii</option>\n' +
                                                            '                                                        <option value="ID">Idaho</option>\n' +
                                                            '                                                        <option value="IL">Illinois</option>\n' +
                                                            '                                                        <option value="IN">Indiana</option>\n' +
                                                            '                                                        <option value="IA">Iowa</option>\n' +
                                                            '                                                        <option value="KS">Kansas</option>\n' +
                                                            '                                                        <option value="KY">Kentucky</option>\n' +
                                                            '                                                        <option value="LA">Louisiana</option>\n' +
                                                            '                                                        <option value="ME">Maine</option>\n' +
                                                            '                                                        <option value="MD">Maryland</option>\n' +
                                                            '                                                        <option value="MA">Massachusetts</option>\n' +
                                                            '                                                        <option value="MI">Michigan</option>\n' +
                                                            '                                                        <option value="MN">Minnesota</option>\n' +
                                                            '                                                        <option value="MS">Mississippi</option>\n' +
                                                            '                                                        <option value="MO">Missouri</option>\n' +
                                                            '                                                        <option value="MT">Montana</option>\n' +
                                                            '                                                        <option value="NE">Nebraska</option>\n' +
                                                            '                                                        <option value="NV">Nevada</option>\n' +
                                                            '                                                        <option value="NH">New Hampshire</option>\n' +
                                                            '                                                        <option value="NJ">New Jersey</option>\n' +
                                                            '                                                        <option value="NM">New Mexico</option>\n' +
                                                            '                                                        <option value="NY">New York</option>\n' +
                                                            '                                                        <option value="NC">North Carolina</option>\n' +
                                                            '                                                        <option value="ND">North Dakota</option>\n' +
                                                            '                                                        <option value="OH">Ohio</option>\n' +
                                                            '                                                        <option value="OK">Oklahoma</option>\n' +
                                                            '                                                        <option value="OR">Oregon</option>\n' +
                                                            '                                                        <option value="PA">Pennsylvania</option>\n' +
                                                            '                                                        <option value="RI">Rhode Island</option>\n' +
                                                            '                                                        <option value="SC">South Carolina</option>\n' +
                                                            '                                                        <option value="SD">South Dakota</option>\n' +
                                                            '                                                        <option value="TN">Tennessee</option>\n' +
                                                            '                                                        <option value="TX">Texas</option>\n' +
                                                            '                                                        <option value="UT">Utah</option>\n' +
                                                            '                                                        <option value="VT">Vermont</option>\n' +
                                                            '                                                        <option value="VA">Virginia</option>\n' +
                                                            '                                                        <option value="WA">Washington</option>\n' +
                                                            '                                                        <option value="WV">West Virginia</option>\n' +
                                                            '                                                        <option value="WI">Wisconsin</option>\n' +
                                                            '                                                        <option value="WY">Wyoming</option>\n' +
                                                            '                                                    </select>' +
                                                            '<i class="fa fa-window-close add"  style="cursor: pointer" id="remove"></i></div>').appendTo(inp);

                                                        i++;

                                                    });

                                                    $('body').on('click','#remove',function(){

                                                        $(this).parent('div').remove();

                                                    });


                                                });

                                            </script>


                                        </form>
                                    </div>

                                </div>
                            </fieldset>
                        </div>
                        <!--Dashboard Section end-->
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

