<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>
@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"><i class="fa fa-list"></i> <SPAN>Technicians List</SPAN>
                            <span style="float: right">
                                <a style="color: white;cursor:pointer;" onclick="openNav()"> Map</a>

<style>


    .sidenav {
        height: 100%;
        width: 0;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        background-color: #0288d1;
        overflow-x: hidden;
        transition: 0.5s;
        padding-top: 60px;
        text-align:center;
    }

    .sidenav a {
        padding: 8px 8px 8px 32px;
        text-decoration: none;
        font-size: 25px;
        color: #818181;
        display: block;
        transition: 0.3s;
        color: #fff;
        display: block;
        transition: 0.3s;
        padding: 30px;

    }

    .sidenav a:hover{
        color: #f1f1f1;
    }

    .closebtn {
        position: absolute;
        top: 0;
        right: 25px;
        font-size: 36px !important;
        margin-left: 50px;
    }

    @media screen and (max-height: 450px) {
        .sidenav {padding-top: 15px;}
        .sidenav a {font-size: 18px;}
    }
    
    #header{
        background: #4285F4;
    }
    #header span{
        line-height: 30px;
        color: #FFF;
        margin-left: 10px;
    }
    #map {
        height: 80%;
        width: 80%;
        margin: auto;
        margin-top:50px;
    }
    #data-panel {
        position: absolute;
        top: 120px;
        left: 25%;
        z-index: 5;
        background-color: #fff;
        padding: 3px;
        border: 1px solid #999;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        font-size: 10px;
    }




</style>
                                <div id="mySidenav" class="sidenav" style="margin-top: 75px;margin-left: 100px;">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()" style="margin-right: 100px;">×</a>

                                    <div id="header">
      <span>Google Maps</span>
    </div>
    <div id="data-panel">
      <input id="origin" type="textbox" value="" placeholder="origin" style="color: black;">
       <input id="destination" type="textbox" value="" placeholder="destination" style="color: black;">
      <input id="submit" type="button" value="Get Distance">
      <div id="data"></div>
    </div>
    <div id="map"></div>
    <script>
      function initMap() {
          var bounds = new google.maps.LatLngBounds;
          markersArray = [];
          var destinationIcon = 'https://chart.googleapis.com/chart?' + 'chst=d_map_pin_letter&chld=D|FF0000|000000';
          var originIcon = 'https://chart.googleapis.com/chart?' +'chst=d_map_pin_letter&chld=O|FFFF00|000000';
          var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 5,
              center: {lat: 36.7783, lng: 119.4179},
          });
          var geocoder = new google.maps.Geocoder;
          document.getElementById('submit').addEventListener('click', function() {
              document.getElementById('data').innerHTML = '';
              var origin = document.getElementById('origin').value;
              var destination = document.getElementById('destination').value;
              var service = new google.maps.DistanceMatrixService();
              var showGeocodedAddressOnMap = function(asDestination) {
                  var icon = asDestination ? destinationIcon : originIcon;
                  return function(results, status) {
                      if (status === google.maps.GeocoderStatus.OK) {
                          map.fitBounds(bounds.extend(results[0].geometry.location));
                          markersArray.push(new google.maps.Marker({
                              map: map,
                              position: results[0].geometry.location,
                              icon: icon
                          }));
                      } else {
                          alert('Geocode was not successful due to: ' + status);
                      }
                  };
              };
              service.getDistanceMatrix({
                  origins: [origin],
                  destinations: [destination],
                  travelMode: google.maps.TravelMode.DRIVING,
                  avoidHighways: false,
                  avoidTolls: false,
              }, function(response, status){
                  console.log(response);
                  deleteMarkers(markersArray);
                  if (status !== google.maps.DistanceMatrixStatus.OK) {
                      alert('Error was: ' + status);
                  } else {
                      var originList = response.originAddresses;
                      var destinationList = response.destinationAddresses;
                      var outputDiv = document.getElementById('data');

                      for (var i = 0; i < originList.length; i++) {
                          var results = response.rows[i].elements;
                          geocoder.geocode({'address': originList[i]},showGeocodedAddressOnMap(false));
                          for (var j = 0; j < results.length; j++) {
                              geocoder.geocode({'address': destinationList[i]},showGeocodedAddressOnMap(true));
                              outputDiv.innerHTML += "<br><div id='data' style='color: black;'><b>Results :</b> " + originList[i] + ' to ' + destinationList[j] +
                                  ': ' + results[j].distance.text + ' in ' + results[j].duration.text + '<br></div>';
                          }
                      }
                  }
              });
          });
      }
      function deleteMarkers(markersArray) {
          for (var i = 0; i < markersArray.length; i++) {
              markersArray[i].setMap(null);
          }
          markersArray = [];
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?callback=initMap" async defer></script>



</div>

                                <script>
                                    function openNav() {
                                        document.getElementById("mySidenav").style.width = "100%";
                                    }

                                    function closeNav() {
                                        document.getElementById("mySidenav").style.width = "0";
                                    }
                                </script>

                            </span>
                            </div>
                            <fieldset style="border: 1px solid #435452 !important;border-radius: 0px;">
                                <div class="panel-body" style="padding: 0px;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- Alert Section-->
                                            @if (session('errorArray'))
                                                <div class="alert alert-danger">
                                                    @foreach($errors->all() AS $key => $value)
                                                        <strong><i class="fa fa-warning"></i> {{ $value }}</strong><br>
                                                    @endforeach
                                                </div>
                                            @endif
                                            @if (session('error'))
                                                <div class="alert alert-danger"  id="error">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('error') }}</strong>
                                                </div>
                                            @endif
                                            @if (session('success'))
                                                <div class="alert alert-success"  id="success">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('success') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="box-body table-responsive no-padding">
                                        <table id="houseList" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                            <thead style="background: #537171;color: white;">
                                            <tr>
                                                <th> TechniciansName</th>
                                                <th> PhoneNumber </th>
                                                <th> Email </th>
                                                <th> State </th>
                                                <th> Country  </th>
                                                <th> StreetAddress </th>
                                                <th> Status </th>
                                                <th> Action </th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($dataList))
                                                @foreach($dataList as $data)
                                            <tr>


                                                        <td>{{$data->tec_name}}</td>
                                                        <td>{{$data->tec_phone_number}}</td>
                                                        <td>{{$data->email}}</td>
                                                        <td> {{$data->state}}</td>
                                                        <td><span class="label label-success" style="color: #fff;font-size: 10px">{{$data->tec_country}}</span></td>
                                                        <td><span class="label" style="color: #fff;font-size: 10px"><a href="" target="_blank">{{$data->tec_street_address}}</a></span></td>
                                                        <td>

                                                            @if($data->tec_status === 'Active')
                                                                <span class="label label-success" style="color: #fff;font-size: 10px"> Active</span>
                                                            @else
                                                                <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Inactive</span>
                                                            @endif

                                                        </td>
                                                        <td> <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#inActive{{$data->tec_id}}" style="color: red">
                                                                <span style="color: #C53431"><i class="fa fa-ban"></i> </span>Assign
                                                            </a>

                                                            <!--  Model For Inactive Any user Admin-->
                                                            <div id="inActive{{ $data->tec_id}}" class="modal fade" role="dialog">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <form method="POST" action="{{ URL::to('portal/qmDashboard/customerinfo/assign') }}">
                                                                            {{ csrf_field() }}
                                                                            <input type="hidden" name="tec_trackid" id="users_track_id" value="{{ $data->tec_trackid }}"/>
                                                                            <input type="hidden" name="lead_trackid" id="users_track_id" value="{{ $leadtrakid }}"/>
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                <h5 class="modal-title" style="text-align: center;">Do you want to assign this technicians ?</h5>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-success center-block"><i class="fa fa-save"></i>&nbsp;Yes</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div> <!-- End Inactive Model -->

                                                        </td>

                                            </tr>

                                                @endforeach
                                            @endif

                                            </tbody>
                                            <tfoot>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <script>
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#houseList').DataTable({
                "aaSorting": []
            });
        });
    </script>
@endsection
