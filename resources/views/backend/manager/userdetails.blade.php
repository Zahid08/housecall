<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>
@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"> Detail of All Users</div>
                            <div class="panel-body">
                                <div class="panel panel-primary">
                                    <div class="panel-body">
                                        <div class="col-md-6">
                                            <h5> User Status - <span class="label label-default"> {{$datalist->	users_veryfication_status}}  </span></h5>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>Lead Created  - <span class="label label-default"> {{ $datalist->created_at->format('d-m-Y') }}</span></h5>
                                        </div>
                                        <div class="clearfix"></div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" id="" style="background-color:#537171; color: white;">User Details Information</div>
                                                <div class="panel-body">
                                                    <div class="col-md-12">
                                                        <table class="table table-responsive table-striped table-bordered">
                                                            <tr>
                                                                <th> User Name:  </th>
                                                                <th> {{$datalist->users_name}} </th>
                                                            </tr>
                                                            <tr>
                                                                <td>Email :</td>
                                                                <td>
                                                                    {{$datalist->email}}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th> Phone Number : </th>
                                                                <th> {{$datalist->users_phone_number}}</th>
                                                            </tr>
                                                            <tr>
                                                                <td>Country : </td>
                                                                <td>{{$datalist->users_country}}</td>
                                                            </tr>
                                                            <tr>
                                                                <th> Present Address :</th>
                                                                <th> {{$datalist->	users_present_address}}  </th>
                                                            </tr>
                                                            <tr>
                                                                <td> Permanent Address :</td>
                                                                <td> {{$datalist->users_permanent_address}}   </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Completeness profile :</td>
                                                                <td>{{$datalist->users_profile_completeness	}} </td>
                                                            </tr>   <tr>
                                                                <th> Image : </th>
                                                                <th>
                                                                    @if(empty($datalist->image ))
                                                                        <img src="{{ asset('backend/images/user.jpg' ) }}" alt="User Image"  class="user-image"  width="150" height="150">
                                                                    @else
                                                                        <img src="{{ asset('upload/users/'.$datalist->image ) }}" alt="User Image"  class="user-image"  width="150" height="150">
                                                                    @endif

                                                                </th>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@stop