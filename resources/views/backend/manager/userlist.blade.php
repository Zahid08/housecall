<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>
@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"><i class="fa fa-list"></i> <SPAN>All user List</SPAN></div>
                            <fieldset style="border: 1px solid #435452 !important;border-radius: 0px;">
                                <div class="panel-body" style="padding: 0px;">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <!-- Alert Section-->
                                            @if (session('errorArray'))
                                                <div class="alert alert-danger">
                                                    @foreach($errors->all() AS $key => $value)
                                                        <strong><i class="fa fa-warning"></i> {{ $value }}</strong><br>
                                                    @endforeach
                                                </div>
                                            @endif
                                            @if (session('error'))
                                                <div class="alert alert-danger"  id="error">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('error') }}</strong>
                                                </div>
                                            @endif
                                            @if (session('success'))
                                                <div class="alert alert-success"  id="success">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong style="text-align: center;">{{ session('success') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="box-body table-responsive no-padding">
                                        <table id="houseList" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                            <thead style="background: #537171;color: white;">
                                            <tr>
                                                <th>  Name </th>
                                                <th>  E-mail </th>
                                                <th>  Designation </th>
                                                {{--<th>  Present Address </th>--}}
                                                {{--<th>  Permanent Address </th>--}}
                                                <th>  Status  </th>
                                                <th> Profile Completeness  </th>
                                                <th> Designation Change  </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($dataList))
                                                @foreach($dataList as $data)
                                                    @if($data->users_type !='Center')
                                                    <tr>
                                                        <td>{{ $data->users_name }}</td>
                                                        <td>{{ $data->email }}</td>
                                                        <td>
                                                            @if($data->users_type==='Qm')
                                                            <span class="label label-success" style="color: #fff;font-size: 10px">Quality Manager</span>
                                                            @elseif($data->users_type==='Qt')
                                                                <span class="label label-default" style="color: #fff;font-size: 10px">Quality Team</span>
                                                            @elseif($data->users_type==='Tc')
                                                                <span class="label label-info" style="color: #fff;font-size: 10px">Technicians</span>
                                                            @elseif($data->users_type==='HR')
                                                                <span class="label label-primary" style="color: #fff;font-size: 10px">HR</span>
                                                            @elseif($data->users_type==='Admin')
                                                                <span class="label label-success" style="color: #fff;font-size: 10px">Admin</span>
                                                            @elseif($data->users_type==='Center')
                                                                <span class="label label-success" style="color: #fff;font-size: 10px">Center</span>
                                                            @elseif($data->users_type==='Super Admin')
                                                                <span class="label label-warning" style="color: #fff;font-size: 10px">Super Admin</span>
                                                            @endif
                                                        </td>
                                                        {{--<td>{{ $data->users_permanent_address }}</td>--}}
                                                        <td>
                                                            @if($data->users_veryfication_status === 'Active')
                                                                <span class="label label-success" style="color: #fff;font-size: 10px"> Active</span>
                                                            @else
                                                                <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Inactive</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if($data->users_profile_completeness === 'Yes')
                                                                <span class="label label-success" style="color: #fff;font-size: 10px"> Complete Profile</span>
                                                            @else
                                                                <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Incomplete Profile</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if($data->users_type==='Super Admin')
                                                                <span class="label label-info" style="color: #fff;font-size: 10px">Super Admin</span>
                                                                @else

                                                            @if($data->users_veryfication_status === 'Active')
                                                                <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#inActive{{ $data->users_track_id }}" style="color: red">
                                                                    <span style="color: #C53431"><i class="fa fa-ban"></i> </span>Inactive
                                                                </a>&nbsp;
                                                            @elseif($data->users_veryfication_status === 'Inactive')
                                                                <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#active{{ $data->users_track_id }}">
                                                                    <span style="color: #489D48"><i class="fa fa-check"></i></span>  Active
                                                                </a>
                                                            @endif

                                                            @if($data->users_type !=='Tc')
                                                            <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#cngdesignation{{ $data->users_track_id }}" style="color: red">
                                                                <span style="color: #C53431"><i class="fa fa-ban"></i> </span>Change Designation
                                                            </a>&nbsp;
                                                            @endif
                                                                <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="{{url('/portal/userlist/details/'. $data->users_track_id)}}"  style="color: red;width: 30px;">
                                                                    <span style="color: #C53431"><i class="fa fa-list"></i> </span>Details
                                                                </a>&nbsp;
@endif
                                                            <!--  Model For Inactive Any user Admin-->
                                                            <div id="inActive{{ $data->users_track_id }}" class="modal fade" role="dialog">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <form method="POST" action="{{ URL::to('portal/manager/inActive') }}">
                                                                            {{ csrf_field() }}
                                                                            <input type="hidden" name="users_track_id" id="users_track_id" value="{{ $data->users_track_id }}"/>
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                <h5 class="modal-title" style="text-align: center;">Do you want to make this manager inactive ?</h5>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-danger center-block"><i class="fa fa-trash"></i>&nbsp;Inactive</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div> <!-- End Inactive Model -->


                                                                <div id="active{{ $data->users_track_id }}" class="modal fade" role="dialog">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <form method="POST" action="{{ URL::to('portal/manager/Active') }}">
                                                                                {{ csrf_field() }}
                                                                                <input type="hidden" name="users_track_id" id="users_track_id" value="{{ $data->users_track_id }}"/>
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                    <h5 class="modal-title" style="text-align: center;">Do you want to make this manager Active ?</h5>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-success center-block"><i class="fa fa-save"></i>&nbsp;Active</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>

                                                                </div> <!--- End Active Modal -->


                                                            <!--  Model For Inactive Any user Admin-->
                                                            <div id="cngdesignation{{ $data->users_track_id }}" class="modal fade" role="dialog">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <form method="POST" action="{{ URL::to('/portal/managerlist/changedesignation') }}">
                                                                            {{ csrf_field() }}
                                                                            <input type="hidden" name="users_track_id" id="users_track_id" value="{{ $data->users_track_id }}"/>
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                <h5 class="modal-title" > Change Designation</h5>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div class="col-sm-6">
                                                                                    <select class="form-control" id="users_gender" name="designation">
                                                                                        <option value="NULL">Select designation------</option>
                                                                                        <option value="Admin">Admin</option>
                                                                                        <option value="HR">HR</option>
                                                                                        <option value="Qt">Quality Team</option>
                                                                                        <option value="Qm">Quality Manager</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-success center-block"><i class="fa fa-save"></i>&nbsp;Confirm</button>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div> <!-- End Inactive Model -->

                                                            <!-- Modal For Active User Admin -->
                                                        </td>
                                                    </tr>
                                                    @endif
                                                @endforeach
                                            @endif
                                            </tbody>
                                            <tfoot>
                                            <tr>

                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <script>
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#houseList').DataTable({
                "aaSorting": []
            });
        });
    </script>
@endsection