
<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>
@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12 " >
                    <div class="panel-group">
                        <!--Dashboard Section-->
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style">New Message</div>
                            <fieldset style="border: 1px solid #537171 !important;border-radius: 0px;">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            @if(\Illuminate\Support\Facades\Auth::user()->users_type=='Qm')
                                            <form method="POST" action="{{ url('/portal/message/compose/store') }}" enctype="multipart/form-data" id="applicationMedicalInf">
                                                @elseif(\Illuminate\Support\Facades\Auth::user()->users_type=='HR')
                                                    <form method="POST" action="{{ url('/hrportal/message/compose/store') }}" enctype="multipart/form-data" id="applicationMedicalInf">
                                                @endif
                                                {{ csrf_field() }}
                                                        <div class="form-group{{ $errors->has('conversation_sent_to') ? ' has-error' : '' }}">
                                                            <label for="conversation_sent_to">Technicians  email address&nbsp;<span id="mark">*</span></label>
                                                            <span id="streetaddresshdiv">
                                                    <select class="form-control requiredOL" id="conversation_sent_to" name="conversation_sent_to">
                                                    <option value="">Select email address </option>
                                                        @foreach($dataList as $data)
                                                            <option value="{{$data->tec_trackid}}"> {{$data->email}}</option>
                                                        @endforeach
                                                    </select>
                                                    <small class="text-danger">{{ $errors->first('conversation_sent_to') }}</small>
                                                    </span>
                                                        </div>


                                                <div class="form-group{{ $errors->has('conversation_subject') ? ' has-error' : '' }}">
                                                    <label for="conversation_subject">Subject&nbsp;<span id="mark">*</span></label>
                                                    <input type="text" class="form-control requiredOL" id="conversation_subject" name="conversation_subject" value="{{ old('conversation_subject') }}">
                                                    <small class="text-danger">{{ $errors->first('conversation_subject') }}</small>
                                                </div>

                                                <div class="form-group{{ $errors->has('conversation_body') ? ' has-error' : '' }}">
                                                    <label for="conversation_body">Message Body&nbsp;<span id="mark">*</span></label>
                                                    <textarea name="conversation_body" id="conversation_body" class="form-control ckeditor_standard requiredOL">{{ old('conversation_body') }}</textarea>
                                                    <small class="text-danger">{{ $errors->first('conversation_body') }}</small>
                                                </div>
                                                <div class="form-group{{ $errors->has('ca_name') ? ' has-error' : '' }}">
                                                    <label for="ca_name">Attachment&nbsp;</label>
                                                    <input type="file" class="form-control ca_name requiredOL" id="ca_name" name="ca_name">
                                                    <p class="help-block oError" style="color: red;margin-bottom: 0"></p>
                                                    <small class="text-danger">{{ $errors->first('ca_name') }}</small>
                                                </div>
                                                <button type="submit" class="btn btn-primary pull-right" style="margin-top: 15px"><i class="fa fa-check"></i>&nbsp;Send Message</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script type="text/javascript">
        function getstreetaddress(id) {
            var tec_trackid = id;
            if (tec_trackid !== '') {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: "/getstreetaddress",
                    dataType: 'json',
                    data: {
                        tec_trackid: tec_trackid
                    },
                    success: function (response) {
                        var obj = response;
                        if (obj.output === "success") {
                            var html = '<select  class="form-control requiredOL" id="conversation_sent_to" name="conversation_sent_to"><option value=""> Select street address </option>';
                            if (obj.streetaddress == "") {
                                html += '<option value=""> street address are not available in this country </option>';
                            } else {
                                $.each(obj.streetaddress, function (key, Event) {
                                    html += '<option value="' + Event.tec_trackid + '">' + Event.tec_street_address + '</option>';
                                });
                            }
                            html += '</select>';
                            $("#streetaddresshdiv").html(html);
                            $(this).parent("div").addClass("has-error");
                            $(this).siblings(".text-danger").removeClass("hide");
                        } else {
                            alert(obj.msg);
                        }
                    }
                });
            }
        }

    </script>

@endsection