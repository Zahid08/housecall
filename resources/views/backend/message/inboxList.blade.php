<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style">Inbox Message List</div>
                            <fieldset style="border: 1px solid #537171 !important;border-radius: 0px;">
                                <div class="panel-body" style="padding: 0px;">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <!-- Alert Section-->
                                            @if (session('errorArray'))
                                                <div class="alert alert-danger">
                                                    @foreach($errors->all() AS $key => $value)
                                                        <strong><i class="fa fa-warning"></i> {{ $value }}</strong><br>
                                                    @endforeach
                                                </div>
                                            @endif
                                            @if (session('error'))
                                                <div class="alert alert-danger"  id="error">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('error') }}</strong>
                                                </div>
                                            @endif
                                            @if (session('success'))
                                                <div class="alert alert-success"  id="success">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('success') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="box-body table-responsive no-padding">
                                        <table id="inboxList"  class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                            <thead style="background: #537171;color: white;">
                                            <tr>
                                                <th> Message Received </th>
                                                <th> Country Name </th>
                                                <th> Street Address </th>
                                                <th> Form </th>
                                                <th> Subject </th>
                                                <th> Message Action </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($dataList as $data)
                                                <tr>
                                                    <td>
                                                        <?php
                                                        $createdList = $data->created_at->diffInWeeks(\Carbon\Carbon::now()) >= 1 ?
                                                            $data->created_at->format('j M Y, g:ia') : $data->created_at->diffForHumans();
                                                        ?>
                                                        {{ $createdList }}

                                                    </td>
                                                    <td>{{ $data->tec_country }}</td>
                                                    <td>{{ $data->tec_street_address }} </td>
                                                    <td>{{ $data->tec_name }} </td>
                                                    <td>{{$data->conversation_subject}}</td>
                                                    <td>
                                                        <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;"  href="{{ URL::to('portal/message/reply/'.$data->conversation_track_id) }}"><span style="color: #469B46"><i class="fa fa-reply"></i></span> Reply </a>&nbsp;

                                                        <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;"  href="{{ URL::to('/portal/message/single/'.$data->conversation_track_id ) }}"> <span style="color: #3F9741"><i class="fa fa-eye"></i></span>&nbsp;View </a>
                                                        <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#deleteMessage{{ $data->conversation_track_id }}">
                                                            <span style="color: #BC3B28"><i class="fa fa-trash-o"></i></span>&nbsp;Delete
                                                        </a>
                                                        <!-- Message delete modal -->
                                                        <div id="deleteMessage{{ $data->conversation_track_id }}" class="modal fade" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <form method="POST" action="{{ URL::to('portal/message/delete/inbox') }}">
                                                                        {{ csrf_field() }}
                                                                        <input type="hidden" name="conversation_track_id" id="conversation_track_id" value="{{ $data->conversation_track_id }}"/>
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                            <h5 class="modal-title" style="text-align: center">Do you want to delete this message?</h5>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                                                                            <button type="submit"  class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;Yes</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Message delete modal end -->
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td class="text-center" colspan="5"></td>
                                                <td class="text-left" colspan="1">
                                                    <a href="{{ URL::to('portal/message/compose/add') }}">
                                                        <button class="btn btn-primary btn-sm" style="background-color: #537171;"><i class="fa fa-plus"></i> Send Message</button>
                                                    </a>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    {{--<script type="text/javascript">--}}
        {{--$("#inboxActive").addClass("active");--}}
        {{--$("#inboxActive").parent().parent().addClass("treeview active");--}}
        {{--$("#inboxActive").parent().addClass("in");--}}
    {{--</script>--}}
    {{--<script>--}}
        {{--$(document).ready(function () {--}}
            {{--$('#inboxList').DataTable({--}}
                {{--"aaSorting": []--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <script>
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#houseList').DataTable({
                "aaSorting": []
            });
        });
    </script>


@stop


