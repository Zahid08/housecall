<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@extends('layout.backend.master')
@section('content')
    <style>
        textarea {
            resize: vertical;
        }
        .has-error .cke_chrome{
            border-color: #dd4b39 !important;
        }
    </style>
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <!--Add new union section-->
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style">Reply Message</div>
                            <fieldset style="border: 1px solid #537171 !important;border-radius: 0px;">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <form method="POST" action="{{ url('/portal/message/reply/store') }}" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="conversation_sent_from" id="conversation_sent_from" value="{{ $dataList->conversation_sent_from }}">
                                                <input type="hidden" name="conversation_sent_to" id="conversation_sent_to" value="{{ $dataList->conversation_sent_to }}">
                                                <div class="form-group{{ $errors->has('conversation_subject') ? ' has-error' : '' }}">
                                                    <label for="conversation_subject">Subject&nbsp;<span id="mark">*</span></label>
                                                    <input type="text" class="form-control requiredOL" id="conversation_subject" name="conversation_subject" value="RE: {{ $dataList->conversation_subject }}">
                                                    <small class="text-danger">{{ $errors->first('conversation_subject') }}</small>
                                                </div>
                                                <div class="form-group{{ $errors->has('conversation_body') ? ' has-error' : '' }}">
                                                    <label for="conversation_body">Message Body&nbsp;<span id="mark">*</span></label>
                                                    <textarea name="conversation_body" id="conversation_body" class="form-control ckeditor_standard requiredOL">{{ old('conversation_body') }}</textarea>
                                                    <small class="text-danger">{{ $errors->first('conversation_body') }}</small>
                                                </div>
                                                <div class="form-group{{ $errors->has('ca_name') ? ' has-error' : '' }}">
                                                    <label for="ca_name">Attachment&nbsp;</label>
                                                    <input type="file" class="form-control ca_name requiredOL" id="ca_name" name="ca_name">
                                                    <p class="help-block oError" style="color: red;margin-bottom: 0"></p>
                                                    <small class="text-danger">{{ $errors->first('ca_name') }}</small>
                                                </div>
                                                <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-check"></i>&nbsp;Reply Message</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <!--Add new union section end-->
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script type="text/javascript">
        $("#composeActive").addClass("active");
        $("#composeActive").parent().parent().addClass("treeview active");
        $("#composeActive").parent().addClass("in");
    </script>
    <script>
        /*
         * keyup global
         */
        $(document).on("keyup change focusout", ".form-control.requiredOL", function () {
            if ($(this).val() == '') {
                $(this).addClass("errorInput");
                $(this).parent("div").addClass("has-error");
                $(this).siblings(".text-danger").removeClass("hide");
                $(this).siblings(".oError").removeClass("hide");
            } else {
                $(this).removeClass("errorInput");
                $(this).parents("div").removeClass("has-error");
                $(this).siblings(".text-danger").addClass("hide");
                $(this).siblings(".oError").addClass("hide");
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            var maxSize = '2048';
            var _validFileExtensions = [];
            var imgExtensions = [".jpg", ".jpeg", ".JPG", ".JPEG", ".png", "PNG"];
            var fileExtensions = [".jpg", ".jpeg", ".JPG", ".JPEG", ".png", "PNG", ".doc", ".docx", ".DOC", ".DOCX", ".pdf", ".PDF"];

            function validateSingleInput(oInput) {
                if (oInput.type == "file") {
                    var sFileName = oInput.value;
                    if (sFileName.length > 0) {
                        if (oInput.className.match(/\bonlyImg\b/)) {
                            _validFileExtensions = imgExtensions;
                        } else {
                            _validFileExtensions = fileExtensions;
                        }
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }
                        if (!blnValid) {
                            oInput.value = "";
                            return false;
                        }
                    }
                }
                return true;
            }
            function fileSizeValidate(fdata) {
                if (fdata.files && fdata.files[0]) {
                    var fsize = fdata.files[0].size / 1024;
                    if (fsize > maxSize) {
                        fdata.value = "";
                        return false;
                    } else {
                        return true;
                    }
                }
            }
//                For Doctor Prescription attachement

            $(document).on("change", ".ca_name", function () {
                var noExtentionError = validateSingleInput(this);
                var noSizeError = fileSizeValidate(this);
                if (noExtentionError === false) {
                    $(this).siblings(".oError").show().text("Message Attachment file must be JPG, JPEG, PNG, DOC, PDF");
                    return false;
                } else {
                    $(this).siblings(".oError").hide();
                    if (noSizeError === false) {
                        $(this).siblings(".oError").show().text("Message Attachment file less then 2 MB");
                        return false;
                    } else {
                        $(this).siblings(".oError").hide();
                    }
                }
            });
        });
    </script>


    <script src="{{ asset('backend/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('backend/js/ckeditor/adapters/jquery.js') }}"></script>
    <script src="{{ asset('backend/js/editors.js') }}"></script>
    <!--All Script/Js File end-->
@stop