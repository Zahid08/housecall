<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>
        <!DOCTYPE html>
<html lang="en">
<head>
    <title>HouseCall </title>
    @include('layout.frontend.scriptLink')
</head>

<body style="margin-top: 130px;background-color: #537171;">
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading" id="panelHeading"  style="	font-family:Pompiere;"> Email Address </div>
                <div class="panel-body">
                    <form id="loginForm" method="POST" action="{{url('portal/profile/forgotPassword/link')}}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <input type="email" class="form-control requiredOL" name="email" id="users_email" value="" placeholder="Your Email"/>
                        </div>
                        <button class="btn btn-default" style="background-color: #537171;border-color: #537171;float:right;color: white" type="submit" id="btnLogin" name="btnLogin"> Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<script>
    /*
     * keyup global
     */
    $(document).on("keyup change focusout", ".form-control.requiredOL", function () {
        if ($(this).val() == '') {
            $(this).addClass("errorInput");
            $(this).parent("div").addClass("has-error");
            $(this).siblings(".text-danger").removeClass("hide");
            $(this).siblings(".oError").removeClass("hide");
        } else {
            $(this).removeClass("errorInput");
            $(this).parents("div").removeClass("has-error");
            $(this).siblings(".text-danger").addClass("hide");
            $(this).siblings(".oError").addClass("hide");
        }
    });
</script>


</body>
</html>