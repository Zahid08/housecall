<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>
@extends('layout.backend.master')
@section('content')

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <!--Dashboard Section-->
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style" style="background-color:#537171;color: white;"><i class="fa fa-edit"></i> <SPAN>Edit Profile</SPAN></div>
                            <div class="panel-body" id="testApp">

                                @if (session('success'))
                                    <div class="alert alert-success" id="success-alert">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>{{ session('success') }}</strong>
                                    </div>
                                @endif


                                <div class="col-sm-8">
                                    <form method="POST" action="{{ url('portal/profile/update') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label for="users_name"> Name&nbsp;<span id="mark">*</span></label>
                                            <input type="text" id="name" name="users_name" value="{{ $dataList->users_name }}" class="form-control"  required/>
                                            <small class="text-danger">{{ $errors->first('users_name') }}</small>
                                        </div>
                                        <div class="form-group">
                                            <label for="users_phone">Phone Number&nbsp;<span id="mark">*</span></label>
                                            <input type="text" id="users_phone" name="users_phone_number" value="{{ $dataList->users_phone_number }}"  class="form-control"  required/>
                                            <small class="text-danger">{{ $errors->first('users_phone_number') }}</small>
                                        </div>
                                        <div class="form-group">
                                            <label for="users_email">Email&nbsp;<span id="mark">*</span></label>
                                            <input type="users_email" id="admins_email" name="email" value="{{ $dataList->email }}" class="form-control" required />
                                            <small class="text-danger">{{ $errors->first('email') }}</small>
                                        </div>

                                        <div class="form-group">
                                            <label for="users_profile_picture">Picture</label>
                                            <input type="file" id="users_profile_picture" name="image" value="" class="form-control"  />
                                            <small class="text-danger">{{ $errors->first('image') }}</small>
                                        </div>

                                        <button type="submit" return="false" name="btnSaveClass" class="btn  btn-lg btn-block" style="background-color:#537171;color: white;"><i class="fa fa-edit"></i> Update Profile </button>

                                    </form>

                                </div>

                                <div class="col-sm-3" style="margin-top: 50px;margin-left: 40px;">
                                        @if(empty(Auth::user()->image ))
                                            <img src="{{ asset('backend/images/user.jpg' ) }}" alt="User Image"  class="user-image"  width="150" height="150">
                                        @else
                                            <img src="{{ asset('upload/users/'. Auth::user()->image ) }}" alt="User Image"  class="user-image"  width="150" height="150">
                                        @endif
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <script>
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
        });
    </script>

@endsection