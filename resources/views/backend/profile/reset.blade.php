<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        Hello {{ $users_name }}<br><br>
        Please reset your account password using the following link<br><br>
        ---<br>
        <a href="{{ $link }}">Click this link for reset password</a><br>
    </body>
</html>
