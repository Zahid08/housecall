
<!DOCTYPE html>
<html lang="en">
    <head>
        <title> Housecall </title>
        @include('layout.frontend.scriptLink')
    </head>

    <body style="margin-top: 130px;background-color: #537171;">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-default">
                        <div class="panel-heading" id="panelHeading">Update Your Password</div>
                        <div class="panel-body">
                            <form id="loginForm" method="POST" action="{{ URL::to('/portal/profile/updatePassord') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="users_track_id" id="users_track_id" value="{{ $userId->users_track_id }}">

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password"> New Password<span class="mark">*</span> </label>
                                    <input type="password" class="form-control " name="password" id="users_phone" value="{{ old('password') }}" />
                                    <small class="text-danger">{{ $errors->first('password') }}</small>
                                </div>
                                <div class="form-group{{ $errors->has('re_password') ? ' has-error' : '' }}">
                                    <label for="password">Confirm New Password<span class="mark">*</span></label>
                                    <input type="password" class="form-control " name="re_password" id="re_password" value="{{ old('re_password') }}" />
                                    <small class="text-danger">{{ $errors->first('re_password') }}</small>
                                </div>
                                <button class="btn btn-default" style="background-color: #537171;border-color: #537171;color: white" type="submit" id="btnLogin" name="btnLogin"> Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <script>
            /*
             * keyup global
             */
            $(document).on("keyup change focusout", ".form-control.requiredOL", function () {
                if ($(this).val() == '') {
                    $(this).addClass("errorInput");
                    $(this).parent("div").addClass("has-error");
                    $(this).siblings(".text-danger").removeClass("hide");
                    $(this).siblings(".oError").removeClass("hide");
                } else {
                    $(this).removeClass("errorInput");
                    $(this).parents("div").removeClass("has-error");
                    $(this).siblings(".text-danger").addClass("hide");
                    $(this).siblings(".oError").addClass("hide");
                }
            });
        </script>
    </body>

</html>