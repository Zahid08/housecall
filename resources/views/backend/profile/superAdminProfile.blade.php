<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@extends('layout.backend.master')
@section('content')

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12 " >
                    <div class="panel-group">
                        <!--Dashboard Section-->
                        <div class="panel panel-primary">
                            <div class="panel-heading" id="" style="background-color:#537171; color: white;"> <i class="fa fa-user"></i> <SPAN>Profile</SPAN>
                            </div>

                            <div class="panel-body" id="testApp">

                                @if(Auth::user()->users_profile_completeness=="Yes")
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="well well-sm">
                                            <div class="row">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-5" style="border:1px solid #ccc; background: #EDF0F5;">
                                                    <div style="text-align: center; margin-top: 10px;">

                                                        @if(empty(Auth::user()->image ))
                                                            <img src="{{ asset('backend/images/user.jpg' ) }}" alt="User Image" width="150" height="150">

                                                        @else
                                                            <img src="{{ asset('upload/users/'. Auth::user()->image ) }}" alt="User Image" width="150" height="150">
                                                        @endif
                                                    </div>

                                                    <table class="table" style="background: #EDF0F5; margin-top:15px; color: #000;">
                                                        <tr>
                                                            <td>Name</td>
                                                            <td>:</td>
                                                            <td>{{ Auth::user()->users_name }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>E-mail</td>
                                                            <td>:</td>
                                                            <td>{{Auth::user()->email}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Phone</td>
                                                            <td>:</td>
                                                            <td>{{ Auth::user()->users_phone_number }}</td>
                                                        </tr>

                                                    </table>

                                                    <div class="text-right" style="margin-bottom:5px;">
                                                      @if(Auth::user()->users_type=="Super Admin")
                                                            <a href="{{ URL::to('/portal/profile/edit') }}" type="button" class="btn btn-default btn-sm pull-left" style="background-color:#537171;color: white;"><i class="fa fa-pencil-square-o"></i> Update Your Profile</a>
@endif
                                                        <a href="{{ URL::to('/portal/profile/passwordEdit') }}" type="button" class="btn btn-default btn-sm" style="background-color:#537171;color: white;"><i class="fa fa-lock" aria-hidden="true"></i> Change Your Password</a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    @endif


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection