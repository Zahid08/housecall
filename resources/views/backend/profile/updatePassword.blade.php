<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@extends('layout.backend.master')
@section('content')

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style" style="background-color:#537171;color: white;"> Change Your Password </div>
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <form method="POST" action="{{ url('portal/profile/passwordUpdate') }}" >
                                        {{ csrf_field() }}

                                        <div class="form-group">
                                            <label for="old_password">Old Password &nbsp;<span id="mark">*</span></label>
                                            <input type="password" id="old_password" value="{{ old('old_password') }}" name="old_password" class="form-control"  required/>
                                        </div>
                                        <div class="form-group">
                                            <label for="password">New Password &nbsp;<span id="mark">*</span></label>
                                            <input type="password" id="password" name="password" value="{{ old('password') }}" class="form-control"  required/>
                                        </div>

                                        <div class="form-group">
                                            <label for="re_password">Confirm New Password &nbsp;<span id="mark">*</span></label>
                                            <input type="password" id="re_password" name="re_password" value="{{ old('re_password') }}" class="form-control"  required />
                                        </div>

                                        <button type="submit" return="false" name="btnSaveClass" class="btn  btn-lg btn-block" style="background-color:#537171;color: white;"><i class="fa fa-edit"></i> Change Password </button>
                                    </form>

                                </div>

                                <div class="col-md-5" style="margin-top: 30px;">
                                    @if (session('errorArray'))
                                        <div class="alert alert-danger" id="error">
                                            @foreach($errors->all() AS $key => $value)
                                                <strong><i class="fa fa-warning"></i> {{ $value }}</strong><br>
                                            @endforeach
                                        </div>
                                    @endif
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>{{ session('success') }}</strong>
                                        </div>
                                    @endif
                                    @if (session('error'))
                                        <div class="alert alert-danger" id="error">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>{{ session('error') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


@endsection