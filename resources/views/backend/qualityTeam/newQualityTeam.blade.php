<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <!--Dashboard Section-->
                        <div class="panel panel-primary">
                                <div class="panel-heading panel-style"><i class="fa fa-plus"></i> <SPAN>Add New Quality Team</SPAN></div>
                            <fieldset style="border: 1px solid #537171 !important;border-radius: 0px;">
                                <div class="panel-body" id="testApp">
                                    <!--Profile Section  Section-->
                                        <div class="panel-body">
                                            <form id="loginForm" method="POST" action="{{url('/login')}}">
                                                {{csrf_field()}}
                                                <div class="form-group col-sm-6">
                                                    <label for="users_name">Name :<span class="mark">*</span> </label>
                                                    <input type="text" class="form-control requiredOL" name="users_name" id="users_name" value=""  />
                                                    <small class="text-danger"></small>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="users_email">Email :<span class="mark">*</span></label>
                                                    <input type="text" class="form-control requiredOL" name="email" id="users_email" value=""  />
                                                    <small class="text-danger"></small>
                                                </div>

                                                <div class="form-group col-sm-6">
                                                    <label for="users_phone_number">Phone Number :<span class="mark">*</span> </label>
                                                    <input type="text" class="form-control requiredOL" name="users_phone_number" id="users_phone_number" value="" />
                                                    <small class="text-danger"></small>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="users_nationalid">National ID:<span class="mark">*</span></label>
                                                    <input type="text" class="form-control requiredOL" name="users_nationalid" id="users_nationalid" value="" />
                                                    <small class="text-danger"></small>
                                                </div>

                                                <div class="form-group col-sm-6">
                                                    <label for="users_gender">Gender :<span class="mark">*</span> </label>
                                                    <select class="form-control" id="users_gender" name="users_gender">
                                                        <option value="NULL">Select Gender------</option>
                                                        <option value="Male">Male</option>
                                                        <option value="Female">Female</option>
                                                    </select>

                                                    <small class="text-danger"></small>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="image">Image :<span class="mark">*</span></label>
                                                    <input type="file" id="image" name="image" value="" class="form-control" />
                                                    <small class="text-danger"></small>
                                                </div>

                                                <div class="form-group col-sm-6">
                                                    <label for="users_birthdate">Date Of Birth :<span class="mark">*</span> </label>
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" class="form-control pull-right" id="datepicker" name="users_birthdate">
                                                    </div>
                                                    <small class="text-danger"></small>
                                                </div>

                                                <div class="form-group col-sm-6">
                                                    <label for="sel1">Select Country:</label>
                                                    <select class="form-control" id="sel1" name="users_country">
                                                        <option>Select Country------</option>
                                                        <option value="Bangladesh">Bangladesh</option>
                                                        <option value="Japan">Japan</option>
                                                        <option value="Chine">Chine</option>
                                                        <option value="India">India</option>
                                                    </select>
                                                </div>

                                                <div class="form-group col-sm-6">
                                                    <label for="users_present_address">Present Address :<span class="mark">*</span> </label>
                                                    <textarea class="form-control" rows="3" placeholder="Enter present address........." name="users_present_address"></textarea>
                                                    <small class="text-danger"></small>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="users_permanent_address">Permanent Address:<span class="mark">*</span></label>
                                                    <textarea class="form-control" rows="3" placeholder="Enter permanent address" name="users_permanent_address"></textarea>
                                                    <small class="text-danger"></small>
                                                </div>

                                                <div class="form-group col-sm-6">
                                                    <button class="btn btn-default btn-block " style="background-color: #537171;border-color: #537171;color: white" type="submit" id="btnLogin" name="btnLogin"> Submit</button>
                                                </div>
                                                <div class="form-group col-sm-6">

                                                </div>

                                            </form>
                                        </div>
                                        <!--End Profile Section -->
                                </div>
                            </fieldset>
                        </div>
                        <!--Dashboard Section end-->
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
