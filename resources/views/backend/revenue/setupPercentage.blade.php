<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <!--Dashboard Section-->
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"><i class="fa fa-plus"></i> <SPAN>Setup Percentage</SPAN></div>

                            <fieldset style="border: 1px solid #537171 !important;border-radius: 0px;">
                                <div class="panel-body" id="testApp">
                                    @if (session('success'))
                                        <div class="alert alert-success"  id="success">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>{{ session('success') }}</strong>
                                        </div>
                                @endif
                                <!--Profile Section  Section-->
                                    <div class="panel-body">
                                        <form id="loginForm" method="POST" action="{{url('/portal/dashboard/setupPercentage/newadd')}}">
                                            {{csrf_field()}}
                                            <div class="col-sm-12">
                                            <div class="form-group col-sm-4">
                                                <label for="sel1">Select Center Name:</label>
                                                <select class="category2 form-control" required="" name="center_name" >
                                                    <option value="">Select your center name</option>
                                                    @foreach($centername as $data)
                                                    <option value="{{$data->users_name}}">{{$data->users_name}}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                            <div class="form-group col-sm-4" >
                                                <label for="percentage_cn">Commision<span class="mark">*</span></label>
                                                <input type="text" class="form-control requiredOL" name="percentage_cn" id="percentage_cn" value=""  required />
                                            </div>
                                                <div class="form-group col-sm-4" style="margin-top: 20px;">
                                                    <button class="btn btn-default btn-block " style="float: right;background-color: #537171;border-color: #537171;color: white" type="submit" id="btnLogin" name="btnLogin"> Submit</button>
                                                </div>
                                            </div>
                                        </form>


                                        <form id="loginForm" method="POST" action="{{url('/portal/dashboard/setupPercentage/technicians')}}">
                                            {{csrf_field()}}
                                            <div class="col-sm-12">
                                                <div class="form-group col-sm-4">
                                                    <label for="technicinas_type">Technicians :<span class="mark">*</span> </label>
                                                    <select class="form-control" name="technicains_trackid">
                                                        <option value="">Select technicians  name</option>
                                                        @foreach($techniciansname as $data)
                                                        <option value="{{$data->users_track_id}}">{{$data->users_name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <small class="text-danger"></small>

                                                </div>
                                                <div class="form-group col-sm-4" >
                                                    <label for="percentage_tc">Percentage %:<span class="mark">*</span></label>
                                                    <input type="text" class="form-control requiredOL" name="percentage_tc" id="percentage_tc" value=""  />
                                                    <small class="text-danger">{{ $errors->first('percentage_tc') }}</small>
                                                </div>
                                                <div class="form-group col-sm-4" style="margin-top: 20px;">
                                                    <button class="btn btn-default btn-block " style="float: right;background-color: #537171;border-color: #537171;color: white" type="submit" id="btnLogin" name="btnLogin"> Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                        <div class="col-sm-6">
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                <tr>
                                                    <th>Serial</th>
                                                    <th>Center Name</th>
                                                    <th>Center Percentage</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php
                                                    $count = 1;
                                                @endphp
                                                @if(!empty($dataList))
                                                    @foreach($dataList as $data)

                                                        <tr class="odd gradeX">
                                                            <td>{{$count}}</td>
                                                            <td>{{$data->center_name}}</td>
                                                            <td>{{$data->percentage_cn}}</td>
                                                            <td>
                                                                @if($data->status === 'Active')
                                                                    <span class="label label-success" style="color: #fff;font-size: 10px"> Active</span>
                                                                @else
                                                                    <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Inactive</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if($data->status === 'Active')
                                                                    <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#inActive{{ $data->percentage_track_id }}">
                                                                        <span style="color: #C4302E"><i class="fa fa-ban"></i></span> Inactive
                                                                    </a>
                                                                @elseif($data->status === 'Inactive')
                                                                    <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#active{{ $data->percentage_track_id }}">
                                                                        <span style="color: #489D48"><i class="fa fa-check"></i></span>  Active
                                                                    </a>
                                                                @endif

                                                                <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#delete{{ $data->percentage_track_id }}">
                                                                    <span style="color: #489D48"><i class="fa fa-remove"></i></span>  Remove
                                                                </a>
                                                                {{--<a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="">--}}
                                                                    {{--<span style="color: #489D48"><i class="fa fa-edit"></i></span>  Edit--}}
                                                                {{--</a>--}}

                                                                    <!--  Model For Delete link item-->
                                                                    <div id="delete{{ $data->percentage_track_id }}" class="modal fade" role="dialog">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <form method="POST" action="{{ URL::to('/portal/dashboard/setupPercentage/remove') }}">
                                                                                    {{ csrf_field() }}
                                                                                    <input type="hidden" name="percentage_track_id" id="house_item_track_id" value="{{ $data->percentage_track_id }}"/>
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                        <h5 class="modal-title" style="text-align: center;">Are you want to remove this link item?</h5>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-danger center-block"><i class="fa fa-trash"></i>&nbsp;Remove</button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div> <!-- End delete Model -->


                                                                    <!--  Model For Inactive house item-->
                                                                    <div id="inActive{{ $data->percentage_track_id }}" class="modal fade" role="dialog">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <form method="POST" action="{{ URL::to('/portal/dashboard/setupPercentage/inActive') }}">
                                                                                    {{ csrf_field() }}
                                                                                    <input type="hidden" name="percentage_track_id" id="house_item_track_id" value="{{ $data->percentage_track_id }}"/>
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                        <h5 class="modal-title" style="text-align: center;">Are you want to inactive this link item?</h5>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-danger center-block"><i class="fa fa-trash"></i>&nbsp;Inactive</button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div> <!-- End Inactive Model -->
                                                                    <!-- Modal For Active house item  -->
                                                                    <div id="active{{ $data->percentage_track_id }}" class="modal fade" role="dialog">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <form method="POST" action="{{ URL::to('/portal/dashboard/setupPercentage/active') }}">
                                                                                    {{ csrf_field() }}
                                                                                    <input type="hidden" name="percentage_track_id" id="house_item_track_id" value="{{ $data->percentage_track_id }}"/>
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                        <h5 class="modal-title" style="text-align: center;">Are you want to active this link item?</h5>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-success center-block"><i class="fa fa-check"></i>&nbsp;Active</button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div> <!--- End Active Modal -->

                                                            </td>

                                                        </tr>

                                                        @php
                                                            $count++;
                                                        @endphp
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>

                                        </div>
                                        <div class="col-sm-6">
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                    <thead>
                                                    <tr>
                                                        <th>Serial</th>
                                                        <th>Technicians</th>
                                                        <th> Percentage</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @php
                                                        $count = 1;
                                                    @endphp
                                                    @if(!empty($dataListtec))
                                                        @foreach($dataListtec as $data)

                                                            <tr class="odd gradeX">
                                                                <td>{{$count}}</td>
                                                                <td>

                                                                        <span class="label label-success" style="color: #fff;font-size: 10px"> {{$data->technicains_name}}</span>


                                                                </td>
                                                                <td>{{$data->percentage_tc}}</td>
                                                                <td>
                                                                    @if($data->status === 'Active')
                                                                        <span class="label label-success" style="color: #fff;font-size: 10px"> Active</span>
                                                                    @else
                                                                        <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Inactive</span>
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if($data->status === 'Active')
                                                                        <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#inActive{{ $data->percentage_track_id }}">
                                                                            <span style="color: #C4302E"><i class="fa fa-ban"></i></span> Inactive
                                                                        </a>
                                                                    @elseif($data->status === 'Inactive')
                                                                        <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#active{{ $data->percentage_track_id }}">
                                                                            <span style="color: #489D48"><i class="fa fa-check"></i></span>  Active
                                                                        </a>
                                                                    @endif

                                                                    <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#delete{{ $data->percentage_track_id }}">
                                                                        <span style="color: #489D48"><i class="fa fa-remove"></i></span>  Remove
                                                                    </a>
                                                                {{--<a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="">--}}
                                                                {{--<span style="color: #489D48"><i class="fa fa-edit"></i></span>  Edit--}}
                                                                {{--</a>--}}

                                                                <!--  Model For Delete link item-->
                                                                    <div id="delete{{ $data->percentage_track_id }}" class="modal fade" role="dialog">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <form method="POST" action="{{ URL::to('/portal/dashboard/setupPercentage/remove/tec') }}">
                                                                                    {{ csrf_field() }}
                                                                                    <input type="hidden" name="percentage_track_id" id="house_item_track_id" value="{{ $data->percentage_track_id }}"/>
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                        <h5 class="modal-title" style="text-align: center;">Are you want to remove this link item?</h5>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-danger center-block"><i class="fa fa-trash"></i>&nbsp;Remove</button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div> <!-- End delete Model -->


                                                                    <!--  Model For Inactive house item-->
                                                                    <div id="inActive{{ $data->percentage_track_id }}" class="modal fade" role="dialog">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <form method="POST" action="{{ URL::to('/portal/dashboard/setupPercentage/inActive/tec') }}">
                                                                                    {{ csrf_field() }}
                                                                                    <input type="hidden" name="percentage_track_id" id="house_item_track_id" value="{{ $data->percentage_track_id }}"/>
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                        <h5 class="modal-title" style="text-align: center;">Are you want to inactive this link item?</h5>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-danger center-block"><i class="fa fa-trash"></i>&nbsp;Inactive</button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div> <!-- End Inactive Model -->
                                                                    <!-- Modal For Active house item  -->
                                                                    <div id="active{{ $data->percentage_track_id }}" class="modal fade" role="dialog">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <form method="POST" action="{{ URL::to('/portal/dashboard/setupPercentage/active/tec') }}">
                                                                                    {{ csrf_field() }}
                                                                                    <input type="hidden" name="percentage_track_id" id="house_item_track_id" value="{{ $data->percentage_track_id }}"/>
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                        <h5 class="modal-title" style="text-align: center;">Are you want to active this link item?</h5>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-success center-block"><i class="fa fa-check"></i>&nbsp;Active</button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div> <!--- End Active Modal -->

                                                                </td>

                                                            </tr>

                                                            @php
                                                                $count++;
                                                            @endphp
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>

                                </div>
                            </fieldset>
                        </div>
                        <!--Dashboard Section end-->
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

