<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('backend/ajax/loaddata.js') }}"></script>

@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12 " >
                    <div class="panel-group">
                        <!--Dashboard Section-->
                        <div class="panel panel-primary">
                            @if(Auth::user()->users_profile_completeness=="Yes")
                                <div class="panel-heading" id="" style="background-color:#537171; color: white;"> <i class="fa fa-list"></i> <SPAN> Sales Report</SPAN>
                                    @else
                                        <div class="panel-heading" id="" style="background-color:#537171; color: white;"> <i class="fa fa-user"></i> <SPAN>Please Update Your Profile</SPAN>
                                            @endif
                                        </div>
                                        <div class="panel-body" id="testApp">
        <div class="col-sm-12">
        <div class="form-group col-sm-4">
        <label for="users_name">State :<span class="mark">*</span> </label>
        <select class="form-control" id="statelist">
        <option value="stateselect">Select State-----</option>
       @foreach($dataList as  $data)
           <option value="{{$data->state}}">{{$data->state}}</option>
           @endforeach
        </select>
        <small class="text-danger">{{ $errors->first('tec_name') }}</small>
        </div>

        <div class="form-group col-sm-4">
        <label for="users_name">Job Status :<span class="mark">*</span> </label>
        <select class="form-control" id="status">
                <option value="status">Select status-----</option>
                <option value="JD" id="pass">Job Done</option>
                <option value="CAN" id="cancel">Cancel</option>
                 <option value="RE" id="reshediule">Reshedule</option>
                <option value="CB" id="callback">Call Back</option>
        </select>
        <small class="text-danger">{{ $errors->first('tec_name') }}</small>
        </div>

        <div class="form-group col-sm-4">
        {{--<label for="users_birthdate">Date :<span class="mark">*</span> </label>--}}
        {{--<div class="input-group ">--}}
        {{--<div class="input-group-addon">--}}
        {{--<i class="fa fa-calendar"></i>--}}
        {{--</div>--}}
        {{--<input type="text" class="form-control pull-right" id="datepicker" name="users_birthdate" required>--}}
        {{--<script>--}}
        {{--$( function() {--}}
        {{--$( "#datepicker" ).datepicker();--}}
        {{--} );--}}
        {{--</script>--}}

        {{--</div>--}}
        {{--<small class="text-danger">{{ $errors->first('users_birthdate') }}</small>--}}
        </div>
</div>

        <div class="box-body table-responsive no-padding">
        <table style="display:none;" id="displayInfo" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
        <tr>
        <th> State Name  </th>
        <th> Job Status  </th>
        <th> Lead Count  </th>
        <th> Revenue </th>
        <th> Action </th>
        </tr>
        <tr>
        <td >
            <span id="statename" class="label label-info" style="color: #fff;font-size: 12px"></span>
        </td>
        <td>
            <span  id="sts" class="label label-success" style="color: #fff;font-size: 12px"></span>
        </td>
        <td id="totallead"></td>
        <td id="balance"></td>
        <td>
            <form method="POST" action="{{ URL::to('/portal/leadinformation/details') }}">
                {{ csrf_field() }}
                <input type="hidden" id="ustate" value="" name="ustate">
                <input type="hidden" id="ujstatus" value="" name="ustatus">
                    <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-success center-block"><i class="fa fa-list"></i>&nbsp;Details</button>
            </form>

        </td>
        </tr>
        </table>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </section>
    </div>

@endsection