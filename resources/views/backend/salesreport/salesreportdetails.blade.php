<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>
@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"><i class="fa fa-list"></i> <SPAN>Lead Information</SPAN></div>
                            <fieldset style="border: 1px solid #435452 !important;border-radius: 0px;">
                                <div class="panel-body" style="padding: 0px;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- Alert Section-->
                                            @if (session('errorArray'))
                                                <div class="alert alert-danger">
                                                    @foreach($errors->all() AS $key => $value)
                                                        <strong><i class="fa fa-warning"></i> {{ $value }}</strong><br>
                                                    @endforeach
                                                </div>
                                            @endif
                                            @if (session('error'))
                                                <div class="alert alert-danger"  id="error">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('error') }}</strong>
                                                </div>
                                            @endif
                                            @if (session('success'))
                                                <div class="alert alert-success"  id="success">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('success') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="box-body table-responsive no-padding">
                                        <table id="houseList" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                            <thead style="background: #537171;color: white;">
                                            <tr>
                                                <th>SL</th>
                                                <th>Center Commision
                                                </th>
                                                <th> Technicians Percentage  </th>
                                                <th> Center Name </th>
                                                <th> Appointment Date  </th>
                                                <th> State  </th>
                                                <th> Current Revenue  </th>
                                                <th class="no-sort"> Action  </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php
                                                $count = 1;
                                            @endphp

                                            @if(!empty($dataList))
                                                @foreach($dataList as $data)
                                                    <tr>
                                                        <td>{{$count}}</td>

                                                        <td>
                                                            <?php
                                                            $centername=$data->center_name;
                                                            $userdata=\App\User::where('users_track_id',$data->center_name)->first();
                                                            $commision=\App\PercentageModel::where('center_name','=',$userdata->users_name)->exists();
                                                            ?>
                                                            <span class="label label-default" style="color: #fff;font-size: 10px"> {{$centercommison}}
                                                            </span>


                                                        </td>

                                                        <td>
                                                            <?php
                                                            $centername=$data->center_name;
                                                            $userdata=\App\User::where('users_track_id',$data->center_name)->first();
                                                            $commision=\App\PercentageModel::where('center_name','=',$userdata->users_name)->exists();
                                                            ?>

                                                                <span class="label label-default" style="color: #fff;font-size: 10px"> {{$centercommison}}
                                                            </span>

                                                        </td>


                                                        <td>
                                                            <?php
                                                            $centername=$data->center_name;
                                                            $userdata=\App\User::where('users_track_id',$data->center_name)->first();
                                                            ?>
                                                            <span class="label label-default" style="color: #fff;font-size: 10px"> {{$userdata->users_name}}
                                                            </span>

                                                        </td>
                                                        <td>{{ $data->appointment_date }}</td>
                                                        <td>{{ $data->state }}</td>
                                                        <td>{{ $data->	lead_ammount }}</td>
                                                        <td >
                                                                <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="{{url('/portal/customersinformation/details/'. $data->lead_trackid)}}"  style="color: red;width: 30px;">
                                                                    <span style="color: #C53431"><i class="fa fa-list"></i> </span>Print
                                                                </a>&nbsp;
                                                        </td>
                                                    </tr>
                                                    @php
                                                        $count++;
                                                    @endphp
                                                @endforeach
                                            @endif
                                            </tbody>
                                            <tfoot>

                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <script>
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#houseList').DataTable({
                "aaSorting": []
            });
        });
    </script>
@endsection