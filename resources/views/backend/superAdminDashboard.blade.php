<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12 " >
                    <div class="panel-group">
                        <!--Dashboard Section-->
                        <div class="panel panel-primary">
                            @if(Auth::user()->users_profile_completeness=="Yes")
                            <div class="panel-heading" id="" style="background-color:#537171; color: white;"> <i class="fa fa-dashboard"></i> <SPAN>Dashboard</SPAN>
                            @else
                                    <div class="panel-heading" id="" style="background-color:#537171; color: white;"> <i class="fa fa-user"></i> <SPAN>Please Update Your Profile</SPAN>
                                        @endif
                            </div>
                            <div class="panel-body" id="testApp">

                                @if(Auth::user()->users_profile_completeness=="Yes")
                                <section class="content">
                                    <!-- Small boxes (Stat box) -->
                                    <div class="row">
                                        <div class="col-lg-3 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box bg-aqua">
                                                <div class="inner">
                                                    <h3>{{$balance}}</h3>

                                                    <p>Total Revenue</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-dollar"></i>
                                                </div>
                                                <a href="{{url('/portal/dashboard/revenuelist')}}" class="small-box-footer">More infoormation <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box bg-green">
                                                <div class="inner">
                                                    <h3>{{$manager}}<sup style="font-size: 20px"></sup></h3>

                                                    <p>Quality Manager</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <a href="{{url('/portal/managerlist')}}" class="small-box-footer">More information <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>

                                        <!-- ./col -->
                                        <div class="col-lg-3 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box bg-yellow">
                                                <div class="inner">
                                                    <h3>{{$technicians}}</h3>

                                                    <p>Technician</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <a href="{{url('/portal/technicianlist')}}" class="small-box-footer">More information <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box bg-aqua-active">
                                                <div class="inner">
                                                    <h3>{{$jobdone}}<sup style="font-size: 20px"></sup></h3>

                                                    <p>Job Done</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <a href="{{url('/portal/dashboard/jobdonelist')}}" class="small-box-footer">More information <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box bg-green">
                                                <div class="inner">
                                                    <h3>{{$qualitytema}}<sup style="font-size: 20px"></sup></h3>

                                                    <p>Quality Team</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <a href="{{url('/portal/qualityteamlist')}}" class="small-box-footer">More information <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>



                                    </div>
                                </section>
                                @else

                                <!--Profile Section  Section-->
                                    <div class="panel-body">
                                        <form id="loginForm" method="POST" action="{{url('/login')}}">
                                            {{csrf_field()}}
                                            <div class="form-group col-sm-6">
                                                <label for="users_name">Name :<span class="mark">*</span> </label>
                                                <input type="text" class="form-control requiredOL" name="users_name" id="users_name" value="{{Auth::user()->users_name}}" disabled />
                                                <small class="text-danger"></small>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="users_email">Email :<span class="mark">*</span></label>
                                                <input type="text" class="form-control requiredOL" name="email" id="users_email" value="{{Auth::user()->email}}" disabled />
                                                <small class="text-danger"></small>
                                            </div>

                                            <div class="form-group col-sm-6">
                                                <label for="users_phone_number">Phone Number :<span class="mark">*</span> </label>
                                                <input type="text" class="form-control requiredOL" name="users_phone_number" id="users_phone_number" value="" />
                                                <small class="text-danger"></small>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="users_nationalid">National ID:<span class="mark">*</span></label>
                                                <input type="text" class="form-control requiredOL" name="users_nationalid" id="users_nationalid" value="" />
                                                <small class="text-danger"></small>
                                            </div>

                                            <div class="form-group col-sm-6">
                                                <label for="users_gender">Gender :<span class="mark">*</span> </label>
                                                <select class="form-control" id="users_gender" name="users_gender">
                                                    <option value="NULL">Select Gender------</option>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                </select>

                                                 <small class="text-danger"></small>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="image">Image :<span class="mark">*</span></label>
                                                <input type="file" id="image" name="image" value="" class="form-control" />
                                                <small class="text-danger"></small>
                                            </div>

                                            <div class="form-group col-sm-6">
                                                <label for="users_birthdate">Date Of Birth :<span class="mark">*</span> </label>
                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right" id="datepicker" name="users_birthdate">
                                                </div>
                                                <small class="text-danger"></small>
                                            </div>

                                            <div class="form-group col-sm-6">
                                                <label for="sel1">Select Country:</label>
                                                <select class="form-control" id="sel1" name="users_country">
                                                    <option>Select Country------</option>
                                                    <option value="Bangladesh">Bangladesh</option>
                                                    <option value="Japan">Japan</option>
                                                    <option value="Chine">Chine</option>
                                                    <option value="India">India</option>
                                                </select>
                                            </div>

                                            <div class="form-group col-sm-6">
                                                <label for="users_present_address">Present Address :<span class="mark">*</span> </label>
                                                <textarea class="form-control" rows="3" placeholder="Enter present address........." name="users_present_address"></textarea>
                                                <small class="text-danger"></small>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="users_permanent_address">Permanent Address:<span class="mark">*</span></label>
                                                <textarea class="form-control" rows="3" placeholder="Enter permanent address" name="users_permanent_address"></textarea>
                                                <small class="text-danger"></small>
                                            </div>

                                            <div class="form-group col-sm-6">
                                                <button class="btn btn-default btn-block " style="background-color: #537171;border-color: #537171;color: white" type="submit" id="btnLogin" name="btnLogin"> Submit</button>
                                            </div>
                                            <div class="form-group col-sm-6">

                                            </div>

                                        </form>
                                    </div>
                                    <!--End Profile Section -->
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>
    </div>

@endsection