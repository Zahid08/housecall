<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"> Detail of All Leads</div>
                            <div class="panel-body">
                                <div class="panel panel-primary">
                                    <div class="panel-body">
                                        <div class="col-md-6">
                                            <h5> Lead Status - <span class="label label-default"> Active  </span></h5>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>Lead Created  - <span class="label label-default"> {{ $datalist->created_at->format('d-m-Y') }}</span></h5>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" id="" style="background-color:#537171; color: white;">Agent Lead Information</div>
                                            <div class="panel-body">
                                                <div class="col-md-12">
                                                    <table class="table table-responsive table-striped table-bordered">
                                                        <tr>
                                                            <th> Time stamp  </th>
                                                            <th> {{$datalist->Timestamp}} </th>

                                                        </tr>
                                                        <tr>
                                                            <td>CenterName</td>
                                                            <td>
                                                                <?php
                                                                $centername=$datalist->center_name;
                                                                $userdata=\App\User::where('users_track_id',$datalist->center_name)->first();
                                                                $centartaken=\App\RevenueCalculated::where('center_name',$datalist->center_name)->first();
                                                                ?>
                                                                <span class="label label-default" style="color: #fff;font-size: 10px"> {{$userdata->users_name}}
                                                            </span>


                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th> Agent Name  </th>
                                                            <th> {{$datalist->agent_name}}</th>

                                                        </tr>
                                                        <tr>
                                                            <td>Owner Name </td>
                                                            <td>{{$datalist->owners_name}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th> Phone Number  </th>
                                                            <th> {{$datalist->phone_number}}  </th>
                                                        </tr>
                                                        <tr>
                                                            <td> Emergency Number  </td>
                                                            <td> {{$datalist->emergency_number}}   </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Street Address</td>
                                                            <td>{{$datalist->street_address}} </td>
                                                        </tr>   <tr>
                                                            <th> City  </th>
                                                            <th> {{$datalist->city}}   </th>
                                                        </tr>
                                                        <tr>
                                                            <td>State</td>
                                                            <td>{{$datalist->state}} </td>
                                                        </tr>

                                                        </tr>   <tr>
                                                            <th> Postal Code   </th>
                                                            <th> {{$datalist->postal_code}}   </th>
                                                        </tr>

                                                        <tr>
                                                            <th> Appointment Date   </th>
                                                            <th> {{$datalist->appointment_date}}   </th>
                                                        </tr>
                                                        <tr>
                                                            <td>Appointment Time</td>
                                                            <td>{{$datalist->appointment_time}} </td>
                                                        </tr>

                                                        <tr>
                                                            <th> Closer Name   </th>
                                                            <th> {{$datalist->Colsers_name}}   </th>
                                                        </tr>

                                                        <tr>
                                                            <th> Technicians Comment   </th>
                                                            <th> {{$datalist->	Tc_comments}}   </th>
                                                        </tr>


                                                    </table>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="panel panel-default">
                                            <div class="panel-heading" id="" style="background-color:#537171; color: white;">Change Status
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-md-12">
                                                    <form method="POST" action="{{ URL::to('/portal/lead/changetec') }}">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="lead_trackid" id="house_item_track_id" value="{{ $datalist->lead_trackid }}"/>
                                                        <div class="col-sm-12">
                                                            <div class="form-group col-sm-6 col-sm-offset-1 ">
                                                                <label for="users_name">Select Status :<span class="mark">*</span> </label>
                                                                <select class="form-control" id="users_gender" name="Tc_status">
                                                                    <option value="NULL">Select Status------</option>
                                                                    <option value="Pass" id="pass" >Pass</option>
                                                                    <option value="Cancel" id="show_pay">Cancel</option>
                                                                    <option value="Hold" id="show_pay1">Hold</option>
                                                                </select>
                                                                <small class="text-danger"></small>
                                                            </div>

                                                        </div>
                                                        <div id="pay_block">
                                                            <div class="col-sm-12 col-sm-offset-1">
                                                                <div class="form-group col-sm-6 ">
                                                                    <label for="users_email">Comment :<span class="mark">*</span></label>
                                                                    <textarea class="form-control" name="Tc_comments" rows="5"  cols="75" placeholder="comments here--------"></textarea>
                                                                    <small class="text-danger"></small>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-3 col-sm-offset-4" style="margin-top: 20px;" >
                                                                <button  id="btn" type="submit" return="false" name="btnSaveClass" class="btn  btn-sm btn-block" style="background-color:#537171;color: white;height: 35px;"><i class="fa fa-edit"></i> Update Status </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <div id="pay_block1">
                                                        <form method="POST" action="{{ URL::to('/portal/lead/changetec') }}">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="lead_trackid" id="house_item_track_id" value="{{ $datalist->lead_trackid }}"/>
                                                            <input type="hidden" name="Tc_status" value="Pass">
                                                            <div class="col-sm-3 col-sm-offset-3" style="margin-top: 20px;" >
                                                                <button  id="btn" type="submit" return="false" name="btnSaveClass" class="btn  btn-sm btn-block" style="background-color:#537171;color: white;height: 35px;"><i class="fa fa-edit"></i> Update Status </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                                        <script>
                                            $("#pay_block").hide();
                                            $("#pay_block1").hide();
                                            $("select").change(function(){
                                                if($("#show_pay").is(":selected")){
                                                    $("#pay_block1").slideUp("slow");
                                                    $("#pay_block").slideDown("slow");
                                                }
                                                else if($("#show_pay1").is(":selected")){
                                                    $("#pay_block1").slideUp("slow");
                                                    $("#pay_block").slideDown("slow");
                                                }
                                                else if($("#pass").is(":selected")){
                                                    $("#pay_block1").slideDown("slow");
                                                    $("#pay_block").slideUp("slow");
                                                }
                                                else { $("#pay_block").slideUp("slow"); }
                                            });

                                        </script>


                                        <script>
                                            $(document).ready(function(){
                                                $("#twohours").click(function(){

                                                    var  ftime=$("#firstime").val();

                                                    var hour=(parseInt(ftime.split(":")[0])+2)%12;
                                                    var min=ftime.split(":")[1].split(" ")[0];
                                                    am=ftime.split(":")[1].split(" ")[1];

                                                    if((parseInt(ftime.split(":")[0])+2)/12>=1)
                                                    {  console.log("hi");
                                                        if(am=="am")
                                                            am="pm";
                                                        else
                                                            am="am";
                                                    }

                                                    $("#secondtime").val(hour+":"+min+" "+am);
                                                });

                                                $("#threehours").click(function(){

                                                    var  ftime=$("#firstime").val();

                                                    var hour=(parseInt(ftime.split(":")[0])+3)%12;
                                                    var min=ftime.split(":")[1].split(" ")[0];
                                                    am=ftime.split(":")[1].split(" ")[1];

                                                    if((parseInt(ftime.split(":")[0])+2)/12>=1)
                                                    {  console.log("hi");
                                                        if(am=="am")
                                                            am="pm";
                                                        else
                                                            am="am";
                                                    }

                                                    $("#secondtime").val(hour+":"+min+" "+am);
                                                });

                                            });


                                        </script>




                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@stop