<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"> Detail of Technicians</div>
                            <div class="panel-body">
                                <div class="panel panel-primary">
                                    <div class="panel-body">
                                        <div class="col-md-4">
                                            <h5 style="color: white;"> Tehnicians Status - <span class="label label-default"> Active  </span></h5>
                                        </div>
                                        <div class="col-md-4">
                                            <h5 style="color: white;">Technicians Created  - <span class="label label-default"> {{ $datalist->created_at->format('d-m-Y') }}</span></h5>
                                        </div>

                                        <div class="panel panel-default">
                                                <div class="panel-heading" id="" style="background-color:#537171; color: white;">Technicans Information</div>
                                                <div class="panel-body">
                                                    <div class="col-md-12">
                                                        <table class="table table-responsive table-striped table-bordered">
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>{{$datalist->users_name}}</th>
                                                            </tr>
                                                            <tr>
                                                                <td>Email</td>
                                                                <td>
                                                                    <span class="label label-default" style="color: #fff;font-size: 10px">
                                                                        {{$datalist->email}}
                                                            </span>
                                                                </td>

                                                            </tr>
                                                            <tr>
                                                                <th> Phone Number</th>
                                                                <th>{{$datalist->users_phone_number}}</th>
                                                            </tr>
                                                            <tr>
                                                                <td>Gender </td>
                                                                <td>  {{$datalist->users_gender}}</td>

                                                            </tr>
                                                            <tr>
                                                                <th>Street Address  </th>
                                                                <th>  {{$datalist->users_present_address}} </th>


                                                            </tr>
                                                            <tr>
                                                                <td> Country  </td>
                                                                <td>{{$datalist->users_country}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Image</td>
                                                                <td></td>
                                                            </tr>

                                                            <tr>
                                                                <th> State  </th>
                                                                <th>

                                                                    @foreach($terchniciansList as $data)

                                                                        <label>{{$data->state}}</label>

                                                                   @endforeach
                                                                </th>
                                                            </tr>

                                                            <tr>
                                                                <td>Action</td>
                                                                <td></td>
                                                            </tr>
                                                            </tr>

                                                        </table>

                                                    </div>
                                                </div>
                                            </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </section>
    </div>

@stop