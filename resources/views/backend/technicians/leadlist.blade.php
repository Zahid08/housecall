<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>
@extends('layout.backend.master')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"><i class="fa fa-list"></i> <SPAN>Lead Information</SPAN></div>
                            <fieldset style="border: 1px solid #435452 !important;border-radius: 0px;">
                                <div class="panel-body" style="padding: 0px;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- Alert Section-->
                                            @if (session('errorArray'))
                                                <div class="alert alert-danger">
                                                    @foreach($errors->all() AS $key => $value)
                                                        <strong><i class="fa fa-warning"></i> {{ $value }}</strong><br>
                                                    @endforeach
                                                </div>
                                            @endif
                                            @if (session('error'))
                                                <div class="alert alert-danger"  id="error">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('error') }}</strong>
                                                </div>
                                            @endif
                                            @if (session('success'))
                                                <div class="alert alert-success"  id="success">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>{{ session('success') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="box-body table-responsive no-padding">
                                        <table id="houseList" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                            <thead style="background: #537171;color: white;">
                                            <tr>
                                                <th> CenterName  </th>
                                                <th> OwnerName </th>
                                                <th> PhoneNo </th>
                                                <th> StreetAddress </th>
                                                <th> City  </th>
                                                <th> AppointmentTime  </th>
                                                <th> AppointmentDate  </th>
                                                <th>Status</th>
                                                <th class="no-sort"> Action  </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($dataList))
                                                @foreach($dataList as $data)
                                                    <tr>

                                                        <td>
                                                            <?php
                                                            $centername=$data->center_name;
                                                            $userdata=\App\User::where('users_track_id',$data->center_name)->first();
                                                            $centartaken=\App\RevenueCalculated::where('center_name',$data->center_name)->first();
                                                            ?>
                                                            <span class="label label-default" style="color: #fff;font-size: 10px"> {{$userdata->users_name}}
                                                            </span>

                                                        </td>
                                                        <td>{{ $data->owners_name }}</td>
                                                        <td>{{ $data->phone_number }}</td>
                                                        <td>{{ $data->street_address }}</td>
                                                        <td>{{ $data->city }}</td>
                                                        <td>{{ $data->appointment_time }}</td>
                                                        <td>{{ $data->appointment_date }}</td>
                                                        <td>

                                                            @if($data->Tc_status === 'Pass')
                                                                <span class="label label-success" style="color: #fff;font-size: 10px"> Pass</span>
                                                            @elseif($data->Tc_status === 'Hold')
                                                                <span class="label label-info" style="background-color: red; color: #fff;font-size: 10px">Hold</span>
                                                            @elseif($data->Tc_status === 'Cancel')
                                                                <span class="label label-danger" style="background-color: red; color: #fff;font-size: 10px">Cancel </span>
                                                            @endif



                                                        </td>
                                                        <td style="width: 250px;">

                                                            <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="{{url('/portal/leadformation/details/lead/tec/'. $data->lead_trackid)}}"  style="color: red;width: 30px;">
                                                                <span style="color: #C53431"><i class="fa fa-list"></i> </span>Details
                                                            </a>&nbsp;
                                                            @if($data->Tc_status === 'Pass')
                                                            <a type="button" class="btn btn-default" style="padding: 0px 6px;font-size: 12px;" href="javascript:void(0);" data-toggle="modal" data-target="#inActive{{ $data->lead_trackid }}" style="color: red">
                                                                <span style="color: #C53431"><i class="fa fa-ban"></i> </span>Paid
                                                            </a>&nbsp;
                                                            @endif

                                                            <!--  Model For Inactive Any user Admin-->
                                                            <div id="inActive{{ $data->lead_trackid }}" class="modal fade" role="dialog">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <form method="POST" action="{{ URL::to('/portal/tecDashboard/leadlist/jobdone/paidammount') }}">
                                                                            {{ csrf_field() }}
                                                                            <input type="hidden" name="lead_trackid" id="users_track_id" value="{{ $data->lead_trackid }}"/>
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                <h5 class="modal-title" style="text-align: center;">Enter Amount For Paid Confirmation</h5>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <input type="text"  name="lead_ammount" class="form-control" placeholder="enter ammount.....">
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit"  name="btnDelete" class="btn btn-success center-block"><i class="fa fa-save"></i>&nbsp;Paid</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div> <!-- End Inactive Model -->



                                                            <div class="modal fade in" id="jobdone{{ $data->lead_trackid }}" style="display: none; padding-right: 17px;">

                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <form method="POST" action="{{ URL::to('/portal/tecDashboard/leadlist/jobdone') }}">
                                                                            {{ csrf_field() }}
                                                                            <input type="hidden" name="lead_trackid" id="lead_trackid" value="{{ $data->lead_trackid }}"/>
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                <h5 class="modal-title" style="text-align: center;">Do you want to assign this technicians ?</h5>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button> <button style="float: right;" type="submit" id="btnDelete" name="btnDelete" class="btn btn-success center-block"><i class="fa fa-save"></i>&nbsp;Yes</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                    </tr>
                                                    @endforeach
                                                    @endif
                                            </tbody>
                                            <tfoot>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script>
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#houseList').DataTable({
                "aaSorting": []
            });
        });
    </script>
    @endsection