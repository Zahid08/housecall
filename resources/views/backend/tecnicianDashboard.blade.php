<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@extends('layout.backend.master')
@section('content')

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group">
                        <!--Dashboard Section-->
                        <div class="panel panel-primary">
                            <div class="panel-heading panel-style"> <i class="fa fa-dashboard"></i> <SPAN>Dashboard</SPAN>
                            </div>
                            <fieldset style="border: 1px solid #537171 !important;border-radius: 0px;">
                                <div class="panel-body" id="testApp">

                                    <section class="content">
                                        <!-- Small boxes (Stat box) -->

                                        <div class="row">

                                            <div class="col-lg-3 col-xs-6">
                                                <!-- small box -->
                                                <div class="small-box bg-green">
                                                    <div class="inner">
                                                        <h3>{{$assignlead}}<sup style="font-size: 20px"></sup></h3>
                                                        <p>Current Lead</p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="fa fa-user"></i>
                                                    </div>
                                                    <a href="{{url('/portal/tecDashboard/leadlist')}}" class="small-box-footer">More information <i class="fa fa-arrow-circle-right"></i></a>
                                                </div>
                                            </div>

                                            <!-- ./col -->
                                            <div class="col-lg-3 col-xs-6">
                                                <!-- small box -->
                                                <div class="small-box bg-yellow">
                                                    <div class="inner">
                                                        <h3>{{$profit}}</h3>
                                                        <p>Profit</p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="fa fa-dollar"></i>
                                                    </div>
                                                    <a href="#" class="small-box-footer">More information <i class="fa fa-arrow-circle-right"></i></a>
                                                </div>
                                            </div>

                                            <!-- ./col -->
                                            <div class="col-lg-3 col-xs-6">
                                                <!-- small box -->
                                                <div class="small-box bg-purple-gradient">
                                                    <div class="inner">
                                                        <h3>{{$jobdone}}</h3>

                                                        <p>Job Done</p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="fa fa-user"></i>
                                                    </div>
                                                    <a href="{{url('/portal/tecDashboard/jobdonelist')}}" class="small-box-footer">More information <i class="fa fa-arrow-circle-right"></i></a>
                                                </div>
                                            </div>
                                            <!-- ./col -->

                                        </div>

                                    </section>

                                </div>
                            </fieldset>
                        </div>
                        <!--Dashboard Section end-->
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection