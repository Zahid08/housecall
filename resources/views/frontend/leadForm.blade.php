<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

<html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <title>Form Registration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="{{asset('frontend')}}/css/jquery-ui.css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <!--stylesheet-css-->
    <link href="http://fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">


    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

    <style>
        body {
            background-color: #537171;
            background-size: cover;
            font-family: 'PT Sans', sans-serif;
            background-attachment: fixed;
            background-position: center;
        }

        body a {
            transition: 0.5s all;
            -webkit-transition: 0.5s all;
            -moz-transition: 0.5s all;
            -o-transition: 0.5s all;
            -ms-transition: 0.5s all;
            font-weight: 400;
        }

        input[type="button"],
        input[type="submit"],
        input[type="text"],
        input[type="email"] {
            transition: 0.5s all;
            -webkit-transition: 0.5s all;
            -moz-transition: 0.5s all;
            -o-transition: 0.5s all;
            -ms-transition: 0.5s all;
            font-family: 'PT Sans', sans-serif;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            margin: 0;
            font-weight: 400;
            transition: 0.5s all;
            -webkit-transition: 0.5s all;
            -moz-transition: 0.5s all;
            -o-transition: 0.5s all;
            -ms-transition: 0.5s all;

        }

        .clear {
            clear: both;
        }

        p {
            margin: 0;
        }

        ul {
            margin: 0;
            padding: 0;
        }

        label {
            margin: 0;

        }

        textarea {}

        img {
            width: 100%;
        }

        footer {}

        footer a {}

        /*--/Reset code--*/

        h1 {
            font-size: 45px;
            font-weight: 500;
            text-align: center;
            text-transform: capitalize;
            letter-spacing: 3px;
            margin: 40px 0 40px 0;
            color: #fff;
            font-family: 'Josefin Sans', sans-serif;
        }

        .w3l-main {
            width: 45%;
            margin: 0 auto;
        }

        .w3l-from {
            flex-basis: 300px;
            padding: 3em;
            justify-content: center;
            background: #446363
        }

        label.head {
            font-size: 17px;
            font-weight: 600;
            text-align: left;
            text-transform: capitalize;
            letter-spacing: 2px;
            padding: 2px;
            margin: 2px;
            display: block;
            color: #fff;
        }

        span.title {
            color: #ca5c93;
        }

        span.w3l-star {
            font-size: 20px;
            vertical-align: middle;
            color: #d60026;
            line-height: 1;
        }

        .w3l-user input[type="text"] {
            font-size: 15px;
            font-weight: 500;
            text-align: left;
            text-transform: capitalize;
            letter-spacing: 1px;
            box-sizing: border-box;
            border: none;
            border-bottom: 2px solid #fff;
            padding: 10px;
            width: 100%;
            outline: none;
            margin: 0px auto 10px;
            color: #000000;
        }

        .w3l-mail input[type="email"] {
            font-size: 15px;
            font-weight: 500;
            text-align: left;
            text-transform: capitalize;
            letter-spacing: 1px;
            box-sizing: border-box;
            border: none;
            padding: 10px;
            width: 100%;
            outline: none;
            margin: 0px auto 10px;
            color: #000000;
        }

        .w3l-num input[type="text"] {
            font-size: 15px;
            font-weight: 500;
            text-align: left;
            text-transform: capitalize;
            letter-spacing: 1px;
            border: none;
            box-sizing: border-box;
            outline: none;
            padding: 10px;
            width: 100%;
            margin: 0 auto;
            color: #000000;
        }

        .w3l-date input.date {
            font-size: 15px;
            font-weight: 500;
            text-align: left;
            text-transform: capitalize;
            letter-spacing: 1px;
            border: none;
            box-sizing: border-box;
            outline: none;
            padding: 10px;
            width: 100%;
            margin: 0px auto 11px;
            color: #000000;
        }

        select.form-control {
            font-size: 15px;
            font-weight: 500;
            text-align: center;
            text-transform: capitalize;
            padding: 10px;
            width: 100%;
            box-sizing: border-box;
            border: none;
            outline: none;
        }

        .w3l-options1 .category1 {
            font-size: 15px;
            font-weight: 500;
            text-align: center;
            text-transform: capitalize;
            padding: 10px;
            width: 100%;
            margin: 0 auto 10px;
            box-sizing: border-box;
            border: none;
            outline: none;

        }

        .w3l-options2 .category2 {
            font-size: 15px;
            font-weight: 500;
            text-align: center;
            text-transform: capitalize;
            padding: 10px;
            width: 100%;
            margin: 0 auto 10px;
            box-sizing: border-box;
            border: none;
            outline: none;
        }

        ul li {
            list-style-type: none;
            display: inline-block;
            line-height: 1.5;
        }

        .w3l-num {
            float: left;
            width: 48%;
        }

        .w3l-date {
            float: left;
            width: 48%;
            margin-left: 4%;
        }

        .w3l-options1 {
            float: left;
            width: 48%;
        }

        .w3l-options2 {
            float: right;
            width: 48%;
        }

        .gender {
            float: left;
            width: 48%;
        }

        .w3l-sym {
            float: right;
            width: 48%;
            margin-left: 4%;
        }

        .w3l-sym input[type="text"] {
            font-size: 15px;
            font-weight: 500;
            text-align: left;
            text-transform: capitalize;
            letter-spacing: 1px;
            border: none;
            box-sizing: border-box;
            outline: none;
            padding: 10px;
            width: 100%;
            margin: 0px auto 10px;
            color: #000000;

        }

        .w3l-right textarea {
            font-size: 15px;
            font-weight: 500;
            text-align: left;
            text-transform: capitalize;
            width: 100%;
            padding: 10px;
            resize: none;
            min-height: 100px;
            border: 1px solid #fff;
            outline: none;
            background: #fff;
            box-sizing: border-box;

        }

        label.w3l-set {
            font-size: 15px;
            font-weight: 500;
            text-align: center;
            text-transform: capitalize;
            padding: 2px;
            color: #3ce0b4;
            cursor: pointer;
        }

        label.w3l-head2 {
            float: left;
            margin-right: 20px;
            letter-spacing: 1px;
        }

        .w3l-left1 {
            margin-top: 25px;
        }

        label.w3l-set2 {
            font-size: 17px;
            text-align: left;
            font-weight: 600;
            text-transform: capitalize;
            letter-spacing: 2px;
            color: #fff;
            display: block;
            margin-bottom: 10px;
        }

        label.w3l-head2 {
            font-size: 15px;
            text-transform: capitalize;
            text-align: center;
            margin-bottom: 20px;
            color: #3ce0b4;

        }

        .btn input[type="submit"] {
            font-size: 15px;
            font-weight: 500;
            text-align: center;
            text-transform: uppercase;
            letter-spacing: 2px;
            padding: 10px;
            width: 50%;
            margin: 20px auto 0px;
            box-sizing: border-box;
            border: 1px solid #fff;
            outline: none;
            cursor: pointer;
            display: block;
            color: #fff;
            background: rgba(199, 65, 132, 0);

        }

        .btn input[type="submit"]:hover {
            background-color: #005ba4;

        }

        footer {
            font-size: 15px;
            font-weight: 500;
            text-align: center;
            text-transform: capitalize;
            letter-spacing: 1px;
            margin: 20px 0 0 0;
            color: #fff;
            line-height: 1.8em;
        }

        footer a {
            font-size: 15px;
            font-weight: 600;
            text-transform: capitalize;
            text-decoration: none;
            color: #fff;
        }

        footer a:hover {
            color: #ffd200;
        }

        /*-- input-effect --*/

        .styled-input.agile-styled-input-top {
            margin-top: 0;
        }

        .styled-input input:focus~label,
        .styled-input input:valid~label,
        .styled-input textarea:focus~label,
        .styled-input textarea:valid~label {
            font-size: .9em;
            color: #fff;
            top: -1.5em;
            -webkit-transition: all 0.125s;
            -moz-transition: all 0.125s;
            -o-transition: all 0.125s;
            -ms-transition: all 0.125s;
            transition: all 0.125s;
        }

        .styled-input {
            width: 100%;
            position: relative;
            margin: 0 0;
            color: #fff;
        }

        .w3l-lef1 h3 {
            font-size: 25px;
            color: #ffd200;
            margin: 2em 0 1em;
            font-family: 'Josefin Sans', sans-serif;
        }

        .styled-input:nth-child(1),
        .styled-input:nth-child(3) {
            margin-left: 0;
        }

        .textarea-grid {
            float: none !important;
            width: 100% !important;
            margin-left: 0 !important;
        }

        .styled-input label {
            color: #8c8c8c;
            color: #bfbfbf;
            padding: 0.5em .9em;
            position: absolute;
            top: 0;
            left: 0;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            transition: all 0.3s;
            pointer-events: none;
            font-weight: 400;
            font-size: .9em;
            display: block;
            line-height: 1em;
            color: #fff;
            font-family: 'Poppins', sans-serif;
        }

        .styled-input input~span,
        .styled-input textarea~span {
            display: block;
            width: 0;
            height: 2px;
            background: #fff;
            position: absolute;
            bottom: 0;
            left: 0;
            -webkit-transition: all 0.125s;
            -moz-transition: all 0.125s;
            transition: all 0.125s;
        }

        .styled-input textarea~span {
            bottom: 5px;
        }

        .styled-input input:focus.styled-input textarea:focus {
            outline: 0;
        }

        .styled-input input:focus~span,
        .styled-input textarea:focus~span {
            width: 100%;
            -webkit-transition: all 0.075s;
            -moz-transition: all 0.075s;
            transition: all 0.075s;
        }

        .ui-datepicker .ui-datepicker-prev {
            left: 10px;
            width: 20px;
            height: 20px;
            background: url(../images/img-sp1.png) no-repeat 0px 0px;
            cursor: pointer;
        }

        .ui-datepicker .ui-datepicker-next {
            right: 10px;
            width: 20px;
            height: 20px;
            background: url(../images/img-sp.png) no-repeat 0px 0px;
            cursor: pointer;
        }

        .ui-datepicker table {
            background-color: #005ba4 !important;
        }

        /*-- //input-effect --*/

        /*--responsive--*/

        @media(max-width:1920px) {}

        @media(max-width:1680px) {}

        @media(max-width:1600px) {}

        @media(max-width:1440px) {}

        @media(max-width:1366px) {
            .w3l-main {
                width: 55%;
            }
        }

        @media(max-width:1280px) {
            .w3l-main {
                width: 55%;
            }
            label.head {
                letter-spacing: 1px;
            }
        }

        @media(max-width:1080px) {
            .w3l-main {
                width: 60%;
            }
        }

        @media(max-width:1050px) {
            .w3l-main {
                width: 60%;
            }
        }

        @media(max-width:1024px) {}

        @media(max-width:991px) {
            .w3l-from {
                padding: 2em;
            }
        }

        @media(max-width:900px) {
            h1 {
                font-size: 40px;
            }
        }

        @media(max-width:800px) {
            .w3l-main {
                width: 70%;
            }
        }

        @media(max-width:768px) {
            .w3l-from {
                padding: 2em;
            }
        }

        @media(max-width:736px) {}

        @media(max-width:667px) {
            .w3l-main {
                width: 80%;
            }
            .w3l-from {
                padding: 2em
            }
            h1 {
                letter-spacing: 2px;
            }
            .btn input[type="submit"] {
                width: 60%;
            }
        }

        @media(max-width:640px) {
            .w3l-main {
                width: 90%;
            }
            h1 {
                letter-spacing: 1px;
            }
            footer {
                letter-spacing: 0px;
            }
        }

        @media(max-width:600px) {
            h1 {
                font-size: 35px;
            }
            .w3l-from {
                padding: 2em;
            }
            footer {
                font-size: 14px;
            }
            footer a {
                font-size: 14px;
            }
        }

        @media(max-width:568px) {
            .w3l-from {
                padding: 2em;
            }
            label.head {
                letter-spacing: 0px;
            }
            .w3l-lef1 h3 {
                font-size: 22px;
                margin: 1em 0 0.5em;
            }
        }

        @media(max-width:480px) {
            h1 {
                font-size: 30px;
                line-height: 44px;
            }
            .w3l-from {
                padding: 1em;
            }
            .w3l-num {
                width: 100%;
            }
            .w3l-date {
                width: 100%;
                margin: 0;
            }
            .w3l-options1 .category1 {
                margin: 0 auto 30px;
            }
            .w3l-options1 {
                width: 100%;
            }
            .w3l-options2 {
                float: right;
                width: 100%;
            }
            .gender {
                float: left;
                width: 100%;
            }
            .w3l-sym {
                width: 100%;
            }
            .btn input[type="submit"] {
                width: 70%;
            }
        }

        @media(max-width:414px) {
            .w3l-from {
                padding: 1em;
            }
            .w3l-main {
                width: 95%;
            }
            h1 {
                font-size: 28px;
            }
            .btn input[type="submit"] {
                width: 100%;
            }
        }

        @media(max-width:384px) {
            .w3l-from {
                padding: 1em;
            }
            h1 {
                font-size: 26px;
            }
            .w3l-options1 {
                float: left;
                width: 100%;
            }
            .w3l-options2 {
                float: right;
                width: 100%;
            }
            .gender {
                float: left;
                width: 100%;
            }
            .w3l-sym {
                float: right;
                width: 100%;
                margin-left: 0%;
            }

        }

        @media(max-width:375px) {
            .w3l-from {
                padding: 1em;
            }
            .w3l-lef1 h3 {
                font-size: 20px;
            }
        }

        @media(max-width:320px) {
            .w3l-from {
                padding: 1em;
            }
            h1 {
                line-height: 36px;
            }
            .w3l-lef1 h3 {
                font-size: 18px;
            }
        }

        /*--/responsive--*/
    </style>
</head>




<body>

<!-- Alert Section -->
@if (session('success'))
<div class="alert alert-success" id="success-alert">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<strong>{{ session('success') }}</strong>
</div>
@endif

@if ($errors->any())
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                <br/>
            @endforeach
        </ul>
    </div>
@endif


<h1>Air Duct Sales Form</h1>

<div class="w3l-main">
    <div class="w3l-from">
        <form class="login100-form validate-form" method="post" action="{{url('/link/agentinformation')}}">
            {{csrf_field()}}
<input type="hidden" name="center_name" value="{{$track_id}}" >
            <div class="clear"></div>
            <div class="w3l-user">
                <label class="head">Agent Name
                    <span class="w3l-star"> * </span>
                </label>
                <input type="text" name="agent_name" placeholder="" required="">
            </div>
            <div class="w3l-user">
                <label class="head">Owners Name
                    <span class="w3l-star"> * </span>
                </label>
                <input type="text" name="owners_name" placeholder="" required="">
            </div>
            <div class="clear"></div>
            <div class="w3l-details1">
                <div class="w3l-num">
                    <label class="head">Phone Number
                        <span class="w3l-star"> * </span>
                    </label>
                    <input type="text" name="phone_number" placeholder="" required="" >
                </div>
                <div class="w3l-sym">
                    <label class="head">Alternative  Number
                        <span class="w3l-star"></span>
                    </label>
                    <input type="text" name="emergency_number" placeholder="" required="">
                </div>
                <div class="clear"></div>

            </div>
            <div class="w3l-user">
                <label class="head">Street Address
                    <span class="w3l-star"> * </span>
                </label>
                <input type="text" name="street_address" placeholder="" required="">
            </div>
            <div class="w3l-num">
                <label class="head">City
                    <span class="w3l-star"> * </span>
                </label>
                <input type="text" name="city" placeholder="" required="">
            </div>
            <div class="w3l-sym">
                <label class="head">State
                    <span class="w3l-star"> * </span>
                </label>
                <select class="form-control" name="state">
                    <option value="AL">Alabama</option>
                    <option value="AK">Alaska</option>
                    <option value="AZ">Arizona</option>
                    <option value="AR">Arkansas</option>
                    <option value="CA">California</option>
                    <option value="CO">Colorado</option>
                    <option value="CT">Connecticut</option>
                    <option value="DE">Delaware</option>
                    <option value="DC">District Of Columbia</option>
                    <option value="FL">Florida</option>
                    <option value="GA">Georgia</option>
                    <option value="HI">Hawaii</option>
                    <option value="ID">Idaho</option>
                    <option value="IL">Illinois</option>
                    <option value="IN">Indiana</option>
                    <option value="IA">Iowa</option>
                    <option value="KS">Kansas</option>
                    <option value="KY">Kentucky</option>
                    <option value="LA">Louisiana</option>
                    <option value="ME">Maine</option>
                    <option value="MD">Maryland</option>
                    <option value="MA">Massachusetts</option>
                    <option value="MI">Michigan</option>
                    <option value="MN">Minnesota</option>
                    <option value="MS">Mississippi</option>
                    <option value="MO">Missouri</option>
                    <option value="MT">Montana</option>
                    <option value="NE">Nebraska</option>
                    <option value="NV">Nevada</option>
                    <option value="NH">New Hampshire</option>
                    <option value="NJ">New Jersey</option>
                    <option value="NM">New Mexico</option>
                    <option value="NY">New York</option>
                    <option value="NC">North Carolina</option>
                    <option value="ND">North Dakota</option>
                    <option value="OH">Ohio</option>
                    <option value="OK">Oklahoma</option>
                    <option value="OR">Oregon</option>
                    <option value="PA">Pennsylvania</option>
                    <option value="RI">Rhode Island</option>
                    <option value="SC">South Carolina</option>
                    <option value="SD">South Dakota</option>
                    <option value="TN">Tennessee</option>
                    <option value="TX">Texas</option>
                    <option value="UT">Utah</option>
                    <option value="VT">Vermont</option>
                    <option value="VA">Virginia</option>
                    <option value="WA">Washington</option>
                    <option value="WV">West Virginia</option>
                    <option value="WI">Wisconsin</option>
                    <option value="WY">Wyoming</option>
                </select>



            </div>
            <div class="clear"></div>
            <div class="w3l-num">
                <label class="head">Postal / Zip Code
                    <span class="w3l-star"> * </span>
                </label>
                <input type="text" name="postal_code" placeholder="" required="">
            </div>

            <div class="w3l-date">
                <label class="head">Date of Appointment
                    <span class="w3l-star"> * </span>
                </label>
                <div class="styled-input">
                    <input class="date" id="datepicker" name="appointment_date" type="text" value="MM/DD/YYYY" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'MM/DD/YYYY';}"
                           required="">
                </div>
            </div>
            <div class="clear"></div>
            <div class="w3l-lef1">
                <label class="head">Appointment Time Frame
                    <span class="w3l-star"> * </span>
                    <label class="radio-inline">
                        <input type="radio" name="optradio" id="twohours" value="2">2 hours
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="optradio" id="threehours" value="3">3 hours
                    </label>
                    <div class="col-lg-9">
                        <div class="form-inline">
                            <div class="form-group ">
                                <div class="col-lg-12">
                                    <input class="form-control" type="text" id="firstime"  name="appointment_time"/>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="col-lg-12">
                                    <input type="text" class="form-control " id="secondtime" value="" name="appointment_time1">
                                </div>
                            </div>
                        </div>
                    </div>
                </label>
            </div>


            <script>
                $('#firstime').datetimepicker({
                    format: 'HH:mm a'

                });
            </script>


            <div class="clear"></div>
            <br>
            <div class="w3l-lef1">
                <div class="w3l-user">
                    <label class="head">Closers Name
                        <span class="w3l-star"> * </span>
                    </label>
                    <input type="text" name="Colsers_name" placeholder="" required="">
                </div>
            </div>
            <div class="w3l-rem">
                <div class="w3l-right">
                    <label class="w3l-set2">comments</label>
                    <textarea name="comments"></textarea>
                </div>
                <div class="btn" style="width: 100%;">
                    <input type="submit" name="submit" value="Submit" />
                </div>
            </div>
            <div class="clear"></div>
        </form>
    </div>
</div>



<script type="text/javascript" src="{{asset('/frontend')}}/js/jquery-2.1.4.min.js"></script>
<script src="{{asset('frontend')}}/js/jquery-ui.js"></script>

<script>
    $(document).ready(function(){
        $("#twohours").click(function(){
            var  ftime=$("#firstime").val();
            var hour=(parseInt(ftime.split(":")[0])+2)%24;
            var min=ftime.split(":")[1].split(" ")[0];
            am=ftime.split(":")[1].split(" ")[1];

            if((parseInt(ftime.split(":")[0])+2)/24>=1)
            {  console.log("hi");
                if(am=="am")
                    am="pm";
                else
                    am="am";
            }

            $("#secondtime").val(hour+":"+min+" "+am);
        });

        $("#threehours").click(function(){

            var  ftime=$("#firstime").val();

            var hour=(parseInt(ftime.split(":")[0])+3)%24;
            var min=ftime.split(":")[1].split(" ")[0];
            am=ftime.split(":")[1].split(" ")[1];

            if((parseInt(ftime.split(":")[0])+2)/24>=1)
            {  console.log("hi");
                if(am=="am")
                    am="pm";
                else
                    am="am";
            }

            $("#secondtime").val(hour+":"+min+" "+am);
        });

    });


</script>

<script>
    $(function () {
        $("#datepicker,#datepicker1").datepicker();
    });
</script>

</body>


</html>