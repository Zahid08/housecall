<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

<footer class="main-footer">
    <div class="row">
        <div class="col-md-6 col-md-offset-1">
            <div class="pull-right">
                <strong>  Copyright © 2018 Developed By <b><a href="http://brotherhoodinfotech.com" target="_blank">Brotherhoodinfotech. </a></b> All Rights Reserved.</strong>
            </div>
        </div>
    </div>
</footer>