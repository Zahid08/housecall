<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

<header class="main-header">
    @if(Auth::user()->users_type == "Super Admin")
        <a href="{{url('/portal/dashboard')}}" class="logo">
            <span class="logo-mini"style="font-size: 10px;"><img src="{{ asset('backend/images/logo-lg.jpg') }}" style="height: 30px;" /> <b>S</b>AD</span>
            <span class="logo-lg" style="font-size: 15px;"><img src="{{ asset('backend/images/logo-lg.jpg') }}" style="height: 30px;padding-right: 20px;" /><b>Super</b> admin</span>
        </a>
    @elseif(Auth::user()->users_type=="Qt")
        <a href="{{url('/portal/qtDashboard')}}" class="logo">

            <span class="logo-mini"style="font-size: 10px;"><img src="{{ asset('backend/images/logo-lg.jpg') }}" style="height: 30px;" /> <b>Q</b>T</span>
            <span class="logo-lg" style="font-size: 15px;"><img src="{{ asset('backend/images/logo-lg.jpg') }}" style="height: 30px;padding-right: 20px;" /><b>Quality</b> team</span>

        </a>

    @elseif(Auth::user()->users_type=="Qm")
        <a href="{{url('/portal/qmDashboard')}}" class="logo">
            <span class="logo-mini"style="font-size: 10px;"><img src="{{ asset('backend/images/logo-lg.jpg') }}" style="height: 30px;" /> <b>Q</b>Mng</span>
            <span class="logo-lg" style="font-size: 15px;"><img src="{{ asset('backend/images/logo-lg.jpg') }}" style="height: 30px;padding-right: 20px;" /><b>Quality</b> manager</span>

        </a>
    @elseif(Auth::user()->users_type=="Tc")
        <a href="{{url('/portal/tecDashboard')}}" class="logo">
            <span class="logo-mini"style="font-size: 10px;"><img src="{{ asset('backend/images/logo-lg.jpg') }}" style="height: 30px;" /> <b>T</b>CN</span>
            <span class="logo-lg" style="font-size: 15px;"><img src="{{ asset('backend/images/logo-lg.jpg') }}" style="height: 30px;padding-right: 20px;" /><b>Techni</b> cian</span>
        </a>
    @elseif(Auth::user()->users_type=="Center")
        <a href="{{url('/portal/centerDashboard')}}" class="logo">
            <span class="logo-mini"style="font-size: 10px;"><img src="{{ asset('backend/images/logo-lg.jpg') }}" style="height: 30px;" /> <b>{{Auth::user()->users_name}}</b></span>
            <span class="logo-lg" style="font-size: 15px;"><img src="{{ asset('backend/images/logo-lg.jpg') }}" style="height: 30px;padding-right: 20px;" /><b>{{Auth::user()->users_name}}</b> </span>
        </a>
    @elseif(Auth::user()->users_type=="HR")
        <a href="{{url('/portal/hrDashboard')}}" class="logo">
            <span class="logo-mini"style="font-size: 10px;"><img src="{{ asset('backend/images/logo-lg.jpg') }}" style="height: 30px;" /> <b>H</b>R</span>
            <span class="logo-lg" style="font-size: 15px;"><img src="{{ asset('backend/images/logo-lg.jpg') }}" style="height: 30px;padding-right: 20px;" /><b>H</b>R </span>
        </a>
    @elseif(Auth::user()->users_type=="Admin")
        <a href="{{url('/portal/admindashboard')}}" class="logo">
            <span class="logo-mini"style="font-size: 10px;"><img src="{{ asset('backend/images/logo-lg.jpg') }}" style="height: 30px;" /> <b>Ad</b>min</span>
            <span class="logo-lg" style="font-size: 15px;"><img src="{{ asset('backend/images/logo-lg.jpg') }}" style="height: 30px;padding-right: 20px;" /><b>Ad</b>min</span>
        </a>
    @endif

    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                @if(Auth::user()->users_type == "Tc")
                    <li class="dropdown messages-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success">
                            <?php
                                $messageList = App\ConversiationModel::leftJoin('conversation_attachment', 'conversation.conversation_track_id', 'conversation_attachment.ca_conversation_id')
                                    ->where('conversation_sent_to', Auth::user()->users_track_id)
                                    ->where('conversation_status', 'Unread')
                                    ->count();
                                ?>
                                {{ $messageList }}
                        </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header" style="background-color: #537171;color: white;">You have {{ $messageList }} messages</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                @php
                                    $listData = App\ConversiationModel::leftJoin('conversation_attachment', 'conversation.conversation_track_id', 'conversation_attachment.ca_conversation_id')
                                    ->where('conversation_sent_to', Auth::user()->users_track_id)
                                    ->where('conversation_status', 'Unread')
                                    ->orderBy('conversation.created_at', 'DESC')
                                    ->select('conversation.*', 'conversation_attachment.*')
                                    ->get();

                                    $wordcount = new App\ConversiationModel();
                                @endphp

                                <div class="slimScrollDiv" style="overflow: hidden;position: relative;width: 100%;height: auto;">
                                    <ul class="menu" style="overflow: hidden;position: relative;width: 100%;height: auto;">
                                        @foreach($listData as $data)
                                            <li style="background-color: #9CB770;color: white;"><!-- start message -->
                                                <a href="{{ URL::to('/portal/message/single/'.$data->conversation_track_id ) }}">
                                                    <h4 style="color: white;">{{$data->conversation_subject}}
                                                        <small>
                                                            <i class="fa fa-clock-o"></i>
                                                            <?php
                                                            $createdList = $data->created_at->diffInWeeks(\Carbon\Carbon::now()) >= 1 ?
                                                                $data->created_at->format('j M Y , g:ia') : $data->created_at->diffForHumans();
                                                            ?>
                                                            {{ $createdList }}
                                                        </small>
                                                    </h4>
                                                </a>
                                            </li>
                                    @endforeach
                                    <!-- end message -->
                                    </ul>
                                </div>
                            </li>
                            <li class="footer"><a href="{{ URL::to('/portal/message/techniciansInbox/list') }}">See All Messages</a></li>
                        </ul>
                    </li>
                    @elseif(Auth::user()->users_type == "Qm")
                    <li class="dropdown messages-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success">
                            <?php
                                $messageList = App\ConversiationModel::leftJoin('conversation_attachment', 'conversation.conversation_track_id', 'conversation_attachment.ca_conversation_id')
                                    ->where('conversation_sent_to', Auth::user()->users_track_id)
                                    ->where('conversation_status', 'Unread')
                                    ->count();
                                ?>
                                {{ $messageList }}
                        </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header" style="background-color: #537171;color: white;">You have {{ $messageList }} messages</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                @php
                                    $listData = App\ConversiationModel::leftJoin('conversation_attachment', 'conversation.conversation_track_id', 'conversation_attachment.ca_conversation_id')
                                    ->where('conversation_sent_to', Auth::user()->users_track_id)
                                    ->where('conversation_status', 'Unread')
                                    ->orderBy('conversation.created_at', 'DESC')
                                    ->select('conversation.*', 'conversation_attachment.*')
                                    ->get();

                                    $wordcount = new App\ConversiationModel();
                                @endphp

                                <div class="slimScrollDiv" style="overflow: hidden;position: relative;width: 100%;height: auto;">
                                    <ul class="menu" style="overflow: hidden;position: relative;width: 100%;height: auto;">
                                        @foreach($listData as $data)
                                            <li style="background-color: #9CB770;color: white;"><!-- start message -->
                                                <a href="{{ URL::to('/portal/message/single/manager/'.$data->conversation_track_id ) }}">
                                                    <h4 style="color: white;">{{$data->conversation_subject}}
                                                        <small>
                                                            <i class="fa fa-clock-o"></i>
                                                            <?php
                                                            $createdList = $data->created_at->diffInWeeks(\Carbon\Carbon::now()) >= 1 ?
                                                                $data->created_at->format('j M Y , g:ia') : $data->created_at->diffForHumans();
                                                            ?>
                                                            {{ $createdList }}
                                                        </small>
                                                    </h4>
                                                </a>
                                            </li>
                                    @endforeach
                                    <!-- end message -->
                                    </ul>
                                </div>
                            </li>
                            <li class="footer"><a href="{{ URL::to('/portal/message/inbox/messagelist') }}">See All Messages</a></li>
                        </ul>
                    </li>
                @endif

            @if(Auth::user()->users_type == "Tc")
                <!-- Notifications: style can be found in dropdown.less -->
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning">
                            <?php
                            $trackid=\Illuminate\Support\Facades\Auth::user()->users_track_id;
                            $dataList= \App\LeadInformationModel::where('tec_trackid', $trackid)
                                ->where('job_done','No')
                                ->count();
                            ?>
                            {{$dataList}}

                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have {{$dataList}} lead information</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <div class="slimScrollDiv" style="overflow: hidden;position: relative;width: 100%;height: auto;">

                            </div>
                        </li>
                        <li class="footer"><a href="{{url('/portal/tecDashboard/leadlist')}}">View all</a></li>
                    </ul>
                </li>
              @endif


                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if(empty(Auth::user()->image ))
                            <img src="{{ asset('backend/images/user.jpg' ) }}" alt="User Image"  class="user-image">
                        @else
                            <img src="{{ asset('upload/users/'. Auth::user()->image ) }}" alt="User Image"  class="user-image">
                        @endif
                        <span class="hidden-xs"> {{Auth::user()->users_name}}</span>
                    </a>
                </li>
                <li class="user user-menu">
                    <a href="javascript:void(0)">
                        <i class="fa fa-ellipsis-v occArrow icon-padding" aria-hidden="true"></i>
                    </a>
                </li>

                <li class="user user-menu">
                    <a role="button" data-toggle="modal" data-target="#modal-default">
                        <span><i class="fa fa-sign-out"></i>&nbsp;Logout</span>
                    </a>
                </li>

                </li>
            </ul>
        </div>

    </nav>
</header>


<div class="modal fade in" id="modal-default" style="display: none; padding-right: 17px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title" style="text-align: center;">Do you want to logout ?</h4>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button><a type="button" class="btn btn-primary" href="{{url('/logout')}}">Yes</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


