<?php
echo header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
echo header("Cache-Control: post-check=0, pre-check=0", false);
echo header("Pragma: no-cache");
echo header('Content-Type: text/html');
?>

@if(Auth::user()->users_profile_completeness=="Yes")
    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu">
                <li class="header" style="font-size: 13px;">All Menu</li>
                @if(Auth::user()->users_type == "Super Admin")
                    <li class="treeview" id="dashActive">
                        <a href="{{ URL::to('/portal/dashboard') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span> </a>
                    </li>
                @elseif(Auth::user()->users_type=="Qt")
                    <li class="treeview" id="dashActive">
                        <a href="{{ URL::to('/portal/qtDashboard') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span> </a>
                    </li>
                @elseif(Auth::user()->users_type=="Qm")
                    <li class="treeview" id="dashActive">
                        <a href="{{ URL::to('/portal/qmDashboard') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span> </a>
                    </li>
                @elseif(Auth::user()->users_type=="Tc")
                    <li class="treeview" id="dashActive">
                        <a href="{{ URL::to('/portal/tecDashboard') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span> </a>
                    </li>
                @elseif(Auth::user()->users_type=="Center")
                    <li class="treeview" id="dashActive">
                        <a href="{{ URL::to('/portal/centerDashboard') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span> </a>
                    </li>
                @elseif(Auth::user()->users_type=="HR")
                    <li class="treeview" id="dashActive">
                        <a href="{{ URL::to('/portal/hrDashboard') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span> </a>
                    </li>
                @elseif(Auth::user()->users_type=="Admin")
                    <li class="treeview" id="dashActive">
                        <a href="{{ URL::to('/portal/admindashboard') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span> </a>
                    </li>
                @endif

                <li class="treeview">
                    <a href="#"><i class="fa fa-cog"></i><span>Basic Settings</span><i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li id="ownerAddActive">
                            <a href="{{url('/portal/profile')}}"><i class="fa fa-user-plus"></i><span>Profile</span></a>
                        </li>
                        <li id="ownerActive">
                            <a href="{{url('/portal/profile/passwordEdit')}}"><i class="fa fa-lock"></i><span>Password Change</span></a>
                        </li>
                    </ul>
                </li>

                @if(Auth::user()->users_type == "Super Admin" )

                    <li class="treeview">
                        <a href="#"><i class="fa fa-list-ol"></i><span>Revenue</span><i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li id="ownerAddActive">
                                <a href="{{url('/portal/dashboard/setupPercentage')}}"><i class="fa fa-user-plus"></i><span>Setup Percentage</span></a>
                            </li>
                            <li id="ownerActive">
                                <a href="{{url('/portal/dashboard/revenuelist')}}"><i class="fa fa-list"></i><span>Revenue list</span></a>
                            </li>
                        </ul>
                    </li>

                    {{--<li id="ownerActive">--}}
                    {{--<a href="{{url('/portal/qualityteamlist')}}"><i class="fa fa-list"></i><span>QualityTeam List</span></a>--}}
                    {{--</li>--}}

                    <li id="ownerActive">
                        <a href="{{url('/portal/managerlist')}}"><i class="fa fa-list"></i><span>All User List</span></a>
                    </li>

                    {{--<li id="ownerActive">--}}
                    {{--<a href="{{url('/portal/dashboard/hrlist')}}"><i class="fa fa-list"></i><span>HR List</span></a>--}}
                    {{--</li>--}}



                    <li class="treeview" id="ownerAddActive">
                        <a href="{{ URL::to('/portal/activitylog') }}"><i class="fa fa-lock"></i><span>Activity Log</span> </a>
                    </li>

                    {{--<li class="treeview" id="ownerAddActive">--}}
                    {{--<a href="{{ URL::to('/portal/registeruserlist') }}"><i class="fa fa-list"></i><span>Registerd User List</span> </a>--}}
                    {{--</li>--}}

                    {{--<li class="treeview" id="ownerAddActive">--}}
                    {{--<a href="{{ URL::to('/portal/dashboard') }}"><i class="fa fa-list"></i><span>Total Revenue List</span> </a>--}}
                    {{--</li>--}}

                    {{--<li class="treeview" id="ownerAddActive">--}}
                    {{--<a href="{{ URL::to('/portal/technicianlist') }}"><i class="fa fa-list"></i><span>Technician List</span> </a>--}}
                    {{--</li>--}}

                    <li class="treeview" id="ownerAddActive">
                        <a href="{{ URL::to('/portal/dashboard/jobdonelist') }}"><i class="fa fa-list"></i><span>Job doneList</span> </a>
                    </li>

                    {{--<li class="treeview" id="ownerAddActive">--}}
                    {{--<a href="{{ URL::to('/portal/dashboard') }}"><i class="fa fa-list"></i><span>InvoiceList</span> </a>--}}
                    {{--</li>--}}

                    <li class="treeview" id="ownerAddActive">
                        <a href="{{ URL::to('/portal/agentLink') }}"><i class="fa fa-list"></i><span>Agent LinkGenerate</span> </a>
                    </li>

                    <li id="ownerAddActive">
                        <a href="{{url('/portal/dashboard/centerRegistration')}}"><i class="fa fa-list"></i><span>Registration Center</span></a>
                    </li>

                    <li id="ownerActive">
                        <a href="{{url('/portal/dashboard/salesreport')}}"><i class="fa fa-list"></i><span>Sales Report</span></a>
                    </li>



                    {{-- If usertupe=Quality Team
                   *He can access those side link
               --}}

                @elseif(Auth::user()->users_type == "Admin")


                    <li id="ownerActive">
                        <a href="{{url('/portal/managerlist')}}"><i class="fa fa-list"></i><span>All User List</span></a>
                    </li>


                    <li class="treeview" id="ownerAddActive">
                        <a href="{{ URL::to('/portal/dashboard/jobdonelist') }}"><i class="fa fa-list"></i><span>Job doneList</span> </a>
                    </li>

                    <li class="treeview" id="ownerAddActive">
                        <a href="{{ URL::to('/portal/agentLink') }}"><i class="fa fa-list"></i><span>Agent LinkGenerate</span> </a>
                    </li>

                    <li id="ownerAddActive">
                        <a href="{{url('/portal/dashboard/centerRegistration')}}"><i class="fa fa-list"></i><span>Registration Center</span></a>
                    </li>
                @elseif(Auth::user()->users_type == "Qt")
                    <li class="treeview" id="dashActive">
                        <a href="{{ URL::to('/portal/contactCustomers') }}"><i class="fa fa-list"></i><span>Pass Lead List</span> </a>
                    </li>

                @elseif(Auth::user()->users_type == "Qm" || Auth::user()->users_type == "HR" )

                    <li class="treeview">
                        <a href="#"><i class="fa fa-user"></i><span>Manage Technician</span><i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li id="ownerAddActive">
                                <a href="{{url('/portal/qmDashboard/addTecnicians')}}"><i class="fa fa-user-plus"></i><span>Add Technician</span></a>
                            </li>
                            <li id="ownerActive">
                                <a href="{{url('/portal/qmDashboard/listTc')}}"><i class="fa fa-list"></i><span>Technician List</span></a>
                            </li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#"><i class="fa fa-envelope"></i><span>Message</span><i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li id="ownerAddActive">
                                <a href="{{url('/portal/message/compose/add')}}"><i class="fa fa-edit"></i><span>Compose</span></a>
                            </li>
                            <li id="ownerAddActive">
                                <a href="{{url('/portal/message/inbox/messagelist')}}"><i class="fa fa-paper-plane"></i><span>Inbox</span></a>
                            </li>

                            <li id="ownerActive">
                                <a href="{{url('portal/message/tecniciansSent/list')}}"><i class="fa fa-play-circle"></i><span>Sent Message</span></a>
                            </li>
                        </ul>
                    </li>

                    <li class="treeview" id="dashActive">
                        <a href="{{ URL::to('/portal/qmDashboard/jobdonelist') }}"><i class="fa fa-list"></i><span>Job Done List</span> </a>
                    </li>

                    <li class="treeview" id="dashActive">
                        <a href="{{url('/portal/qmDashboard/revenueList')}}"><i class="fa fa-list"></i><span>Revenue List</span> </a>
                    </li>


                    <li class="treeview" id="dashActive">
                        <a href="{{ URL::to('/portal/qmDashboard/contact') }}"><i class="fa fa-phone"></i><span>Contact Customer</span> </a>
                    </li>


                @elseif(Auth::user()->users_type == "Tc")
                    <li class="treeview">
                        <a href="#"><i class="fa fa-list"></i><span>Message</span><i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li id="composeActive">
                                <a href="{{ URL::to('/portal/message/techniciansCompose/add') }}"><i class="fa fa-pencil"></i><span>Compose</span></a>
                            </li>
                            <li id="inboxActive">
                                <a href="{{ URL::to('/portal/message/techniciansInbox/list') }}"><i class="fa fa-inbox"></i><span>Inbox</span></a>
                            </li>
                            <li id="sentActive">
                                <a href="{{ URL::to('portal/message/texhnicianSent/list') }}"><i class="fa fa-paper-plane"></i><span>Sent</span></a>
                            </li>

                        </ul>
                    </li>

                    <li class="treeview" id="dashActive">
                        <a href="{{ URL::to('/portal/tecDashboard/jobdonelist') }}"><i class="fa fa-list"></i><span>Job Done List</span> </a>
                    </li>

                    <li class="treeview" id="ownerAddActive">
                        <a href="{{url('/portal/tecDashboard/leadlist')}}"><i class="fa fa-list"></i><span>Customer Information</span> </a>
                    </li>

                @elseif(Auth::user()->users_type == "Center")
                    <li class="treeview" id="ownerAddActive">
                        <a href="{{url('/portal/centerDashboard/leadinformation')}}"><i class="fa fa-list"></i><span>Lead Information</span> </a>
                    </li>

                    {{--@elseif(Auth::user()->users_type == "HR")--}}

                    {{--<li class="treeview" id="ownerAddActive">--}}
                    {{--<a href="{{ URL::to('/portal/customersinformation') }}"><i class="fa fa-list"></i><span>QualityTeam Information</span> </a>--}}
                    {{--</li>--}}

                    {{--<li class="treeview">--}}
                    {{--<a href="#"><i class="fa fa-user"></i><span>Manage Technician</span><i class="fa fa-angle-left pull-right"></i></a>--}}
                    {{--<ul class="treeview-menu">--}}
                    {{--<li id="ownerAddActive">--}}
                    {{--<a href="{{url('/portal/qmDashboard/addTecnicians')}}"><i class="fa fa-user-plus"></i><span>Add Technician</span></a>--}}
                    {{--</li>--}}
                    {{--<li id="ownerActive">--}}
                    {{--<a href="{{url('/portal/qmDashboard/listTc')}}"><i class="fa fa-list"></i><span>Technician List</span></a>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="treeview">--}}
                    {{--<a href="#"><i class="fa fa-envelope"></i><span>Message</span><i class="fa fa-angle-left pull-right"></i></a>--}}
                    {{--<ul class="treeview-menu">--}}
                    {{--<li id="ownerAddActive">--}}
                    {{--<a href="{{url('/portal/message/compose/add')}}"><i class="fa fa-edit"></i><span>Compose</span></a>--}}
                    {{--</li>--}}
                    {{--<li id="ownerAddActive">--}}
                    {{--<a href="{{url('/portal/message/inbox/messagelist')}}"><i class="fa fa-paper-plane"></i><span>Inbox</span></a>--}}
                    {{--</li>--}}

                    {{--<li id="ownerActive">--}}
                    {{--<a href="{{url('portal/message/tecniciansSent/list')}}"><i class="fa fa-play-circle"></i><span>Sent Message</span></a>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="treeview" id="dashActive">--}}
                    {{--<a href="{{ URL::to('/portal/qmDashboard/jobdonelist') }}"><i class="fa fa-list"></i><span>Job Done List</span> </a>--}}
                    {{--</li>--}}

                    {{--<li class="treeview" id="dashActive">--}}
                    {{--<a href="{{url('/portal/qmDashboard/revenueList')}}"><i class="fa fa-list"></i><span>Revenue List</span> </a>--}}
                    {{--</li>--}}

                    {{--<li class="treeview" id="ownerAddActive">--}}
                    {{--<a href="{{url('/portal/qmDashboard/customerinfo')}}"><i class="fa fa-list"></i><span>Customer Information</span> </a>--}}
                    {{--</li>--}}

                    {{--<li class="treeview" id="dashActive">--}}
                    {{--<a href="{{ URL::to('/portal/qmDashboard/contact') }}"><i class="fa fa-phone"></i><span>Contact Customer</span> </a>--}}
                    {{--</li>--}}

                @endif

                @if(Auth::user()->users_type == "Qt")
                    <li class="treeview" id="ownerAddActive">
                        <a href="{{ URL::to('/portal/customersinformation') }}"><i class="fa fa-list"></i><span>Lead Information</span> </a>
                    </li>
                @endif


                @if(Auth::user()->users_type == "Super Admin" || Auth::user()->users_type == "Qm" || Auth::user()->users_type == "Admin" )
                <li class="treeview" id="ownerAddActive">
                    <a href="{{url('/portal/qmDashboard/customerinfo')}}" ><i class="fa fa-list"></i><span>Lead Information</span> </a>
                </li>
                @endif

                @if(Auth::user()->users_type == "HR")
                    <li class="treeview" id="ownerAddActive">
                        <a href="{{ URL::to('/portal/customersinformation') }}"><i class="fa fa-list"></i><span>Quality TeamInfo</span> </a>
                    </li>
            @endif

            <!--Customer information for all-->

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
@endif


{{-- Commoon for every one --}}
@if(Auth::user()->users_profile_completeness=="No" || Auth::user()->users_profile_completeness==NULL)
    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu">

                <li class="header" style="font-size: 13px;">All Menu</li>
                @if(Auth::user()->users_type == "Super Admin")
                    <li class="treeview" id="dashActive">
                        <a href="{{ URL::to('/portal/dashboard') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span> </a>
                    </li>
                @elseif(Auth::user()->users_type=="Qt")
                    <li class="treeview" id="dashActive">
                        <a href="{{ URL::to('/portal/dashboard') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span> </a>
                    </li>
                @elseif(Auth::user()->users_type=="Qm")
                    <li class="treeview" id="dashActive">
                        <a href="{{ URL::to('/portal/dashboard') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span> </a>
                    </li>
                @elseif(Auth::user()->users_type=="Tc")
                    <li class="treeview" id="dashActive">
                        <a href="{{ URL::to('/portal/dashboard') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span> </a>
                    </li>
                @endif

            </ul>
        </section>
    </aside>
@endif

