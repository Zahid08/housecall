<?php

Route::get('pagenotfound',function (){
   Auth::logout();
   return view('errors.503');
});

/***************************************** LOG OUT SECTION ******************************************/
Route::get('logout', function () {
    Auth::logout();
    return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
});

//Same Package or Folder
Route::group(['namespace'=>'backend'],function (){

    Route::get('/linktrack','AgentController@linkstate');
    Route::get('/linktrack/{linkname}/{trkid}','AgentController@Page');
    Route::post('/link/agentinformation','AgentController@postData');
    //profileconleteness
    Route::post('/portal/profilecompleteness','ProfileController@CompleteProfile');
    //lead info
    Route::get('/portal/customersinformation','LeadInformationController@leadinformation');
    Route::post('/portal/lead/change','LeadInformationController@statusUpdateLead');
    Route::post('/portal/lead/comments','LeadInformationController@comentLeadsInfo');
    Route::post('/portal/lead/addrecording','LeadInformationController@addRecording');

    Route::post('/portal/leadformation/details/lead/updateajax','LeadInformationController@updateajaxcallLeadinfo');

    //mail specification
    Route::post('portal/profile/forgotPassword/link', 'ProfileController@forgotPasswordLink');
    Route::get('portal/profile/resetPassword/{id}', 'ProfileController@passwordResetPage');
    Route::post('portal/profile/updatePassord', 'ProfileController@passwordUpdate');


    Route::get('/','LoginControllerBI@loginuser');
    Route::post('/login','LoginControllerBI@authenticate');
    Route::get('/forgotpassword','LoginControllerBI@forgotpassword');
    Route::get('/registration','RegistrationController@registration');
    Route::post('/registationuser','RegistrationController@registrationUser');

    //super admin profile
    Route::get('/portal/profile','ProfileController@superAdminProfile');
    Route::get('/portal/profile/passwordEdit','ProfileController@updatePassword');
    Route::post('portal/profile/passwordUpdate','ProfileController@updatePasswordConfirmed');
    Route::get('/portal/profile/edit','ProfileController@profileEdit');
    Route::post('portal/profile/update','ProfileController@update');

/*********************************************  SUPER ADMIN MIDDLEWARE  AND DATA********************************************** */

    Route::group(['middleware'=>'Super Admin'],function (){
        Route::get('/portal/dashboard','DashboardController@superadminDashboard');
        Route::get('/portal/dashboard/salesreport','SalesreportController@salesReport');
        Route::post('/ajax/leadinfo','SalesreportController@LeadData');
    });

    Route::post('/portal/managerlist/changedesignation','RegistrationController@changeDesignation');


    Route::get('/portal/activitylog','ActivityController@ActivityList');

    Route::post('/portal/leadinformation/details','SalesreportController@detailsreport');

    //  Route::get('/portal/registeruserlist','RegistrationController@registerUserlist');
    Route::post('/portal/registeruser/active','RegistrationController@registerdUser');
    Route::get('/portal/managerlist','ManagerController@managerList');
    //  Route::get('/portal/dashboard/hrlist','ManagerController@hrList');
    Route::get('/portal/add/newmanager','ManagerController@addNewManager');
    Route::get('/portal/userlist/details/{id}','ManagerController@detailsUserList');
    Route::post('portal/manager/inActive','ManagerController@inactiveManager');
    Route::post('portal/manager/Active','ManagerController@activeUser');
    //   Route::get('/portal/qualityteamlist','QualityTeamController@qualityTeamList');
    Route::get('/portal/add/newqualityteam','QualityTeamController@newqualityTeam');
    //    Route::get('/portal/technicianlist','TechnicianController@techniciansList');

    Route::get('/portal/agentLink','AgentController@agentLink');

    Route::post('/portal/agentlinkgenerate','AgentController@agentLinkGenerate');

    Route::post('/portal/agentLink/active','AgentController@activeAgentLink');
    Route::post('/portal/agentLink/inActive','AgentController@inActiveAgentLink');
    Route::post('/portal/agentLink/remove','AgentController@removeAgentLink');

    //leadlist
    Route::get('/portal/customersinformation/details/{id}','LeadInformationController@detailsLeadlist');
    Route::get('/portal/customersinformation/details/lead/{id}','LeadInformationController@detailsLeadlist');

    Route::get('/portal/dashboard/jobdonelist','LeadInformationController@jobDoneList');
    //remove activity
    Route::post('/portal/activitylog/remove','ActivityController@removeActivity');

    //percemtage seup
    Route::get('/portal/dashboard/setupPercentage','PercentageController@percentageSetup');
    Route::post('/portal/dashboard/setupPercentage/newadd','PercentageController@newpercentageSetup');

    Route::post('/portal/dashboard/setupPercentage/technicians','PercentageController@newpercentageSetupTech');

    Route::post('/portal/dashboard/setupPercentage/inActive','PercentageController@inactivePercentage');

    Route::post('/portal/dashboard/setupPercentage/inActive/tec','PercentageController@inactivePercentagetec');

    Route::post('/portal/dashboard/setupPercentage/active','PercentageController@activePercentage');

    Route::post('/portal/dashboard/setupPercentage/active/tec','PercentageController@activePercentagetec');
    Route::post('/portal/dashboard/setupPercentage/remove','PercentageController@removePercentage');

    Route::post('/portal/dashboard/setupPercentage/remove/tec','PercentageController@removePercentagetec');

    //revenue lsit
    Route::get('/portal/dashboard/revenuelist','PercentageController@revenueList');

    //center
    Route::get('/portal/dashboard/centerRegistration','CenterRegistration@centerregistration');
    Route::post('/portal/dashboard/centerRegistration/add','CenterRegistration@centerregistrationnew');

    Route::post('/portal/dashboard/centerRegistration/inActive','CenterRegistration@inactiveCenter');

    Route::post('/portal/dashboard/centerRegistration/active','CenterRegistration@activeCenter');
    Route::post('/portal/dashboard/centerRegistration/remove','CenterRegistration@removeCenter');


    /*********************************************  ADMIN MIDDLEWARE  AND LEAST ON********************************************** */
    Route::group(['middleware'=>'Admin'],function (){
        Route::get('/portal/admindashboard','DashboardController@adminDashboard');
    });


    /*********************************************  QUALITY team  MIDDLEWARE  AND LEAST ON********************************************** */
    Route::group(['middleware'=>'Qt'],function (){
        Route::get('/portal/qtDashboard','DashboardController@qualityTeamDashboard');
        Route::get('/portal/contactCustomers','LeadInformationController@contactCustomsers');
        //
    });

    /*********************************************  QUALITY MANAGER  MIDDLEWARE  AND LEAST ON********************************************** */
    Route::group(['middleware'=>'Qm'],function (){
        Route::get('/portal/qmDashboard','DashboardController@qualityManagementDashboard');
    });


   // Route::get('/portal/qmDashboard/customerinfo','ManagerController@leadinformation');

    Route::get('/portal/qmDashboard/customerinfo','ManagerController@leadinformation');

    Route::get('/portal/qmDashboard/customerinfo/details','ManagerController@leadinformationDetails');

    Route::post('/portal/qmDashboard/customerinfo/search','ManagerController@leaddetailsinformation');

    Route::post('/portal/lead/changestatusqm','LeadInformationController@changeLeadstatus');

    Route::post('/portal/leadformation/details/lead/update','LeadInformationController@updateLeadinformation');


    Route::get('/portal/leadformation/details/lead/{id}','ManagerController@detailsleadinformation');
    //lead
    Route::post('/portal/lead/changeQm','ManagerController@leadStatusChange');
    //tecnician add route
    Route::get('portal/qmDashboard/addTecnicians','ManagerController@newTechnicainaddview');

    Route::post('/portal/qmDashboard/addTecnicians/new','ManagerController@newTechnicians');

    Route::get('/portal/qmDashboard/assigntechnicianlist/{id}','ManagerController@assignTechnicianList');
  //  Route::post('/portal/qmDashboard/assigntechnicianlist','ManagerController@assignTechnicianList');

    Route::get('/portal/qmDashboard/listTc','ManagerController@listTc');

    Route::get('/portal/qmDashboard/listTc/details/{id}','ManagerController@DetailslistTc');

    Route::post('portal/qmDashboard/customerinfo/assign','ManagerController@assigConfirmed');
    //jobdonelist
    Route::get('/portal/qmDashboard/jobdonelist','ManagerController@jobDoneList');

    Route::get('/portal/qmDashboard/contact','ManagerController@contactCustomer');
    //lead assign

    //message
    Route::get('/portal/message/compose/add','MessageController@messageCompose');
    //ajaxload  street address
    Route::post('/getstreetaddress','MessageController@getstreetaddress');

    //message
    Route::post('/portal/message/compose/store','MessageController@storeMessageCompose');

    Route::post('/hrportal/message/compose/store','MessageController@storeMessageCompose');

    Route::get('portal/message/tecniciansSent/list','MessageController@technicinasList');
    Route::get('/portal/message/technicinassend/{id}','MessageController@viewtechnicinas');

    //delete message
    Route::post('/portal/message/deletemessage','MessageController@deleteMessage');

    Route::get('/portal/message/inbox/messagelist','MessageController@inboxMessage');

    //inbox message view
    Route::get('/portal/message/single/{id}','MessageController@viewInboxmessage');

    //inbox message delete
    Route::post('portal/message/delete/inbox','MessageController@inboxDelete');

    //message replay
    Route::get('portal/message/reply/{id}','MessageController@replayMessage');

    Route::post('/portal/message/reply/store','MessageController@sendReplayMessage');
    Route::get('/portal/message/single/manager/{id}', 'MessageController@singleMessageViewManager');

    Route::post('/portal/qmDashboard/jobdonelist/confirmation','PercentageController@confirmedLeadPercentage');

    //inactive tech
    Route::post('/portal/qmDashboard/listTc/inactive','TechnicianController@inactiveTechnicians');
    Route::post('/portal/qmDashboard/listTc/active','TechnicianController@activeTecnicians');
    Route::post('/portal/qmDashboard/listTc/remove','TechnicianController@removeTechnicians');
    Route::get('/portal/qmDashboard/revenueList','PercentageController@revenueListM');


    /*********************************************  TECHNICIANS MIDDLEWARE  AND LEAST ON********************************************** */
    Route::group(['middleware'=>'Tec'],function (){
        Route::get('/portal/tecDashboard','DashboardController@tecnicianDashboard');
        Route::get('/portal/tecDashboard/leadlist','TechnicianController@cutomerlist');

        Route::get('/portal/leadformation/details/lead/tec/{id}','TechnicianController@detailsLead');

        Route::get('/portal/tecDashboard/jobdonelist','TechnicianController@jobDonelist');
        Route::post('/portal/tecDashboard/leadlist/jobdone','TechnicianController@jobdone');

        Route::get('/portal/message/techniciansCompose/add','MessageController@techniciansSendmessage');
        Route::post('/portal/message/tecnicianCompose/store','MessageController@composeTechniciansMessage');
        Route::get('portal/message/texhnicianSent/list','MessageController@sentMessagelist');

        Route::get('/portal/message/techniciansSendSingle/{id}','MessageController@viewSentMessage');

        Route::post('portal/message/techniciansSentDelete','MessageController@sentMessageDelete');

        Route::get('/portal/message/techniciansInbox/list','MessageController@inboxlistTechnicians');

        Route::get('portal/message/techniciansReply/{id}','MessageController@techniciansReplayMessage');

        Route::post('/portal/message/techniciansReply/store','MessageController@replayMessageTechnicains');

        Route::get('/portal/message/techniciansInboxSingle/{id}','MessageController@viewInboxMessageTecnicains');

        Route::get('/portal/message/single/{id}', 'MessageController@singleMessageView');
        //paid ammount
        Route::post('/portal/tecDashboard/leadlist/jobdone/paidammount', 'TechnicianController@paidAmmount');


        Route::post('/portal/lead/changetec','TechnicianController@changeStatus');


    });

    /*********************************************  CENTER MIDDLEWARE  AND LEAST ON********************************************** */
     Route::group(['middleware'=>'Center'],function (){
         Route::get('/portal/centerDashboard','DashboardController@centerDashboard');
         Route::get('/portal/centerDashboard/leadinformation','CenterController@leaddetails');
         Route::get('/portal/customersinformation/details/center/{id}','LeadInformationController@detailsLeadlist');

     });

    /*********************************************  HR MIDDLEWARE  AND LEAST ON********************************************** */
    Route::group(['middleware'=>'HR'],function (){

        Route::get('/portal/hrDashboard','DashboardController@hrDashboard');

    });

});








